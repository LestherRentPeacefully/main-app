import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

	public app = environment.app;
  public social = environment.social;
	public authenticated;

  constructor(private authService:AuthService) { }

  ngOnInit() {

  	this.authService.authenticated.subscribe(authenticated=> this.authenticated = authenticated? authenticated:false);
  }

}
