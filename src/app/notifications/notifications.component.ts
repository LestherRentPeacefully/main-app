import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {ToastService, ModalDirective} from 'ng-uikit-pro-standard';
import {SharedService} from '../shared/shared.service';
import {AuthService} from  '../auth.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  @ViewChild('notificationModal') public notificationModal:ModalDirective;
  @ViewChild('provideFeedbackModal') public provideFeedbackModal:ModalDirective;
  @ViewChild('confirmModal') public confirmModal:ModalDirective;

  @Input() userInfo;


  notifications:any[];
  loadingOldNotifications:boolean;
  canLoadMoreOldNotifications = true;
  notificationsOffset = 10;
  getLastNotificationSub;
  notificiationForFeedback;
  notificationToConfirm;
  userForFeedback;
  providingFeedback;

  confirming:boolean;

  public itemForm = this.fb.group({
    score: ['0',[Validators.required, Validators.min(1), Validators.max(5)]],
    avgTimeOfTicketResolution: ['0'],
    ontimePayments: ['0'],
    review: ['', [Validators.maxLength(200) ]],
  });

  private toastOptions = this.sharedService.toastOptions;


  constructor(
    private sharedService:SharedService, 
    private toast: ToastService, 
    private fb: FormBuilder,
    private authService:AuthService) { }

  ngOnInit() {
  }

  ngOnDestroy(){
    if(this.getLastNotificationSub) this.getLastNotificationSub.unsubscribe();
  }

  async openNotificationModal(){
    this.notificationModalClosed();
    this.notificationModal.show();
    this.notifications = await this.sharedService.getNotifications(this.userInfo.uid, this.notificationsOffset, null);
    if(this.userInfo.preferences.counters.unreads.notifications>0) await this.sharedService.markNotificationsAsRead(this.userInfo.uid);
    this.getLastNotificationSub = this.authService.notification$.subscribe(notification=>{
      this.insertNotification(notification);
    });
  }


  notificationModalClosed(){
    if(this.getLastNotificationSub) this.getLastNotificationSub.unsubscribe();
    this.getLastNotificationSub = null;
    this.notifications = null;
    this.canLoadMoreOldNotifications = true;
    this.loadingOldNotifications = false;
  }

  formatDate(unFormatedDate:number){
    let formatedDate = this.sharedService.formatDate(unFormatedDate);
    
    formatedDate.minutes = <any>(formatedDate.minutes<10? `0${String(formatedDate.minutes)}` : formatedDate.minutes);
    formatedDate.hours = formatedDate.hours==0? 12:formatedDate.hours;
    formatedDate.hours = formatedDate.hours>12? formatedDate.hours-12:formatedDate.hours;
    return `${formatedDate.month} ${formatedDate.dayOfMonth} ${formatedDate.year}, ${formatedDate.hours}:${formatedDate.minutes}:${formatedDate.seconds} ${formatedDate.hours>=12? 'PM':'AM'}`;
  }

  private insertNotification(notification){
    if(notification && !this.notifications.find(item=>item.id==notification.id)) {
      this.notifications.push(notification);
    }
  }

  async loadMore(){
    if(!this.loadingOldNotifications && this.canLoadMoreOldNotifications){
      this.loadingOldNotifications = true;
      const oldNotifications = await this.sharedService.getNotifications(this.userInfo.uid, this.notificationsOffset, this.notifications[this.notifications.length-1].creationTS);
      this.notifications.push(...oldNotifications);
      this.loadingOldNotifications = false;
      this.canLoadMoreOldNotifications = oldNotifications.length==this.notificationsOffset;
    }
  }


  public dateFromNow(date){
    return this.sharedService.dateFromNow(date);
  }


  public async openProvideFeedbackModal(notification:any){
    this.itemForm.reset();
    this.userForFeedback = null;

    this.notificiationForFeedback = notification;
    this.provideFeedbackModal.show();
    this.userForFeedback = await this.sharedService.getUser(this.notificiationForFeedback.feedbackFor);
  }

  public async openConfirmModal(notification:any) {
    this.notificationToConfirm = notification;
    this.confirmModal.show();
  }


  selectScore(score:number, action:string){
    this.itemForm.get(action).setValue(score);
  }


  async provideFeedBack(){
    this.providingFeedback = true;

    try {
      
      if (this.itemForm.get('avgTimeOfTicketResolution').errors || this.itemForm.get('ontimePayments').errors || this.itemForm.get('score').errors) {
        if (this.notificiationForFeedback.action == 'provide property landlord feedback') this.toast.error('','Please, rate the Landlord', this.toastOptions);
        else if (this.notificiationForFeedback.action == 'provide property tenant feedback') this.toast.error('','Please, rate the Tenant', this.toastOptions);
        return;
      }

      if(this.itemForm.get('review').errors){
        this.toast.error('',"You can't type more than 200 characteres", this.toastOptions); return;
      }


      let req = {
        notificationID: this.notificiationForFeedback.id,
        feedback: this.itemForm.value
      }

      this.itemForm.disable();

      const response = await this.sharedService.provideFeedback(req);

      if (response.success) {
        const index = this.notifications.findIndex(notif=>notif.id == this.notificiationForFeedback.id);
        delete this.notifications[index].hasButton, delete this.notifications[index].buttonText; 
        delete this.notifications[index].action, delete this.notifications[index].feedbackFor;
        this.toast.success('','Feedback provided successfully', this.toastOptions);
        this.provideFeedbackModal.hide();
        
      } else {
        console.error(response.error.message);
        this.toast.error('',response.error.message, this.toastOptions);
      }


    } catch (err) {
      console.error(err.message || err);
      this.toast.error('','Unexpected error submitting feedback', this.toastOptions);
    }

    this.providingFeedback = false;
    this.itemForm.enable();
  }


  async confirmPropertyListing(){
    this.confirming = true;
    try {
      const response = await this.sharedService.confirmPropertyListing({notificationID: this.notificationToConfirm.id});

      if (response.success) {
        const index = this.notifications.findIndex(notif=>notif.id == this.notificationToConfirm.id);
        delete this.notifications[index].hasButton, delete this.notifications[index].buttonText; 
        delete this.notifications[index].action, delete this.notifications[index].feedbackFor;
        this.toast.success('','Listing confirmed successfully', this.toastOptions);
        this.confirmModal.hide();
      } else {
        console.error(response.error.message);
        this.toast.error('',response.error.message, this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('','Unexpected error submitting feedback', this.toastOptions);
    }
    this.confirming = false;
  }


}
