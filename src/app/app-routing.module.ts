import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import {GuardsService} from './guards/guards.service';
import {NotGuardsService} from './guards/not.guards.service';


import {IntroComponent} from './intro/intro.component';
import {HowItWorksComponent} from './how-it-works/how-it-works.component';

const routes: Routes = [
  {
    path:'how-it-works',
    component:HowItWorksComponent
  },{
    path:'my-wallet',
    loadChildren:'./my-wallet/my-wallet.module#MyWalletModule',
    canActivate: [GuardsService]
  },{
    path:'account',
    loadChildren:'./account/account.module#AccountModule',
    canActivate: [NotGuardsService]
  },{
    path:'user',
    loadChildren:'./user/user.module#UserModule'
  },{
    path:'help',
    loadChildren:'./help/help.module#HelpModule'
  },{
    path:'my-properties',
    loadChildren:'./my-properties/my-properties.module#MyPropertiesModule',
    canActivate: [GuardsService]
  },{
    path:'listings',
    loadChildren:'./listings/listings.module#ListingsModule'
  },{
    path:'agreements',
    loadChildren:'./agreements/agreements.module#AgreementsModule'
  },{
    path:'applications',
    loadChildren:'./tenant/applications/applications.module#ApplicationsModule',
    canActivate: [GuardsService]
  },{
    path:'service-providers',
    loadChildren:'./directories/service-providers/service-providers.module#ServiceProvidersModule'
  },{
    path:'real-estate-agents',
    loadChildren:'./directories/real-estate-agents/real-estate-agents.module#RealEstateAgentsModule'
  },{
    path:'my-service-providers-jobs',
    loadChildren:'./my-service-providers-jobs/my-service-providers-jobs.module#MyServiceProvidersJobsModule',
  },{
    path:'admin/id-verification',
    loadChildren:'./admin/idverification/idverification.module#IDVerificationModule'
  },{
    path:'admin/property-listing',
    loadChildren:'./admin/property-listing/property-listing.module#PropertyListingModule'
  }
  ,{
    path:'admin/withdrawal',
    loadChildren:'./admin/withdrawal/withdrawal.module#WithdrawalModule'
  },{
    path:'admin/balances',
    loadChildren:'./admin/balances/balances.module#BalancesModule'
  },{
    path:'',
    component:IntroComponent
  },{ 
    path: '**', redirectTo: '', pathMatch: 'full' 
  } //PAGE NOT FOUND
  // { path: '**', component: PageNotFoundComponent }   

];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    { enableTracing: false }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
