import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../auth.service';
import { Router }  from '@angular/router';
import {SharedService} from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { ListingsService } from '../listings.service';
@Component({
  selector: 'app-mine',
  templateUrl: './mine.component.html',
  styleUrls: ['./mine.component.css']
})
export class MineComponent implements OnInit {
  public userInfo:any;
  public userInfoSubscriber: any;
  public listings:any[];

  constructor(
    private listingsService:ListingsService,
    private router: Router,
    private authService:AuthService,
  ) { }

  ngOnInit() {
    this.userInfoSubscriber = this.authService.userInfo.subscribe(async (userInfo) => {
      this.userInfo = userInfo;
      if(this.userInfo){
        this.listings = await this.listingsService.getMyListings(this.userInfo.uid);
      }
    });
  }


  ngOnDestroy() {
    this.userInfoSubscriber.unsubscribe();
  }



  public listProperty(){
    // this.router.navigate([`listings/new/${myPropertyID}`]); 
    this.router.navigate([`listings/new`]); 
  }

  public createdAt(timestamp:number) : string {
    let date = new Date(timestamp);
    return `${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()}`;
  }

  public viewListing(listingID:string){
    this.router.navigate([`listings/details/${listingID}`]); 
  }

}
