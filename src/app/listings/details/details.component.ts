import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormBuilder,Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {ToastService, ModalDirective} from 'ng-uikit-pro-standard';
import {AuthService} from '../../auth.service';
import {SharedService} from '../../shared/shared.service';
import {ListingsService} from '../listings.service';
import {BigNumber} from 'bignumber.js';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  public app = environment.app;
  private sub;
  public submitting;
  public listingID:string;
	private listingSnapshot;
	private toastOptions = this.sharedService.toastOptions;
	public listing;
  public amenities;
  public userInfo:any;
  private userInfoSubscriber:any;
  public highestOffer;
  private loadedOnce;
  public loadingApplication;
  public rejecting;
  public actionToReject;
  public acceptingApplication;
  public movingIn;
  public markingAsRented;
  public applications:any[];
  public application;
  public applicantToAccept;
  public applicantToReject;
  public feesAndChargesAction;
  public submittingFeesAndChargesAction;
  public currencies:any[];
  public currencySelected:any;
  public amountToPay:string;
  public contactingAdvertiser;
  
  public isTenant:boolean;

  public applyForm = this.fb.group({
    rentAmount: ['',[Validators.required, Validators.min(1) ]],
    securityDepositAmount: ['',[Validators.required, Validators.min(0) ]],
    message: ['',[Validators.required, Validators.maxLength(1000) ]],
  });

  public contactAdvertiserForm = this.fb.group({
    message: ['', [Validators.required, Validators.maxLength(200) ]],
    email: ['', [Validators.required, Validators.email ]],
    name: ['', [Validators.required ]],
  });

  @ViewChild('applyModal') public applyModal:ModalDirective;
  @ViewChild('acceptApplicationModal') public acceptApplicationModal:ModalDirective;
  @ViewChild('rejectionModal') public rejectionModal:ModalDirective;
  @ViewChild('markAsRentedModal') public markAsRentedModal:ModalDirective;
  @ViewChild('feesAndChargesModal') public feesAndChargesModal:ModalDirective;
  @ViewChild('contactAdvertiserModal') public contactAdvertiserModal:ModalDirective;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public sharedService:SharedService,
    private authService:AuthService,
    private listingsService:ListingsService,
    private toast: ToastService,) { }

  ngOnInit() {
    this.sub = this.route.paramMap.subscribe((params)=>{

      this.listingID = params.get('id');
      this.loadedOnce = false;


      this.listingSnapshot = this.listingsService.getListing$(this.listingID).subscribe((listing)=>{
        this.listing = listing;

        this.listing.photos = this.listing.photos.map((url, index)=>{ return {img:url,thumb:url,description:`Image ${index}`}});
          
        if(this.listing.amenities) this.amenities = this.sharedService.amenities.filter(amenity=>this.listing.amenities[amenity.id]);


        if(!this.loadedOnce){

          this.loadedOnce = true;

          if(this.userInfoSubscriber) this.userInfoSubscriber.unsubscribe();

          this.userInfoSubscriber = this.authService.userInfo.subscribe(async (userInfo)=>{

            this.userInfo = userInfo;

            if (this.userInfo && (this.listing.user.role != 'real estate agent' || this.listing.includesLandlord)) {
              if(this.userInfo.uid != this.listing.user.uid){ //Not the landlord
                  
                this.loadingApplication = true;
                
                let application = (await this.listingsService.getApplication(this.listingID, this.userInfo.uid));
                if(application){
                  this.application = application;
                  this.applyForm.get('rentAmount').setValue(this.application.applicant.offer.rentAmount);
                  this.applyForm.get('securityDepositAmount').setValue(this.application.applicant.offer.securityDepositAmount);
                  this.applyForm.get('message').setValue(this.application.applicant.message);

                }else{
                  if(this.listing.myPropertyID){
                    let response = await this.listingsService.isTenant({myPropertyID:this.listing.myPropertyID});
                    if(response.success) this.isTenant = response.data;
                  }else{ //PROPERTY IS NOT CREATED YET
                    this.isTenant = false;
                  }
                }
                
              }

              if(this.listing.applicants==0){
                this.applications = [];
              }else{
                this.applications = await this.listingsService.getApplications(this.listingID);
              }

              this.loadingApplication = false;
            }
          });

        }
      });
    });
  }


  ngOnDestroy(){
    this.sub.unsubscribe();
    if(this.userInfoSubscriber) this.userInfoSubscriber.unsubscribe();
    if(this.listingSnapshot) this.listingSnapshot.unsubscribe();
  }


  public get availableFrom() : string {
    let date = new Date(this.listing.availableFrom);
    return new Date()>=date? 'Right Now':`${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()}`;
  }

  public tooltipText(status:string) : string {
    switch (status) {
      case 'pending':
        return `Listing will be publicly visible once ${this.app.name} team verifies your form.`;
      case 'rejected':
        return `Listing has been rejected by ${this.app.name} team.`;
      case 'listed':
        return `Listing is publicly visible.`;
      case 'rented':
        return `This property has already been rented`;
    }
  }


  viewProfile(uid:string){
    this.router.navigate(['/user', uid], { queryParams: {role: 'tenant'}});
  }

  public publishDate(timestamp:number) : string {
    let date = new Date(timestamp);
    return `${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()}`;
  }


  public apply(){
    if (this.userInfo) {
      if(this.listing.user.uid==this.userInfo.uid) this.toast.error('',"You can't apply to your own property", this.toastOptions);
      else if(!this.userInfo.status.IDVerified) this.toast.error('','Please, verify your Identity first', this.toastOptions);
      else this.applyModal.show();
    } else {
      this.toast.info('','Please log in first.',this.toastOptions);
      this.authService.redirectUrl = this.router.url;
      this.router.navigate(['/account/login']);
    }
  }

  async applyModalShown(){
    this.highestOffer = await this.listingsService.getHighestOffer(this.listingID);
  }

  applyModalHidden(){
    this.highestOffer = null;
  }

  async submitApplication(){
    this.submitting = true;
    let req = this.applyForm.value;
    req.listingID = this.listingID;
    this.applyForm.disable();

    try {
      let response = await this.listingsService.submitApplication(req);

      if (response.success) {
        this.toast.success('Application submitted successfully','',this.toastOptions);
        if (!this.application) {
          this.application = {
            applicant:{
              firstName:this.userInfo.firstName,
              lastName:this.userInfo.lastName,
              email:this.userInfo.email,
              tenant:this.userInfo.tenant,
              photo:this.userInfo.photo,
              membership:this.userInfo.membership,
              message:req.message,
              uid:this.userInfo.uid,
              offer:{
                rentAmount:req.rentAmount,
                securityDepositAmount:req.securityDepositAmount
              }
            },
            creationTS:Date.now()
          };

          this.applications.push(this.application);
          

        } else {

          this.application.applicant.message = req.message;
          this.application.applicant.offer = {
            rentAmount:req.rentAmount,
            securityDepositAmount:req.securityDepositAmount
          }

          
          const index = this.applications.findIndex(item=>item.applicant.uid==this.userInfo.uid);

          this.applications[index].applicant.message = req.message;
          this.applications[index].applicant.offer = {
            rentAmount:req.rentAmount,
            securityDepositAmount:req.securityDepositAmount
          }

        }

        this.applyModal.hide();

      } else {
        this.toast.error('', response.error.message, this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('','Unexpected error submitting application. Try again later', this.toastOptions);
    }

    this.submitting = false;
    this.applyForm.enable();

  }


  

  public opentAcceptApplicationModal(applicant:any){
    this.applicantToAccept = applicant;
    this.acceptApplicationModal.show();
  }

  
  async acceptApplication(){
    this.acceptingApplication = true;
    try {
      let response = await this.listingsService.acceptApplication({listingID:this.listingID,  applicantID:this.applicantToAccept.uid});
      if (response.success) {
        this.toast.success('Application accepted successfully. Waiting for Applicant confirmation','',this.toastOptions);
        this.applications[this.applications.findIndex(item=>item.applicant.uid==this.applicantToAccept.uid)].applicant.status = 'selected';
        this.acceptApplicationModal.hide();
      } else {
        this.toast.error('', response.error.message, this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('','Unexpected error accepting application. Try again later', this.toastOptions);
    }
    this.acceptingApplication = false;
  }


  public openRejectionModal(action, applicant?){
    this.actionToReject = action;
    this.rejectionModal.show();
    if(action=='retractApplication'){
      this.applicantToReject = applicant;
    }
  }


  async rejectAction(){
    this.rejecting = true;

    try {
      if (this.actionToReject=='deleteApplication') {
        let response = await this.listingsService.deleteApplication({listingID:this.listingID});
        if (response.success) {
          this.toast.success('Application deleted successfully','',this.toastOptions);
          this.applications.splice(this.applications.findIndex(item=>item.applicant.uid==this.userInfo.uid), 1);
          this.application = null;
          this.applyForm.reset();
          this.rejectionModal.hide();
        } else {
          console.error(response.error.message);
          this.toast.error('', response.error.message, this.toastOptions);
        }

      }else if(this.actionToReject=='retractApplication'){
        let response = await this.listingsService.retractApplication({listingID:this.listingID, applicantID:this.applicantToReject.uid});
        if (response.success) {
          this.toast.success('','Application retracted successfully',this.toastOptions);
          delete this.applications[this.applications.findIndex(item=>item.applicant.uid==this.applicantToReject.uid)].applicant.status;
          this.rejectionModal.hide();
        } else {
          console.error(response.error.message);
          this.toast.error('', response.error.message, this.toastOptions);
        }

      }else if(this.actionToReject=='deleteListing'){
        let oldFiles = [];
        for(let photoOpt of this.listing.photos){
          let url = photoOpt.img;
          oldFiles.push(this.sharedService.photoPathFromURL(url));
        }
        let response = await this.listingsService.deleteListing({listingID:this.listingID, oldFiles});
        if (response.success) {
          this.toast.success('Listing deleted successfully','',this.toastOptions);
          this.rejectionModal.hide();
          this.router.navigate(['/user', this.userInfo.uid], { queryParams: {role: this.userInfo.role}});
        } else {
          console.error(response.error.message);
          this.toast.error('', response.error.message, this.toastOptions);
        }
      }
      

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('','Unexpected error. Try again later', this.toastOptions);
    }

    this.rejecting = false;
  }



  async moveIn(){
    this.movingIn = true;
    try {
      let response = await this.listingsService.moveIn({listingID:this.listingID, listingOwnerID:this.listing.user.uid});
      if (response.success) {
        this.toast.success('Property successfully rented','Welcome to your property Dashboard', this.toastOptions);
        this.router.navigate([`/my-properties/details/${response.data}`]);
      } else {
        console.error(response.error.message);
        this.toast.error('', response.error.message, this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('','Unexpected error confirming move. Try again later', this.toastOptions);
    }
    this.movingIn = false;
  }

  openMarkAsRentedModal(){
    this.markAsRentedModal.show();
  }

  async markListingAsRented(){
    this.markingAsRented = true;
    try {
      const response = await this.listingsService.markListingAsRented({listingID:this.listingID})
      if(response.success){
        this.toast.success('Property marked as rented','', this.toastOptions);
        this.listing.status = 'rented';
        this.markAsRentedModal.hide();

      }else{
        console.error(response.error.message);
        this.toast.error('', response.error.message, this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('','Unexpected error. Try again later', this.toastOptions);
    }
    this.markingAsRented = false;
  }

  public isRecommended(recommendedUntil:number){
    return recommendedUntil >= Date.now();
  }


  async openFeesAndChargesModal(action:string){
    this.currencies = null;
    this.currencySelected = null;

    this.feesAndChargesModal.show();
    this.feesAndChargesAction = action;
    this.currencies = await this.sharedService.getCurrencies();
  }


  async selectCurrency(item:any){
    if(!this.submittingFeesAndChargesAction && (!this.currencySelected || this.amountToPay)){

      this.currencySelected = item;
      this.amountToPay = null;

      try {
        let response, amountToPayInUSD;

        if(this.feesAndChargesAction == 'highlightListing'){
          amountToPayInUSD = 10;
        }else if(this.feesAndChargesAction == 'highlightApplication'){
          amountToPayInUSD = 5;
        }
        response = await this.listingsService.getCurrencyPriceForFeesAndCharges({currency:this.currencySelected.currency});

        if (response.success) {
          this.amountToPay = new BigNumber(amountToPayInUSD).div(response.data.price.USD).decimalPlaces(8).toString(10);
          
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'',this.toastOptions);
        }

        
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }
    }
  }


  public formatBalance(value){
    return this.sharedService.formatBalance(value, 0); 
  }




  async submitFeesAndChargesAction(){
    
    try {

      if (!this.userInfo.balances[this.currencySelected.currency] || !(new BigNumber(this.userInfo.balances[this.currencySelected.currency].available).isGreaterThanOrEqualTo(this.amountToPay))) {
        this.toast.error('Insufficient funds','', this.toastOptions); return;
      }

      this.submittingFeesAndChargesAction = true;


      if(this.feesAndChargesAction == 'highlightListing'){
        const response = await this.listingsService.highlightListing({currency:this.currencySelected.currency, listingID:this.listingID});

        if (response.success) {
          this.toast.success('Listing highlighted successfully','', this.toastOptions);
          this.listing.recommendedUntil = this.sharedService.addMonths(Date.now(), 1).getTime();
          this.feesAndChargesModal.hide();
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'',this.toastOptions);
        }

      } else if(this.feesAndChargesAction == 'highlightApplication'){
        const response = await this.listingsService.highlightApplication({currency:this.currencySelected.currency, listingID:this.listingID});
        if (response.success) {
          this.toast.success('Application highlighted successfully','', this.toastOptions);
          this.application.applicant.isHighlighted = 1;
          this.applications[this.applications.findIndex(item=>item.applicant.uid==this.userInfo.uid)].applicant.isHighlighted = 1;
          this.feesAndChargesModal.hide();
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'',this.toastOptions);
        }
      }



    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }

    this.submittingFeesAndChargesAction = false;
  }




  openContactAdvertiserModal(){
    this.contactAdvertiserForm.reset();
    this.contactAdvertiserModal.show();
  }

  async contactAdvertiser(){
    let req = {
      listingID: this.listingID,
      message: this.contactAdvertiserForm.value.message,
      email: this.contactAdvertiserForm.value.email,
      name: this.contactAdvertiserForm.value.name
    }

    this.contactingAdvertiser = true;
    this.contactAdvertiserForm.disable();

    try {
      const response = await this.listingsService.contactAdvertiser(req);

      if (response.success) {
        this.toast.success('Email sent to the Advertiser','', this.toastOptions);
        this.contactAdvertiserModal.hide();
      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'',this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.contactAdvertiserForm.enable();
    this.contactingAdvertiser = false;
  }


  public openMessagesObj(applicant) {
    let obj = {
      chatName:`(${this.sharedService.capitalize(applicant.firstName)} ${applicant.lastName[0].toUpperCase()}) ${this.listing.name}`,
      id: applicant.chatID,
      user: this.listing.user.uid != this.userInfo.uid? this.listing.user : applicant,
      listingID:this.listingID,
      applicantID: applicant.uid
    }
    return obj;
  }



}
