import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AngularFireStorage } from '@angular/fire/storage';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import {SharedService} from '../shared/shared.service';
import {AngularFirestore} from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class ListingsService {

  private APIURL = environment.APIURL;

  constructor(private storage: AngularFireStorage,
              private http: HttpClient,
              private afs:AngularFirestore,
              private sharedService:SharedService,
              public afAuth: AngularFireAuth) { }



  public getFilePath(uid:string, fileName:string){
    return `users/${uid}/listings/images/${fileName}`;
  }

  public uploadPhoto(uid:string, file:File, fileName:string){
 		let fileRef = this.storage.ref(this.getFilePath(uid, fileName));
    let task = fileRef.put(file, {customMetadata:{fileName: file.name }});
    return task.snapshotChanges().pipe(map(snapshot=>(snapshot.bytesTransferred / snapshot.totalBytes) * 100));
  }

  public getImageURL(uid:string, fileName:string){
    let fileRef = this.storage.storage.ref(this.getFilePath(uid, fileName));
    return fileRef.getDownloadURL();
  }

  async new(fields): Promise<any>{
  	let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/listings/new`, fields, _httpOptions).toPromise();
  }

  async getMyProperty(myPropertyID:string){
    const db = firebase.firestore();
    return (await db.doc(`myProperties/${myPropertyID}`).get()).data();
  }

  public getListing$(listingID:string){
    return this.afs.doc(`listings/${listingID}`).valueChanges();
  }

  public async isTenant(req:{myPropertyID:string}): Promise<any>{
    // return (await db.doc(`myProperties/${myPropertyID}/tenants/${uid}`).get()).exists;
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/listings/isTenant`, req, _httpOptions).toPromise();
  }

  public async getHighestOffer(listingID:string){
    const db = firebase.firestore();
    
    let offer:any = {};
    let [rentSnapshot, depositSnapshot] = 
        await Promise.all([db.collection('applications')
                            .where('listing.id','==',listingID)
                            .orderBy('applicant.offer.rentAmount','desc').limit(1).get(), 
                           db.collection('applications')
                            .where('listing.id','==',listingID)
                            .orderBy('applicant.offer.securityDepositAmount','desc').limit(1).get()]);

    offer.rentAmount = rentSnapshot.empty? 0 : rentSnapshot.docs[0].data().applicant.offer.rentAmount;
    offer.securityDepositAmount = depositSnapshot.empty? 0 : depositSnapshot.docs[0].data().applicant.offer.securityDepositAmount;
    return offer as {rentAmount:number,securityDepositAmount:number};
  }

  public async getApplication(listingID:string, uid:string){
    const db = firebase.firestore();
    let applicationSnapshot = await db.collection('applications')
                                      .where('listing.id','==',listingID)
                                      .where('applicant.uid','==',uid).limit(1).get();
    return applicationSnapshot.empty? null : applicationSnapshot.docs[0].data();
  }

  public async getApplications(listingID:string){
    const db = firebase.firestore();
    let applications = [];
    let arrayOfPromises:Promise<any>[] = [];
    let applicationSnapshot = await db.collection('applications')
                                      .where('listing.id','==',listingID)
                                      .orderBy('applicant.isHighlighted','desc')
                                      .orderBy('creationTS','asc').get();
    if(!applicationSnapshot.empty){
      for(let applicationDoc of applicationSnapshot.docs){
        let application = applicationDoc.data();
        delete application.listing;
        applications.push(application);
        arrayOfPromises.push(db.doc(`users/${application.applicant.uid}`).get());
      }

      let usersSnapshot = await Promise.all(arrayOfPromises);
      
      applications = applications.map(application=>{
        let userSnapshot = usersSnapshot.find(item=>item.data().uid==application.applicant.uid);
        application.applicant.firstName = userSnapshot.data().firstName;
        application.applicant.lastName = userSnapshot.data().lastName;
        application.applicant.email = userSnapshot.data().email;
        application.applicant.tenant = userSnapshot.data().tenant;
        application.applicant.photo = userSnapshot.data().photo;
        application.applicant.membership = userSnapshot.data().membership;
        return application;
      });
      
    }
    
    return applications;
  }

  async submitApplication(req:{listingID:string, rentAmount:number, securityDepositAmount:number, message:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/listings/submitApplication`, req, _httpOptions).toPromise();
  }

  async deleteApplication(req:{listingID:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/listings/deleteApplication`, req, _httpOptions).toPromise();
  }

  async acceptApplication(req:{listingID:string, applicantID:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/listings/acceptApplication`,req, _httpOptions).toPromise();
  }
  
  async retractApplication(req:{listingID:string, applicantID:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/listings/retractApplication`, req, _httpOptions).toPromise();
  }


  async deleteListing(req:{listingID:string, oldFiles:string[]}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/listings/deleteListing`,req, _httpOptions).toPromise()
  }

  public async moveIn(req:{listingID:string, listingOwnerID:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/listings/moveIn`, req, _httpOptions).toPromise();
  }

  public getListings(filter:any,offset:number):Promise<any>{
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    return this.http.post(`${this.APIURL}/listings/getListings`, {filter, offset}, httpOptions).toPromise();
  }

  public getTotalListings(filter:any){
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    return this.http.post(`${this.APIURL}/listings/getTotalListings`, {filter}, httpOptions).toPromise();
  }

  public getTotalAndListings(filter:any,offset:number):Promise<any>{
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    return this.http.post(`${this.APIURL}/listings/getTotalAndListings`, {filter, offset}, httpOptions).toPromise();
  }

  async getListing(listingID:string){
    const db = firebase.firestore();
    let snapshot = await db.doc(`listings/${listingID}`).get();
    return snapshot.data();
  }

  async editListing(fields:any, listingID:string, oldFiles:string[]): Promise<any>{
  	let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/listings/editListing`, {...fields, listingID, oldFiles}, _httpOptions).toPromise();
  }


  async markListingAsRented(req:{listingID:string}){
  	let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/listings/markListingAsRented`, req, _httpOptions).toPromise();
  }

  async getCurrencyPriceForFeesAndCharges(req:{currency:string}){
  	let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/listings/getCurrencyPriceForFeesAndCharges`, req, _httpOptions).toPromise();
  }

  async highlightListing(req:{currency:string, listingID:string}){
  	let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/listings/highlightListing`, req, _httpOptions).toPromise();
  }
  async highlightApplication(req:{currency:string, listingID:string}){
  	let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/listings/highlightApplication`, req, _httpOptions).toPromise();
  }

  async getMyListings(uid:string, status?: string){
    const db = firebase.firestore();
    let listings = [];
    let ref = db.collection(`listings`).where('user.uid','==',uid);
    if(status) ref = ref.where('status','==',status);
    let snapshot = await ref.get();
    snapshot.forEach(doc=>listings.push(doc.data()));
    return listings;
  }

  public contactAdvertiser(req:{listingID:string, message:string, email:string, name:string}):Promise<any>{
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    return this.http.post(`${this.APIURL}/listings/contactAdvertiser`, req, httpOptions).toPromise();
  }

  async startChat(req:{content:string, listingID:string, applicantID:string}){
  	let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/listings/startChat`, req, _httpOptions).toPromise();
  }


}
