import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { ToastService, IMyOptions, ModalDirective } from 'ng-uikit-pro-standard';
import {SharedService} from '../../shared/shared.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { environment } from '../../../environments/environment';
import * as Cropper from 'cropperjs/dist/cropper.js';
import {ListingsService} from '../listings.service';
import { BehaviorSubject } from 'rxjs';
import { MapsAPILoader } from '@agm/core';
import * as geofirex from 'geofirex';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  private sub;
  public userInfo$:BehaviorSubject<any>;
  private toastOptions = this.sharedService.toastOptions;
  public submiting;
  public app = environment.app;
  public status = 0;

  public listing;

  public listingID:string;

  public amenities: Array<any> = this.sharedService.amenities;


  public propertyOptions = this.sharedService.propertyOptions;
  public petsAllowanceOptions = this.sharedService.petsAllowanceOptions;
  public bedsOptions = this.sharedService.bedsOptions;
  public bathsOptions = this.sharedService.bathsOptions;

  public addPhotoLogoURL = './assets/addPhoto2.png';
  public file:any = {};
  private oldFiles:string[] = [];
  public imageToCrop;
  public imageCropped = {
    localURL: [],
    uploading: [],
    deleting: [],
    uploadFilePercentage: [],
    filePath: []
  }

  public cropper;

  public myDatePickerOptions: IMyOptions = {
    minYear: new Date().getFullYear(),
    closeAfterSelect: true
  }

  public itemForm = this.fb.group({
    name: ['', [Validators.required, Validators.maxLength(50)]],
    address: ['', [Validators.required,]],
    location: this.fb.group({
      lat: [''],
      lng: [''],
    }),
    // includesLandlord: ['no'],
    unit: ['', []],
    type: ['', [Validators.required,]],
    numOfBeds: ['', [Validators.required,]],
    numOfBaths: ['', [Validators.required,]],
    sqft: ['', [Validators.required, Validators.min(1)]],
    rentAmount: ['', [Validators.required, Validators.min(1)]],
    securityDepositAmount: ['', [Validators.required, Validators.min(0)]],
    details: ['', [Validators.required, Validators.maxLength(1000)]],
    availableFrom: ['', [Validators.required,]],
    // typeOfContract:['traditional contract', [Validators.required,]],
    photos: this.fb.array([
      this.fb.control('', Validators.required),
      this.fb.control('', Validators.required),
      this.fb.control('', Validators.required)
    ])
  });


  @ViewChild("search") public searchElementRef: ElementRef;
  @ViewChild('cropImageModal') public cropImageModal: ModalDirective;
  @ViewChild('imageToCrop') public imageToCropElementRef: ElementRef;


  constructor(
    private toast: ToastService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private sharedService:SharedService,
    private listingsService:ListingsService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private authService: AuthService) { }

  ngOnInit() {

    this.userInfo$ = this.authService.userInfo;

    this.sub = this.route.paramMap.subscribe(async (params)=>{
      this.listingID = params.get('id');
       
      this.listing = await this.listingsService.getListing(this.listingID);
      
      if(this.listing.photos.length>this.photos.length){
        for(let i=0; i<this.listing.photos.length-this.photos.length; i++){
          this.photos.push(this.fb.control(''));
        }
      }

      this.userInfo$.subscribe(async userInfo=>{

        if(userInfo && userInfo.role == 'real estate agent'){
          this.itemForm.get('location.lat').setValidators(Validators.required);
          this.itemForm.get('location.lng').setValidators(Validators.required);
          // this.itemForm.get('includesLandlord').disable();

          await this.mapsAPILoader.load();
          
          let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
            types: ["geocode"]
          });

          autocomplete.addListener("place_changed", () => {
            this.ngZone.run(() => {

              let place: google.maps.places.PlaceResult = autocomplete.getPlace();

              if (place.geometry) {

                this.itemForm.get('address').setErrors(null);
                this.itemForm.get('address').setValue(this.searchElementRef.nativeElement.value);

                this.itemForm.get('location').setValue({
                  lat: place.geometry.location.lat(),
                  lng: place.geometry.location.lng()
                });


              } else {

                this.itemForm.get('address').setErrors({ wrongAddress: true });
              }

            });
          });


        } else if(userInfo && userInfo.role != 'real estate agent'){
          this.itemForm.get("address").disable();
          this.itemForm.get("unit").disable();
        }

        this.unformatFields();

        this.itemForm.patchValue(this.listing);

        this.imageCropped.localURL.push(...this.photos.value);
        this.imageCropped.filePath.push(...(<string[]>this.photos.value).map(value=>this.sharedService.photoPathFromURL(value)));

        // this.itemForm.get("typeOfContract").disable();

      });

      

    });

  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }



  get photos() {
    return this.itemForm.get('photos') as FormArray;
  }

  get photosUploading() {
    return this.imageCropped.uploading.find(uploading => uploading);
  }

  get photosDeleting() {
    return this.imageCropped.deleting.find(uploading => uploading);
  }




  onShown() {
    this.cropper = new Cropper(this.imageToCrop, {
      aspectRatio: 1.5,
    });
  }

  onHidden() {
    this.cropper.destroy();
    this.cropper = null;
  }

  addNewPhoto() {
    this.photos.push(this.fb.control(''));
  }

  addPhoto(e, index) {

    this.imageToCrop = this.imageToCropElementRef.nativeElement;
    let files = e.target.files as File[];
    
    if (files && files.length > 0) {

      let file = files[0];
      if (!file.type.match(/image-*/)) {
        this.toast.error('', 'Invalid format', this.toastOptions);
      } else {
        this.file.type = file.type;

        let reader = new FileReader();

        reader.onloadend = (e) => {
          let url = reader.result;
          this.imageToCrop.src = url;
          this.cropImageModal.show();
        };
        reader.readAsDataURL(file);

      }
    }
    
    e.target.value = '';
  }



  uploadNewPhoto() {
    let canvas;

    if (this.cropper) {

      canvas = this.cropper.getCroppedCanvas({
        // width: 220,
        // height: 220,
      });

      canvas.toBlob(file => {

        this.cropImageModal.hide();

        let index = this.imageCropped.localURL.length;

        this.imageCropped.localURL.push(canvas.toDataURL(this.file.type));

        let fileName = this.sharedService.newFileName(file.type);

        let subscriber = this.listingsService.uploadPhoto(this.userInfo$.getValue().uid, file, fileName).subscribe((uploadFilePercent) => {

          this.imageCropped.uploading[index] = true;
          this.imageCropped.uploadFilePercentage[index] = uploadFilePercent;

        }, (err) => {

          this.imageCropped.uploading[index] = false;
          this.imageCropped.localURL.splice(index, 1);
          this.photos.removeAt(index);
          if (this.photos.length < 3) this.photos.push(this.fb.control('', Validators.required)); else this.photos.push(this.fb.control(''));
          this.photos.updateValueAndValidity();
          console.error(err.message || err);
          this.toast.error('', 'Error uploading photo. Try again later', this.toastOptions);

        }, async () => {

          subscriber.unsubscribe();

          try {
            this.photos.controls[index].setValue(await this.listingsService.getImageURL(this.userInfo$.getValue().uid, fileName));
            this.imageCropped.uploading[index] = false;
            this.imageCropped.filePath[index] = this.listingsService.getFilePath(this.userInfo$.getValue().uid, fileName);

          } catch (err) {
            this.imageCropped.uploading[index] = false;
            this.imageCropped.localURL.splice(index, 1);
            this.photos.removeAt(index);
            if (this.photos.length < 3) this.photos.push(this.fb.control('', Validators.required)); else this.photos.push(this.fb.control(''));
            this.photos.updateValueAndValidity();
            console.error(err.message || err);
            this.toast.error('', 'Error uploading photo. Try again later', this.toastOptions);
          }

        });


      }, this.file.type);

    }

  }



  async deletePhoto(index) {
    this.imageCropped.deleting[index] = true;

    try {

      if(index > this.listing.photos.length-1){
        await this.sharedService.deletePhoto(this.imageCropped.filePath[index]);
      }else{
        this.oldFiles.push(this.sharedService.photoPathFromURL(this.listing.photos[index]));
        this.listing.photos.splice(index, 1);
      }

      this.imageCropped.localURL.splice(index, 1);
      this.imageCropped.filePath.splice(index, 1);
      this.photos.removeAt(index);

      if (this.photos.length < 3) this.photos.push(this.fb.control('', Validators.required));

      this.photos.updateValueAndValidity();


    } catch (err) {
      console.error(err.message || err);
      this.toast.error('', 'Error deleting photo. Try again later', this.toastOptions);
    }

    this.imageCropped.deleting[index] = false;
  }



  public selectAmenities(amenity: string) {
    let index = this.amenities.findIndex(item => item.name == amenity);
    this.amenities[index].selected = !this.amenities[index].selected;
  }



  private unformatFields(){
    this.listing.type = this.propertyOptions.find(item => item.label == this.listing.type).value;
    this.listing.numOfBeds = this.bedsOptions.find(item => item.label == this.listing.numOfBeds).value;
    this.listing.numOfBaths = this.bathsOptions.find(item => item.label == this.listing.numOfBaths).value;

    let dateTS = new Date(this.listing.availableFrom);
    this.listing.availableFrom = `${dateTS.getFullYear()}-${dateTS.getMonth()+1}-${dateTS.getDate()}`;

    if(this.userInfo$.getValue().role == 'real estate agent'){
      // this.listing.includesLandlord = this.listing.includesLandlord? 'yes' : 'no';
      this.listing.location = {
        lat:this.listing.location.geopoint.latitude,
        lng:this.listing.location.geopoint.longitude
      }
    }
      
    this.amenities = this.amenities.map(amenity=>{
      if(this.listing.amenities[amenity.id]){
        amenity.selected = true;
      }
      return amenity;
    });
  }
  
  private formatFields(fields: any) {
    let req = fields;
    req.type = this.propertyOptions.find(item => item.value == req.type).label;
    req.numOfBeds = this.bedsOptions.find(item => item.value == req.numOfBeds).label;
    req.numOfBaths = this.bathsOptions.find(item => item.value == req.numOfBaths).label;

    let getTimezoneOffset = new Date().getTimezoneOffset()*60*1000; //miliseconds
    req.availableFrom = new Date(req.availableFrom).getTime() + getTimezoneOffset;

    req.amenities = {};
    for (let amenity of this.amenities) {
      if (amenity.selected) req.amenities[amenity.id] = true;
    }

    if(req.role == 'real estate agent'){
      const geo = geofirex.init(firebase);
      req.location.geohash = geo.point(req.location.lat, req.location.lng).hash;      
    }

    return req;
  }




  async onSubmit() {
    this.submiting = true;

    let req = this.formatFields(this.itemForm.value);

    this.itemForm.disable();

    try {
      const response = await this.listingsService.editListing(req, this.listingID, this.oldFiles);

      if (response.success) {
        this.status = 1;
      } else {
        this.toast.error('', response.error.message, this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('', 'Error submitting request. Try again later', this.toastOptions);
    }

    this.itemForm.enable();
    this.submiting = false;
  }



}
