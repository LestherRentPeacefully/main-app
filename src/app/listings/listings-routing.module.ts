import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



/* Components */

import { ListingsComponent } from './listings.component';
import { NewComponent } from './new/new.component';
import { DetailsComponent } from './details/details.component';
import { AllComponent } from './all/all.component';
import {EditComponent} from './edit/edit.component';
import { MineComponent } from './mine/mine.component';

const routes: Routes = [{
  path:'',
  component:ListingsComponent,
  children:[
    {
      path:'all',
      component:AllComponent
    },
    {
      path:'mine',
      component:MineComponent
    },
    {
      path:'new',
      component:NewComponent
    },{
      path:'details/:id',
      component:DetailsComponent
    },{
      path:'edit/:id',
      component:EditComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListingsRoutingModule { }
