/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, ElementRef,NgZone } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {ToastService, ModalDirective } from 'ng-uikit-pro-standard';
import {SharedService} from '../../shared/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import {ListingsService} from '../listings.service';
import {BigNumber} from 'bignumber.js';


@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {

  public propertyOptions = this.sharedService.propertyOptions;
  public bathsOptions = this.sharedService.bathsOptions;
  public bedsOptions = this.sharedService.bedsOptions;
  private toastOptions = this.sharedService.toastOptions;
  public amenities: Array<any> = this.sharedService.amenities;
  public minPriceSub;
  public maxPriceSub;
  public submitting;
  private sub:any;
  public listings: any[];
  public total:number;
  public addressEntered;
  public location;
  public filter:any = {};

  public itemForm = this.fb.group({
    minPrice: ['',[Validators.min(0)]],
    maxPrice: ['',[Validators.min(0)]],
    type: ['',[]],
    numOfBeds: ['',[ ]],
    numOfBaths: ['',[ ]],
    sqft: ['',[Validators.min(1) ]],
  });

  @ViewChild("search")  public searchElementRef: ElementRef<HTMLInputElement>;
  @ViewChild("search2")  public searchElementRef2: ElementRef<HTMLInputElement>;
  @ViewChild('filterModal') public filterModal:ModalDirective;
  @ViewChild("minPrice")  public InputMinPrice: ElementRef<HTMLInputElement>;
  @ViewChild("maxPrice")  public InputMaxPrice: ElementRef<HTMLInputElement>;

  constructor(
    public sharedService:SharedService,
    private toast: ToastService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private listingsService: ListingsService,
    private ngZone: NgZone) { }

  ngOnInit() {

    this.sub = this.route.queryParams.subscribe(async params => {
      
      this.filter = params;
      
      if(this.filter.realEstateAgent) {
        this.listings = await this.listingsService.getMyListings(this.filter.realEstateAgent, 'listed');
        return;
      }

      this.resetForm();
      this.listings = null;
      this.total = null;
      this.submitting = true;

      if (this.filter.lat && this.filter.lng && this.filter.radius) {

        this.addressEntered = true;
        this.location = {lat:this.filter.lat, lng:this.filter.lng, radius:this.filter.radius};

        try{
          const response = await this.listingsService.getTotalAndListings(this.filter, 0);
          if (response.success) {
            this.listings = response.data.listings;
            this.total = response.data.total;
          } else {
            console.error(response.error.message);
            this.toast.error(response.error.message,'Error', this.toastOptions);
          }
        }catch(err){
          console.error(err.message || err);
          this.toast.error('Unexpected error loading properties. Try again later','Error', this.toastOptions);
        }

      } else {
        this.addressEntered = false;
      }
      this.submitting = false;

    });

    this.minPriceSub = this.itemForm.get('minPrice').valueChanges.subscribe((price)=>{

      if(this.itemForm.get('maxPrice').value<price){
        this.itemForm.get('maxPrice').setErrors({wrongPriceRange:true});
      }
    });

    this.maxPriceSub = this.itemForm.get('maxPrice').valueChanges.subscribe((price)=>{

      this.InputMinPrice.nativeElement.focus();
      this.InputMaxPrice.nativeElement.blur();
      this.InputMaxPrice.nativeElement.focus();

      this.itemForm.get('minPrice').updateValueAndValidity();
    });


    this.mapsAPILoader.load().then(() => {

      this.newAutocompleteElement(this.searchElementRef.nativeElement);

      this.newAutocompleteElement(this.searchElementRef2.nativeElement);

    });

    // var mapProp = {
    //   center: new google.maps.LatLng(18.5793, 73.8143),
    //   zoom: 15,
    //   mapTypeId: google.maps.MapTypeId.ROADMAP
    // };
    // this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    //  var marker1 = new google.maps.Marker({position: new google.maps.LatLng(18.5793, 73.8143), map: this.map,label:'L'});
    //  var marker2 = new google.maps.Marker({position: new google.maps.LatLng(18.5783, 73.8143), map: this.map,label:'B'});
    //  var markerCluster = new MarkerClusterer(this.map, [marker1,marker2],
    //         {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      
  }

  ngOnDestroy(){
    this.sub.unsubscribe(); 
  }

  private newAutocompleteElement(nativeElement:HTMLInputElement){

    let autocomplete = new google.maps.places.Autocomplete(nativeElement, {
      types: ["geocode"]
    });

    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {

        let place: google.maps.places.PlaceResult = autocomplete.getPlace();

        if(!place.geometry) return;

        let center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
        let corner = new google.maps.LatLng(place.geometry.viewport.getNorthEast().lat(), place.geometry.viewport.getNorthEast().lng())
        let _radius = new BigNumber(google.maps.geometry.spherical.computeDistanceBetween(center, corner)).div(1000); //in Km
        let radius = _radius.times(1.1).decimalPlaces(3).toNumber(); // + 10%

        this.router.navigate(['/listings/all'], {
          queryParams: {
            lat: place.geometry.location.lat(), 
            lng: place.geometry.location.lng(),
            radius:radius
          },
          queryParamsHandling: 'merge' });

        nativeElement.value = ''


      });
    });
  }


  public get filterUsed(){
    for(let key of Object.keys(this.filter)){
      if(key !='lat' && key!='lng' && key!='radius') return true;
    }
    return false;
  }

  public isRecommended(recommendedUntil:number){
    return recommendedUntil >= Date.now();
  }


  public selectPropertyType(type:string){
    this.itemForm.get('type').setValue(type);
  }

  public selectnumOfnumOfBeds(numOfBeds:string){
    this.itemForm.get('numOfBeds').setValue(numOfBeds);
  }

  public selectnumOfBaths(numOfBaths:string){
    this.itemForm.get('numOfBaths').setValue(numOfBaths);
  }

  public selectAmenities(amenity: string) {
    let index = this.amenities.findIndex(item => item.name == amenity);
    this.amenities[index].selected = !this.amenities[index].selected;
  }

  public clearFilters(){
    this.filterModal.hide();
    this.router.navigate(['/listings/all'], { queryParams: this.location});
  }

  resetForm(){
    this.itemForm.get('minPrice').setValue(this.filter.minPrice);
    this.itemForm.get('maxPrice').setValue(this.filter.maxPrice);
    this.itemForm.get('type').setValue(this.filter.type);
    this.itemForm.get('numOfBeds').setValue(this.filter.numOfBeds);
    this.itemForm.get('numOfBaths').setValue(this.filter.numOfBaths);

    if(this.filter.amenities){
      let amenitiesArray = this.filter.amenities.split(',');
      this.amenities = this.amenities.map(amenity=>{
        if( amenitiesArray.find(amenityID=>amenityID==amenity.id) ) amenity.selected = true;
        else amenity.selected = false;
        return amenity;
      });

    }else{
      this.amenities = this.amenities.map(amenity=>{
        amenity.selected = false;
        return amenity;
      });
    }
  }

  async onSubmit(){
    this.filterModal.hide();
    let filterValues = this.itemForm.value;
    filterValues.amenities ='';
    for (let amenity of this.amenities) {
      if (amenity.selected) filterValues.amenities+=`,${amenity.id}`;
    }
    filterValues.amenities = filterValues.amenities.substring(1);
    Object.keys(filterValues).forEach(key => !filterValues[key] ? delete filterValues[key] : '');
    this.router.navigate(['/listings/all'], { queryParams: filterValues, queryParamsHandling: 'merge' });
  }


}
