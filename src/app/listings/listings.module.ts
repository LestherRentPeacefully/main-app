/* Angular 2 modules */

import { NgModule } from '@angular/core';


/* Google maps module */

import { AgmCoreModule } from '@agm/core';

import {environment} from '../../environments/environment';


/* Routes */

import { ListingsRoutingModule } from './listings-routing.module';



/* Shared modules */

import { SharedModule } from '../shared/shared.module';


/* Components */

import { ListingsComponent } from './listings.component';
import { NewComponent } from './new/new.component';
import { DetailsComponent } from './details/details.component';
import { AllComponent } from './all/all.component';
import { EditComponent } from './edit/edit.component';
import { MineComponent } from './mine/mine.component';

@NgModule({
  declarations: [ListingsComponent, NewComponent, DetailsComponent, AllComponent, EditComponent, MineComponent],
  imports: [
    SharedModule,
    AgmCoreModule.forRoot({apiKey: environment.googleMaps.apiKey, libraries: ["places","geometry"]}),
    ListingsRoutingModule
  ]
})
export class ListingsModule { }
