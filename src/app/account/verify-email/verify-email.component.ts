import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AccountService} from '../account.service';
import {ToastService} from 'ng-uikit-pro-standard';
import {SharedService} from '../../shared/shared.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {
  public sub:any;
  public user;
  public itemForm: any;
  public errorMessage:string;
  public state;
  
  constructor(public accountService:AccountService,
		  			  private route: ActivatedRoute,
              public sharedService:SharedService) { }

  ngOnInit() {
  	this.user={};
  	this.sub = this.route
  		.queryParams
  		.subscribe(params => {
  			if (params['code']) {
  				this.user.code = params['code'];
  				this.accountService.verifyEmail(this.user)
  					.then(response=>{
  						console.log(response);
  						if (response.success) { 
  							this.state = 1;
  						} else {
  							this.state = 2;
  							this.errorMessage = response.error.message;
  						}
  					}).catch(err=>{
  						this.state = 3;
              console.error(err.message);
              this.errorMessage = "Unexpected error. Try again later";
  					});
  			 }else{
  			 	this.state = 4;
  			 	this.errorMessage ="No code detected";
  			}
  		});


  }

  ngOnDestroy(){
    if(this.sub){
      this.sub.unsubscribe();
    }
  }

}