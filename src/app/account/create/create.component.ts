import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router }  from '@angular/router';
import {ValidationsService} from '../../validations.service';
import {AccountService} from '../account.service';
import {ToastService} from 'ng-uikit-pro-standard';
import {SharedService} from '../../shared/shared.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
	public submiting=false;
	public captchaProcessed=false;
	public status = 0;
  private toastOptions = this.sharedService.toastOptions;
  private repeatPasswordSubscriber;
  private passwordSubscriber;
  // private roleSubscriber;
  public recapchaKey = environment.recapchaKey;
  public roleOptions = this.sharedService.roleOptions;

	public itemForm = this.fb.group({
    role: ['',[ Validators.required ]],
    firstName: ['',[ Validators.required ]],
    lastName: ['',[ Validators.required ]],
    email: ['',[ Validators.required, Validators.email ], [this.validationService.uniqueEmail.bind(this)]],
    brokerage: [''],
    licenceNo: [''],
    password: ['',[ Validators.required, this.validationService.passwordValid() ]],
    repeatPassword: ['',[ Validators.required ]]
  });

  @ViewChild('password') inputPassword: ElementRef<HTMLInputElement>;
  @ViewChild('repeatPassword') inputRepeatPassword: ElementRef<HTMLInputElement>;

  constructor(private router: Router,
  						private fb: FormBuilder,
  						private accountService:AccountService,
  						private validationService:ValidationsService,
  						public sharedService:SharedService,
  						private toast: ToastService) {


  }

  ngOnInit() {

    // this.roleSubscriber = this.itemForm.get('role').valueChanges.subscribe((value)=>{

    //   if (this.role=='real estate agent') {
    //     this.itemForm.get('brokerage').setValidators(Validators.required);
    //     this.itemForm.get('licenceNo').setValidators(Validators.required);
    //   }else{
    //     this.itemForm.get('brokerage').clearValidators();
    //     this.itemForm.get('licenceNo').clearValidators();
    //   }

    //   this.itemForm.get('brokerage').updateValueAndValidity();
    //   this.itemForm.get('licenceNo').updateValueAndValidity();

    // });


    this.repeatPasswordSubscriber = this.itemForm.get('repeatPassword').valueChanges.subscribe((value)=>{

      if(this.itemForm.get('password').value!=value){
        this.itemForm.get('repeatPassword').setErrors({differentPassword:true});
      }
    });


    this.passwordSubscriber = this.itemForm.get('password').valueChanges.subscribe((value)=>{

      this.inputRepeatPassword.nativeElement.focus();
      this.inputPassword.nativeElement.blur();
      this.inputPassword.nativeElement.focus();

      this.itemForm.get('repeatPassword').updateValueAndValidity();
    });

  }


  ngOnDestroy(){
    this.repeatPasswordSubscriber.unsubscribe();
    this.passwordSubscriber.unsubscribe();
    // this.roleSubscriber.unsubscribe();
  }


  public async createAccount(){
    let req = this.itemForm.value;
    req.role = this.role;
    if(this.role!='real estate agent') delete req.brokerage, delete req.licenceNo;
    
  	this.submiting = true;
    this.itemForm.disable();

    try{
      let response = await this.accountService.createAccount(req);
      if (response.success) { 
        this.status = 1;
      } else {
        this.toast.error(response.error.message,'', this.toastOptions);
      }
    }catch(err){
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }

    this.submiting = false;
    this.itemForm.enable();

  }

  get role(){
    return this.roleOptions.find(item=>item.value==this.itemForm.get('role').value).role;
  }


	public handleCorrectCaptcha(e){
    this.captchaProcessed = true;
  } 


  public emailErrorMessage():string{
    if (this.itemForm.get('email').hasError('email')) {
      return 'Invalid format';
    }else if(this.itemForm.get('email').hasError('emailTaken')){
      return 'Email already in use';
    } else if(this.itemForm.get('email').hasError('required')){
      return 'This field is required';
    } else{
      return ' ';
    }
  }

  public passwordErrorMessage():string{
      if (this.itemForm.get('password').hasError('passwordInvalid')) {
        return 'The password must contain at least: 8 characters, 1 capital letter and 1 number';
      } else if(this.itemForm.get('password').hasError('required')){
        return 'This field is required';
      } else{
        return ' ';
      }
  }

  public repeatPasswordErrorMessage():string{
      if(this.itemForm.get('repeatPassword').hasError('required')){
        return 'This field is required';
      } else if(this.itemForm.get('repeatPassword').hasError('differentPassword')){
        return 'Passwords must match';
      } else{
        return ' ';
      }
  }


}