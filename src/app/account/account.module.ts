/* Angular 2 modules */

import { NgModule } from '@angular/core';


/*Routes*/

import {accountRoutingModule} from './accountRoutes';


/* Shared modules */

import { SharedModule } from '../shared/shared.module';


/* Components */

import { AccountComponent } from './account.component';
import { CreateComponent } from './create/create.component';
import { LoginComponent } from './login/login.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ResendEmailVerificationCodeComponent } from './resend-email-verification-code/resend-email-verification-code.component';
import { ForgotYourPasswordComponent } from './forgot-your-password/forgot-your-password.component';
import { RecoverAccountComponent } from './recover-account/recover-account.component';

@NgModule({
  imports: [
    SharedModule,
    accountRoutingModule
  ],
  declarations: [AccountComponent, 
                 CreateComponent, 
                 LoginComponent, 
                 VerifyEmailComponent, 
                 ResendEmailVerificationCodeComponent, 
                 ForgotYourPasswordComponent, 
                 RecoverAccountComponent
                 ]
})
export class AccountModule { }
