import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

/*
Components
*/
import {AccountComponent} from './account.component';
import {CreateComponent} from './create/create.component';
import {LoginComponent} from './login/login.component';
import {VerifyEmailComponent} from'./verify-email/verify-email.component';
import {ResendEmailVerificationCodeComponent} from'./resend-email-verification-code/resend-email-verification-code.component';
import { ForgotYourPasswordComponent } from './forgot-your-password/forgot-your-password.component';
import { RecoverAccountComponent } from './recover-account/recover-account.component';

const appRoutes:Routes = [
	{
		path:'', //account
		component:AccountComponent,
		children:[
			{
				path:'create',
				component:CreateComponent
			},
			{
				path:'login',
				component:LoginComponent
			},
			{
        path:'verify-email',
        component:VerifyEmailComponent
      },
      {
        path:'resend-email-verification-code',
        component:ResendEmailVerificationCodeComponent
      },{
        path:'forgot-your-password',
        component:ForgotYourPasswordComponent
      },{
        path:'reset-password',
        component:RecoverAccountComponent
      }
		]
	}
]



@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class accountRoutingModule {}