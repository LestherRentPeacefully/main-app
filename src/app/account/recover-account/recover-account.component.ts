import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AccountService} from '../account.service';
import {ToastService} from 'ng-uikit-pro-standard';
import {SharedService} from '../../shared/shared.service';
import {environment} from '../../../environments/environment';
import { Validators, FormBuilder } from '@angular/forms';
import {ValidationsService} from '../../validations.service';

@Component({
  selector: 'app-recover-account',
  templateUrl: './recover-account.component.html',
  styleUrls: ['./recover-account.component.css']
})
export class RecoverAccountComponent implements OnInit {
  public sub:any;
  public text:string;
  public state;
  public submiting;
  private toastOptions = this.sharedService.toastOptions;
  public errorMessage;

  public itemForm = this.fb.group ({
        password: ['', [ Validators.required, this.validationService.passwordValid()]],
        code: ['', [ Validators.required ]],
    });
  constructor(public accountService:AccountService,
		  			  private route: ActivatedRoute,
		  			  private fb: FormBuilder,
              public sharedService:SharedService,
              private validationService:ValidationsService,
		          private toast: ToastService) { }

  ngOnInit() {
  	this.sub = this.route.queryParams.subscribe(params => {
  		if (params['code']) {
  			this.state = 0;
  			this.itemForm.get('code').setValue(params['code']);

  		}else{
  			this.state = 1;
  			this.errorMessage = 'No code detected';
  		}
  	});

  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  async onSubmit(){
  	this.submiting = true;
  	let req = this.itemForm.value;
  	this.itemForm.disable();
  	try{
  		let response = await this.accountService.resetPassword(req);
  		if (response.success) { 
  			this.state = 2;
  		} else {
  			this.state = 3;
  			this.submiting = false;
        this.itemForm.enable();
        this.errorMessage = response.error.message
        this.toast.error(response.error.message,'', this.toastOptions);
  		}
  	}catch(err){
  		this.submiting = false;
      this.itemForm.enable();
      console.error(err.message);
      this.toast.error('Unexpected error occured. Try again later','', this.toastOptions);
  	}
  }



  public newPasswordErrorMessage():string{
		  if (this.itemForm.get('password').hasError('passwordInvalid')) {
		    return 'The password must contain at least: 8 characters, 1 capital letter and 1 number';
		  } else if(this.itemForm.get('password').hasError('required')){
		    return 'This field is required';
		  } else{
        return ' ';
      }
  }

}
