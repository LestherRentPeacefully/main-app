import { Component, OnInit } from '@angular/core';
import {AccountService} from '../account.service';
import { Validators, FormBuilder } from '@angular/forms';
import {ValidationsService} from '../../validations.service';
import {  Router } from '@angular/router';
import {ToastService} from 'ng-uikit-pro-standard';
import {SharedService} from '../../shared/shared.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-resend-email-verification-code',
  templateUrl: './resend-email-verification-code.component.html',
  styleUrls: ['./resend-email-verification-code.component.css']
})
export class ResendEmailVerificationCodeComponent implements OnInit {
  public itemForm = this.fb.group ({
        email: ['',[ Validators.required, Validators.email ],this.validationService.emailExists.bind(this)]
    });
  public status = 0;
  public submiting=false;
  public captchaProcessed=false;
  private toastOptions = this.sharedService.toastOptions;
  public recapchaKey = environment.recapchaKey;

  constructor(private accountService:AccountService,
		  			  private router: Router,
              private fb: FormBuilder,
		  			  private validationService:ValidationsService,
              public sharedService:SharedService,
		          private toast: ToastService) {


  }

  ngOnInit() {
  }

  public async sendEmailVerificationCode(){
  		this.submiting = true;
      let req = this.itemForm.value;
  		this.itemForm.disable();
      try{

        let response = await this.accountService.resendEmailVerificationCode(req);
        if (response.success) {
          this.status = 1;
        } else {
          this.submiting=false;
          this.itemForm.enable();
          this.toast.error(response.error.message,'', this.toastOptions);
        }

      }catch(err){
        this.submiting=false;
        this.itemForm.enable();
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }
  }

  public resend(){
    this.status = 0;
    this.submiting=false;
    this.itemForm.enable();
  }


  public handleCorrectCaptcha(e){
    this.captchaProcessed = true;
  }


  public emailErrorMessage():string{
    if (this.itemForm.get('email').hasError('email')) {
      return 'Invalid format';
    }else if(this.itemForm.get('email').hasError('emailNotFound')){
      return 'Email not found';
    } else if(this.itemForm.get('email').hasError('required')){
      return 'This field is required';
    } else{
      return ' ';
    }
  }

}
