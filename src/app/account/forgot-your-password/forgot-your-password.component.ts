import { Component, OnInit } from '@angular/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {SharedService} from '../../shared/shared.service';
import {environment} from '../../../environments/environment';
import { Validators, FormBuilder } from '@angular/forms';
import {AccountService} from '../account.service';
import {ValidationsService} from '../../validations.service';

@Component({
  selector: 'app-forgot-your-password',
  templateUrl: './forgot-your-password.component.html',
  styleUrls: ['./forgot-your-password.component.css']
})
export class ForgotYourPasswordComponent implements OnInit {
  public itemForm = this.fb.group ({
        email: ['',[ Validators.required, Validators.email ], this.validationService.emailExists.bind(this)]
    });
  public status = 0;
  public submiting=false;
  public captchaProcessed=false;
  private toastOptions = this.sharedService.toastOptions;
  public recapchaKey = environment.recapchaKey;

  constructor(private accountService:AccountService,
              private fb: FormBuilder,
		  			  private validationService:ValidationsService,
              public sharedService:SharedService,
		          private toast: ToastService) { }

  ngOnInit() {
  }


  public async onSubmit(){
  	this.submiting = true;
  	let req = this.itemForm.value;
  	this.itemForm.disable();
  	try{
  		let response = await this.accountService.sendAndUploadResetPasswordCode(req);

      if (response.success) {
        this.status = 1;
      } else {
        this.submiting=false;
        this.itemForm.enable();
        this.toast.error(response.error.message,'', this.toastOptions);
      }

      }catch(err){
        this.submiting=false;
        this.itemForm.enable();
        console.error(err.message);
        this.toast.error('Unexpected error occured. Try again later','', this.toastOptions);
      }

  }


  public resend(){
    this.status = 0;
    this.submiting = false;
    this.itemForm.enable();
  }


  public handleCorrectCaptcha(e){
    this.captchaProcessed = true;
  }


  public emailErrorMessage():string{
    if (this.itemForm.get('email').hasError('email')) {
      return 'Invalid format';
    }else if(this.itemForm.get('email').hasError('emailNotFound')){
      return 'Email not found';
    } else if(this.itemForm.get('email').hasError('required')){
      return 'This field is required';
    } else{
      return ' ';
    }
  }


}
