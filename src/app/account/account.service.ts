import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AccountService {

	private APIURL = environment.APIURL;

  constructor(private http: HttpClient) { }

  public createAccount(user:{firstName:string, lastName:string, email:string, password:string}): Promise<any>{
  	return this.http.post(`${this.APIURL}/account/create`, user, httpOptions).toPromise();
  }

  public verifyEmail(user:any):Promise<any>{
    return this.http.post(`${this.APIURL}/account/verifyEmail`,user,httpOptions).toPromise();
  }

  public resendEmailVerificationCode(user:any):Promise<any>{
    return this.http.post(`${this.APIURL}/account/resendEmailVerificationCode`,user,httpOptions).toPromise();
  }

  public sendAndUploadResetPasswordCode(user:{email:string}):Promise<any>{
    return this.http.post(`${this.APIURL}/account/sendAndUploadResetPasswordCode`,user,httpOptions).toPromise();
  }  

  public resetPassword(user:{code:string, password:string}):Promise<any>{
    return this.http.post(`${this.APIURL}/account/resetPassword`,user,httpOptions).toPromise();
  }

}
