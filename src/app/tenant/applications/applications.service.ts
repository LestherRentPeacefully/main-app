import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';

const db = firebase.firestore();

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService {


  constructor() { }

  public async getApplications(uid:string){
    
    let applications = [];
    let applicationSnapshot = await db.collection('applications')
                                      .where('applicant.uid','==',uid)
                                      .orderBy('creationTS','desc').get();
    if(!applicationSnapshot.empty){
      for(let applicationDoc of applicationSnapshot.docs){
        let application = applicationDoc.data();
        // delete application.listing;
        applications.push(application);
      }
     }
     return applications;

  }
  



}
