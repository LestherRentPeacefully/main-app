/* Angular 2 modules */

import { NgModule } from '@angular/core';


/* Routes */

import { ApplicationsRoutingModule } from './applications-routing.module';


/* Shared modules */

import { SharedModule } from '../../shared/shared.module';


/* Components */

import { ApplicationsComponent } from './applications.component';
import { AllComponent } from './all/all.component';

@NgModule({
  imports: [
    SharedModule,
    ApplicationsRoutingModule
  ],
  declarations: [ApplicationsComponent, AllComponent]
})
export class ApplicationsModule { }
