import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


/* Components */

import { ApplicationsComponent } from './applications.component';
import { AllComponent } from './all/all.component';


const routes: Routes = [{
  path:'',
  component:ApplicationsComponent,
  children:[
    {
      path:'all',
      component:AllComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }
