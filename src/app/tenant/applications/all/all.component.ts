import { Component, OnInit } from '@angular/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {AuthService} from '../../../auth.service';
import {SharedService} from '../../../shared/shared.service';
import {ApplicationsService} from '../applications.service';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {
  private toastOptions = this.sharedService.toastOptions;
  public app = environment.app;
  public userInfoSubscriber;
  public loading;
  public userInfo:any;

  public applications:any[];

  constructor(private toast: ToastService,
              private applicationsService: ApplicationsService,
              private authService:AuthService,
              public sharedService:SharedService) { }

  ngOnInit() {
    this.userInfoSubscriber = this.authService.userInfo.subscribe(async (userInfo)=>{
      this.userInfo = userInfo;

      if(this.userInfo){
        try{
          this.loading = true;
          this.applications = await this.applicationsService.getApplications(this.userInfo.uid);
          this.loading = false;
        }catch(err){
          console.error(err.message || err);
          this.toast.error('Unexpected error loading applications. Try again later','', this.toastOptions);
          this.loading = false;
        }
      }
    });
  }

  ngOnDestroy(){
    this.userInfoSubscriber.unsubscribe();
  }

  public applicationDate(timestamp:number) : string {
    let date = new Date(timestamp);
    return `${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()}`;
  }


}
