import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

/*
Components
*/

import {HelpComponent} from './help.component';
import {FAQComponent} from './faq/faq.component';
import { PricingComponent } from './pricing/pricing.component';

const appRoutes:Routes = [
	{
		path:'',
		component:HelpComponent,
    children:[
      {
        path:'FAQ',
        component:FAQComponent
      },
      {
        path:'pricing',
        component:PricingComponent
      }
    ]
	}
]



@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class helpRoutingModule {}