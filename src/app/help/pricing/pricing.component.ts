import { Component, OnInit, ViewChild } from '@angular/core';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../auth.service';
import {ToastService, ModalDirective} from 'ng-uikit-pro-standard';
import {SharedService} from '../../shared/shared.service';
import {BehaviorSubject} from 'rxjs';
import {PricingService} from '../../shared/pricing.service';
import {BigNumber} from 'bignumber.js';
import { Router }  from '@angular/router';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {
  public app = environment.app;
  public userInfo$:BehaviorSubject<any>;
  public currencies:any[];
  public pricingSelected: {plan: string, amount: number};
  public currencySelected:any;
  public amountToPay:string;
  private toastOptions = this.sharedService.toastOptions;
  public buying;
  public canceling;

  public pricings: any;

  @ViewChild('pricingModal') public pricingModal:ModalDirective;
  @ViewChild('cancelationModal') public cancelationModal:ModalDirective;

  constructor(
    private authService:AuthService, 
    private toast: ToastService,
    private sharedService:SharedService,
    private pricingService:PricingService,
    private router: Router) { }

  ngOnInit() {
    this.userInfo$ = this.authService.userInfo;
    this.pricings = this.sharedService.pricings;
  }
  
  async selectPlan(plan:string){
    this.pricingModal.show();
    this.pricingSelected = {plan, amount: this.pricings[plan].amount};
    this.currencies = await this.sharedService.getCurrencies();
  }

  async selectCurrency(item:any){
    if(!this.buying && (!this.currencySelected || this.amountToPay)){

      this.currencySelected = item;
      this.amountToPay = null;
      try {
        let response = await this.pricingService.getCurrencyPrice({currency:this.currencySelected.currency});
        if (response.success) {
          this.amountToPay = new BigNumber(this.pricingSelected.amount).div(response.data.price.USD).decimalPlaces(8).toString(10);
          
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'',this.toastOptions);
        }
        
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }
    }
  }

  closed(){
    this.pricingSelected = null;
    this.currencies = null;
    this.currencySelected = null;
  }

  public formatBalance(value){
    return this.sharedService.formatBalance(value, 0); 
  }

  async buyPricingPlan(){

    let userInfo = this.userInfo$.getValue();
    
    if (userInfo.balances[this.currencySelected.currency] && new BigNumber(userInfo.balances[this.currencySelected.currency].available).isGreaterThanOrEqualTo(this.amountToPay)) {
      
      this.buying = true;
      try {
        let response = await this.pricingService.buyPricingPlan({currency:this.currencySelected.currency, plan:this.pricingSelected.plan});
        if (response.success) {

          this.pricingModal.hide();

          userInfo.membership = {
            pricingPlan: this.pricingSelected.plan,
            paymentMethod:{
              currency: this.currencySelected.currency,
              name: this.currencySelected.name,
            },
            nextPayment: this.addMonths(new Date(), 1).getTime()
          }

          // userInfo.balances[this.currencySelected.currency].available = 
            // new BigNumber(userInfo.balances[this.currencySelected.currency].available).minus(this.amountToPay).toString(10);

          this.userInfo$.next(userInfo);

          this.toast.success(`You're now a ${this.pricingSelected.plan} Member`,'Pricing Plan purchased', this.toastOptions);
          this.router.navigate(['/user/settings'], { queryParams: {panel:3} });

        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'',this.toastOptions);
        }

      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }
      this.buying = false;

    } else {
      this.toast.error('Insufficient funds','', this.toastOptions);
    }
  }


  private addMonths (oldDate:number | Date, count:number) {
    const date = new Date(oldDate);
    const day = date.getDate();
    date.setMonth(date.getMonth() + count, 1);
    const month = date.getMonth();
    date.setDate(day);
    if (date.getMonth() !== month) date.setDate(0);
    return date;
  }

  public openCancelationModal(){
    this.cancelationModal.show();
  }

  public async cancelPricingPlan(){
    this.canceling = true;
    let userInfo = this.userInfo$.getValue();
    
    try {
      let response = await this.pricingService.cancelPricingPlan();

      if (response.success) {

        this.cancelationModal.hide();

        userInfo.membership.pricingPlan = 'free';
        this.userInfo$.next(userInfo);

        this.toast.success(`You're now a free Member`,'Pricing Plan downgraded', this.toastOptions);
        this.router.navigate(['/user/settings'], { queryParams: {panel:3} });

      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'',this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.canceling = false;
  }



}
