/* Angular 2 modules */

import { NgModule } from '@angular/core';

/* Routes */

import {helpRoutingModule} from './helpRoutes';


/* Shared modules */

import { SharedModule } from '../shared/shared.module';


/* Components */

import { HelpComponent } from './help.component';
import { FAQComponent } from './faq/faq.component';
import { PricingComponent } from './pricing/pricing.component';


@NgModule({
  imports: [
    SharedModule,
    helpRoutingModule
  ],
  declarations: [HelpComponent, FAQComponent, PricingComponent]
})
export class HelpModule { }
