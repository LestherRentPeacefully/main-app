import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subscription, combineLatest} from 'rxjs';
import { Router }  from '@angular/router';
import {ToastService} from 'ng-uikit-pro-standard';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import {SharedService} from './shared/shared.service';
import { filter } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

	public userInfo = new BehaviorSubject(undefined);
  public authenticated = new BehaviorSubject(undefined);
  public notification$ = new BehaviorSubject(undefined);
	public userSession:Subscription;
  public logginSession:Subscription;
  public userSnapshot:any;
  private toastOptions = this.sharedService.toastOptions;
  public unreadNotifications;
  public redirectUrl: string;
  

  constructor(private router: Router,
  						public afAuth: AngularFireAuth,
              public afs: AngularFirestore,
              public sharedService:SharedService,
  						private toast: ToastService) {

  	this.logginSession = this.stateChanged().subscribe(user => {
      if(!user) {
        this.authenticated.next(false);
        this.userInfo.next(false);
        return;
      }

      this.authenticated.next(true);

      this.userSession = this.getUser(user.uid).pipe(filter((a)=> !!a[0] && !!a[1] && !!a[2]))
        .subscribe(async snapshot => {
          
          this.userInfo.next({
            ...snapshot[0],
            role: (<any>snapshot[1]).role, 
            preferences: snapshot[1],
            balances: this.formatBalances(snapshot[2])
          });
          
          const unreadNotifications = (<any>snapshot[1]).counters.unreads.notifications;
          const thereAreNewNotifications = unreadNotifications>0 && this.unreadNotifications!=unreadNotifications;

          if(!thereAreNewNotifications) return;

          this.unreadNotifications = unreadNotifications;
          const notification = await this.sharedService.getLastNotification((<any>snapshot[0]).uid);
          this.notification$.next(notification);
          this.toast.info(`${notification.content.substr(0, 60)}...`, notification.title, this.toastOptions);
            

        }, (err)=>{
          console.error(err.message || err);
          this.toast.error('Unexpected error. Login again','', this.toastOptions);
          this.logout();
        }
      );
  	});

  }



  public async login(user:{email:string, password:string}){
    await this.afAuth.auth.setPersistence('session'); //local session none
    const uid = (await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)).user.uid;
    
    let redirect = this.redirectUrl ? this.redirectUrl : `/user/${uid}`;
    this.router.navigateByUrl(redirect);
    this.redirectUrl = null;
  }

  public async logout(){
    await this.afAuth.auth.signOut();
    this.userSession.unsubscribe();
    this.toast.success('Logged out successfully','', this.toastOptions);
    this.router.navigate(['/']);
  }

  public stateChanged(): Observable<any>{
  	return new Observable(observer=> this.afAuth.auth.onAuthStateChanged(user => observer.next(user)));
  }

  private getUser(uid:string){
    return combineLatest(
      this.afs.doc(`users/${uid}`).valueChanges(),
      this.afs.doc(`users/${uid}/settings/preferences`).valueChanges(),
      this.afs.collection(`balances`, ref=>ref.where('uid','==',uid)).valueChanges()
    );
  }

  private formatBalances(balances:any[]){
    const balanceObj = {};
    for(let balance of balances){
      balanceObj[balance.currency] = balance;
    }
    return balanceObj;
  }


}
