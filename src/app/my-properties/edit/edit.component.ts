import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { ToastService, ModalDirective } from 'ng-uikit-pro-standard';
import { AuthService } from '../../auth.service';
import { ActivatedRoute } from '@angular/router';
import { SharedService } from '../../shared/shared.service';
import * as Cropper from 'cropperjs/dist/cropper.js';
import { environment } from '../../../environments/environment';
import {MyPropertiesService} from '../my-properties.service';
import { Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import * as geofirex from 'geofirex';
import * as firebase from 'firebase/app';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  private sub;
  private toastOptions = this.sharedService.toastOptions;
  public submiting;
  public userInfo$: BehaviorSubject<any>;
  public app = environment.app;
  public myPropertyID:string;

  public propertyOptions = this.sharedService.propertyOptions;

  public addPhotoLogoURL = './assets/addPhoto2.png';
  public file:any = {};
  private oldFiles:string[] = [];
  public imageToCrop;
  public imageCropped = {
    localURL: [],
    uploading: [],
    deleting: [],
    uploadFilePercentage: [],
    filePath: []
  }

  public myProperty;

  public cropper;

  public itemForm = this.fb.group({
    name: ['', [Validators.required, Validators.maxLength(50)]],
    address: ['', [Validators.required,]],
    location: this.fb.group({
      lat: ['', [Validators.required,]],
      lng: ['', [Validators.required,]],
    }),
    unit: ['', []],
    type: ['', [Validators.required]],
    // typeOfContract:['traditional contract', [Validators.required,]],
    photos: this.fb.array([
      this.fb.control('', Validators.required)
    ])
  });

  @ViewChild("search") public searchElementRef: ElementRef;
  @ViewChild('cropImageModal') public cropImageModal: ModalDirective;
  @ViewChild('imageToCrop') public imageToCropElementRef: ElementRef;



  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private toast: ToastService,
    public sharedService: SharedService,
    private mapsAPILoader: MapsAPILoader,
    private MyPropertiesService: MyPropertiesService,
    private route: ActivatedRoute,
    private router: Router,
    private ngZone: NgZone) { }



  async ngOnInit() {

    this.userInfo$ = this.authService.userInfo;


    this.sub = this.route.paramMap.subscribe(async (params)=>{
      this.myPropertyID = params.get('id');
      this.myProperty = await this.MyPropertiesService.getMyProperty(this.myPropertyID);

      this.unformatFields();

      this.itemForm.patchValue(this.myProperty);

      this.imageCropped.localURL.push(...this.photos.value);
      this.imageCropped.filePath.push(...(<string[]>this.photos.value).map(value=>this.sharedService.photoPathFromURL(value)));


      // this.itemForm.get("typeOfContract").disable();


      await this.mapsAPILoader.load();
      
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["geocode"]
      });

      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
  
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
  
          if (place.geometry) {
  
            this.itemForm.get('address').setErrors(null);
            this.itemForm.get('address').setValue(this.searchElementRef.nativeElement.value);
  
            this.itemForm.get('location').setValue({
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng()
            });
  
  
          } else {
            this.itemForm.get('address').setErrors({ wrongAddress: true });
          }
  
        });
      });



    });

  }


  ngOnDestroy(){
    this.sub.unsubscribe();
  }



  get photos() {
    return this.itemForm.get('photos') as FormArray;
  }


  onShown() {
    this.cropper = new Cropper(this.imageToCrop, {
      aspectRatio: 1.5,
    });
  }


  onHidden() {
    this.cropper.destroy();
    this.cropper = null;
  }


  addPhoto(e, index) {

    this.imageToCrop = this.imageToCropElementRef.nativeElement;
    let files = e.target.files as File[];
    
    if (files && files.length > 0) {

      let file = files[0];
      if (!file.type.match(/image-*/)) {
        this.toast.error('', 'Invalid format', this.toastOptions);
      } else {
        this.file.type = file.type;

        let reader = new FileReader();

        reader.onloadend = (e) => {
          let url = reader.result;
          this.imageToCrop.src = url;
          this.cropImageModal.show();
        };
        reader.readAsDataURL(file);

      }
    }
    
    e.target.value = '';
  }
  


  uploadNewPhoto() {
    let canvas;

    if (this.cropper) {

      canvas = this.cropper.getCroppedCanvas({
        // width: 220,
        // height: 220,
      });

      canvas.toBlob(file => {

        this.cropImageModal.hide();

        let index = 0;

        this.imageCropped.localURL.push(canvas.toDataURL(this.file.type));

        let fileName = this.sharedService.newFileName(file.type);

        let subscriber = this.MyPropertiesService.uploadPhoto(this.userInfo$.getValue().uid, file, fileName).subscribe((uploadFilePercent) => {

          this.imageCropped.uploading[index] = true;
          this.imageCropped.uploadFilePercentage[index] = uploadFilePercent;

        }, (err) => {

          this.imageCropped.uploading[index] = false;
          this.imageCropped.localURL.splice(index, 1);
          this.photos.removeAt(index);
          if (this.photos.length < 1) this.photos.push(this.fb.control('', Validators.required)); else this.photos.push(this.fb.control(''));
          this.photos.updateValueAndValidity();
          console.error(err.message || err);
          this.toast.error('', 'Error uploading photo. Try again later', this.toastOptions);

        }, async () => {

          subscriber.unsubscribe();

          try {
            this.photos.controls[index].setValue(await this.MyPropertiesService.getImageURL(this.userInfo$.getValue().uid, fileName));
            this.imageCropped.uploading[index] = false;
            this.imageCropped.filePath[index] = this.MyPropertiesService.getFilePath(this.userInfo$.getValue().uid, fileName);

          } catch (err) {
            this.imageCropped.uploading[index] = false;
            this.imageCropped.localURL.splice(index, 1);
            this.photos.removeAt(index);
            if (this.photos.length < 1) this.photos.push(this.fb.control('', Validators.required)); else this.photos.push(this.fb.control(''));
            this.photos.updateValueAndValidity();
            console.error(err.message || err);
            this.toast.error('', 'Error uploading photo. Try again later', this.toastOptions);
          }

        });


      }, this.file.type);

    }

  }


  async deletePhoto(index) {
    this.imageCropped.deleting[index] = true;

    if (index > this.myProperty.photos.length-1) {
      await this.sharedService.deletePhoto(this.imageCropped.filePath[index]);
    } else {
      this.oldFiles.push(this.sharedService.photoPathFromURL(this.myProperty.photos[index]));
      this.myProperty.photos.splice(index, 1);
    }

    this.imageCropped.localURL.splice(index, 1);
    this.imageCropped.filePath.splice(index, 1);
    this.photos.removeAt(index);
    if (this.photos.length < 1) this.photos.push(this.fb.control('', Validators.required));
    this.photos.updateValueAndValidity();
    this.imageCropped.deleting[index] = false;
  }

  get photosUploading() {
    return this.imageCropped.uploading.find(uploading => uploading);
  }

  get photosDeleting() {
    return this.imageCropped.deleting.find(uploading => uploading);
  }



  private unformatFields(){
    this.myProperty.type = this.propertyOptions.find(item => item.label == this.myProperty.type).value;

    this.myProperty.location = {
      lat:this.myProperty.location.geopoint.latitude,
      lng:this.myProperty.location.geopoint.longitude
    }
  }

  private formatFields(fields: any) {
    let req = fields;
    req.type = this.propertyOptions.find(item => item.value == req.type).label;

    const geo = geofirex.init(firebase);
    req.location.geohash = geo.point(req.location.lat, req.location.lng).hash;

    return req;
  }




  async onSubmit() {
    this.submiting = true;

    let req = this.formatFields(this.itemForm.value);
    // req.typeOfContract = 'traditional contract';

    this.itemForm.disable();

    try {
      const response = await this.MyPropertiesService.edit(req, this.myPropertyID, this.oldFiles);

      if (response.success) {
        this.toast.success('Property updated','', this.toastOptions);
        this.router.navigate([`/my-properties/details/${this.myPropertyID}`]);
        
      } else {
        this.toast.error('', response.error.message, this.toastOptions);
      }
    } catch (err) {
      this.toast.error('', 'Error submitting request. Try again later', this.toastOptions);
      console.error(err);
    }
    
    this.submiting = false;
    this.itemForm.enable();
    // this.itemForm.get("typeOfContract").disable();

  }

}
