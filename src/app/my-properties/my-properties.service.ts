import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AngularFireStorage } from '@angular/fire/storage';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import {SharedService} from '../shared/shared.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {ToastService} from 'ng-uikit-pro-standard';
import { PropertySignatureRequest } from '../shared/interfaces';
// import * as ipfsClient from 'ipfs-http-client';
// const ipfs = ipfsClient({ host: 'ipfs.infura.io', port: '5001', protocol: 'https' });

// declare let IpfsHttpClient:any;

@Injectable({
  providedIn: 'root'
})
export class MyPropertiesService {

  private APIURL = environment.APIURL;

  private toastOptions = this.sharedService.toastOptions;

  constructor(
    private storage: AngularFireStorage,
    private http: HttpClient,
    private afs:AngularFirestore,
    private sharedService:SharedService,
    private toast: ToastService,
    public afAuth: AngularFireAuth) {

    }


  public getFilePath(uid:string, fileName:string){
    return `users/${uid}/myProperties/images/${fileName}`;
  }

  public uploadPhoto(uid:string, file:File, fileName:string){
    let fileRef = this.storage.ref(this.getFilePath(uid, fileName));
    let task = fileRef.put(file, {customMetadata:{fileName: file.name }});
    return task.snapshotChanges().pipe(map(snapshot=>(snapshot.bytesTransferred / snapshot.totalBytes) * 100));
  }

  public getImageURL(uid:string, fileName:string){
    let fileRef = this.storage.storage.ref(this.getFilePath(uid, fileName));
    return fileRef.getDownloadURL();
  }

  async new(fields): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/my-properties/new`, fields, _httpOptions).toPromise();
  }

  async getAllMyProperties(uid:string, role:string){
    const db = firebase.firestore();
    let snapshot:firebase.firestore.QuerySnapshot;
    let docs = [];
    if(role=='landlord'){
      snapshot = await db.collection('myProperties').where('user.uid','==',uid).get();
      snapshot.forEach(doc=> docs.push(doc.data()));

    } else if(role=='tenant'){
      snapshot = await db.collection('myProperties').where('tenantsID','array-contains',uid).get();
      snapshot.forEach(doc=> docs.push(doc.data()));
    }
    return docs;
  }

  async getMyProperty(myPropertyID:string){
    const db = firebase.firestore();
    return (await db.doc(`myProperties/${myPropertyID}`).get()).data();
  }
  
  getMyProperty$(myPropertyID:string){
    return this.afs.doc(`myProperties/${myPropertyID}`).valueChanges();
  }

  async edit(fields:any, myPropertyID:string, oldFiles:string[]){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/my-properties/edit`, {...fields, myPropertyID, oldFiles}, _httpOptions).toPromise();
  }

  async delete(req:{myPropertyID:string}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/my-properties/delete`, req, _httpOptions).toPromise();
  }

  async getTenants(req:{tenantsID:string[]}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/my-properties/getTenants`, req, _httpOptions).toPromise();
  }

  async getTenantSelected(myPropertyID:string, tenantID:string){
    const db = firebase.firestore();
    return (await db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}`).get()).data();
  }

  async getLandlord(uid:string){
    const db = firebase.firestore();
    return (await db.doc(`users/${uid}`).get()).data();
  }

  public async getArrayOfSignatures(req:{signaturesRequestID:string[]}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/my-properties/getArrayOfSignatures`, req, _httpOptions).toPromise(); 
  }

  async getSignaturesRequest(myPropertyID:string, tenantID:string, offset:number, lastVisibleTS:number){
    const db = firebase.firestore();
    let signaturesRequest = [];

    let docs: firebase.firestore.QueryDocumentSnapshot[];

    let query = db.collection(`myProperties/${myPropertyID}/tenants/${tenantID}/signaturesRequest`).orderBy('creationTS','desc');

    if(!lastVisibleTS) {
      docs = (await query.limit(offset).get()).docs;
    } else {
      docs = (await query.startAfter(lastVisibleTS).limit(offset).get()).docs;
    }
    
    docs.forEach(doc=> signaturesRequest.push(doc.data()));
    return <PropertySignatureRequest[]>signaturesRequest;
  }

  public async getCurrencyPriceForEsign(req:{currency:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/my-properties/getCurrencyPriceForEsign`,req,_httpOptions).toPromise(); 
  }

  public async payForEsignWithCrypto(req:{currency:string, myPropertyID:string, tenantID:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/my-properties/payForEsignWithCrypto`,req,_httpOptions).toPromise(); 
  }  
  
  public async uploadDocument(req:FormData, cb1, cb2, cb3){
    const token = await this.sharedService.getToken();

    const xhr = new XMLHttpRequest();

    xhr.upload.addEventListener('progress', oEvent=> {if (oEvent.lengthComputable) cb1(oEvent.loaded / oEvent.total * 100)});
    xhr.addEventListener('load', ()=> cb2(JSON.parse(xhr.response)));
    xhr.addEventListener('error', ()=> cb3());
    
    xhr.open('POST', `${this.APIURL}/my-properties/uploadDocument`);
    xhr.setRequestHeader('Authorization', token);
    xhr.send(req);
  }

  public async getSignUrl(req:{signatureID:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/my-properties/getSignUrl`, req, _httpOptions).toPromise(); 
  }

  public async getDocUrl(req:{signatureRequestID:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/my-properties/getDocUrl`, req, _httpOptions).toPromise(); 
  }

  public async depositWithCrypto(req:{myPropertyID:string, currency:string, amount:number}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/my-properties/depositWithCrypto`, req ,_httpOptions).toPromise(); 
  }

  public async ticket(req:{myPropertyID:string, details:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/my-properties/ticket`, req, _httpOptions).toPromise(); 
  }

  public async modifyRent(req:{myPropertyID:string, tenantID:string, newRent:string}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/my-properties/modifyRent`, req, _httpOptions).toPromise(); 
  }

  async getPropertyHistory(myPropertyID:string, tenantID:string){
    const db = firebase.firestore();
    let propertyHistory = [];
    let snapshots = await db.collection(`myProperties/${myPropertyID}/tenants/${tenantID}/history`).orderBy('creationTS','desc').get();
    for(let doc of snapshots.docs){
      propertyHistory.push(doc.data());
    }
    return propertyHistory;
  }

  public async endLease(req:{myPropertyID:string, tenantID:string}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/my-properties/endLease`, req, _httpOptions).toPromise(); 
  }

  public async cancelCulmination(req:{myPropertyID:string, tenantID:string}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/my-properties/cancelCulmination`, req, _httpOptions).toPromise(); 
  }

  public async estimateGasAndGasPriceContractCreation(){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/my-properties/estimateGasAndGasPriceContractCreation`, {}, _httpOptions).toPromise(); 
  }

  public async createSmartContract(req:{ myPropertyID: string, gasPrice: number, gasLimit: number }){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:string, error:{message?:string}}>(`${this.APIURL}/my-properties/createSmartContract`, req, _httpOptions).toPromise(); 
  }

  async uploadToIPFS(req: {url: string}) {
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:string, error:{message?:string}}>(`${this.APIURL}/my-properties/uploadToIPFS`, req, _httpOptions).toPromise(); 
  }

  async estimateGasAndGasPriceDocSaving(req: {myPropertyID: string}) {
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:{estimateGas:number, gasPrice:number}, error:{message?:string}}>(`${this.APIURL}/my-properties/estimateGasAndGasPriceDocSaving`, req, _httpOptions).toPromise(); 
  }

  async saveDocOnBlockchain(req: {ipfsHash: string, myPropertyID: string, tenantID: string, signatureRequestSelectedID: string, gasPrice: number, gasLimit: number}) {
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:string, error:{message?:string}}>(`${this.APIURL}/my-properties/saveDocOnBlockchain`, req, _httpOptions).toPromise(); 
  }

  async startChat(req: {content: string, myPropertyID: string, tenantID: string}) {
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:{estimateGas:number, gasPrice:number}, error:{message?:string}}>(`${this.APIURL}/my-properties/startChat`, req, _httpOptions).toPromise(); 
  }



}
