/* Angular 2 modules */

import { NgModule } from '@angular/core';


/* Google maps module */

import { AgmCoreModule } from '@agm/core';

import {environment} from '../../environments/environment';

/* Routes */

import { MyPropertiesRoutingModule } from './my-properties-routing.module';


/* Shared modules */

import { SharedModule } from '../shared/shared.module';


/* Components */

import { MyPropertiesComponent } from './my-properties.component';
import { AllComponent } from './all/all.component';
import { DetailsComponent } from './details/details.component';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { GeneralComponent } from './details/general/general.component';
import { HistoryComponent } from './details/history/history.component';
import { ActionsComponent } from './details/actions/actions.component';
import { EsignedDocsComponent } from './details/esigned-docs/esigned-docs.component';
import { SettingsComponent } from './details/settings/settings.component';
import { SmartContractComponent } from './details/smart-contract/smart-contract.component';

@NgModule({
  declarations: [MyPropertiesComponent, AllComponent, DetailsComponent, EditComponent, NewComponent, GeneralComponent, HistoryComponent, ActionsComponent, EsignedDocsComponent, SettingsComponent, SmartContractComponent],
  imports: [
    SharedModule,
    AgmCoreModule.forRoot({apiKey: environment.googleMaps.apiKey, libraries: ["places","geometry"]}),
    MyPropertiesRoutingModule
  ]
})
export class MyPropertiesModule { }
