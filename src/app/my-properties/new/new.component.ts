import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { ToastService, ModalDirective } from 'ng-uikit-pro-standard';
import { AuthService } from '../../auth.service';
import { SharedService } from '../../shared/shared.service';
import * as Cropper from 'cropperjs/dist/cropper.js';
import { environment } from '../../../environments/environment';
import {MyPropertiesService} from '../my-properties.service';
import { Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import * as geofirex from 'geofirex';
import * as firebase from 'firebase/app';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  private toastOptions = this.sharedService.toastOptions;
  public submiting;
  public userInfo$: BehaviorSubject<any>;
  public app = environment.app;

  public propertyOptions = this.sharedService.propertyOptions;

  public addPhotoLogoURL = './assets/addPhoto2.png';
  public file:any = {};
  public imageToCrop;
  public imageCropped = {
    localURL: [],
    uploading: [],
    deleting: [],
    uploadFilePercentage: [],
    filePath: []
  }

  public cropper;

  public itemForm = this.fb.group({
    name: ['', [Validators.required, Validators.maxLength(50)]],
    address: ['', [Validators.required,]],
    location: this.fb.group({
      lat: ['', [Validators.required,]],
      lng: ['', [Validators.required,]],
    }),
    unit: ['', []],
    type: ['', [Validators.required]],
    // typeOfContract:['traditional contract', [Validators.required,]],
    photos: this.fb.array([
      this.fb.control('', Validators.required)
    ])
  });

  @ViewChild("search") public searchElementRef: ElementRef;
  @ViewChild('cropImageModal') public cropImageModal: ModalDirective;
  @ViewChild('imageToCrop') public imageToCropElementRef: ElementRef;



  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private toast: ToastService,
              public sharedService: SharedService,
              private mapsAPILoader: MapsAPILoader,
              private MyPropertiesService: MyPropertiesService,
              private router: Router,
              private ngZone: NgZone) { }

  async ngOnInit() {

    // this.itemForm.get("typeOfContract").disable();

    this.itemForm.get('address').setErrors({ wrongAddress: true });

    await this.mapsAPILoader.load();
    
    let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      types: ["geocode"]
    });

    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {

        let place: google.maps.places.PlaceResult = autocomplete.getPlace();

        if (place.geometry) {

          // console.log(place);
          this.itemForm.get('address').setErrors(null);
          this.itemForm.get('address').setValue(this.searchElementRef.nativeElement.value);

          this.itemForm.get('location').setValue({
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
          });


        } else {

          this.itemForm.get('address').setErrors({ wrongAddress: true });
        }

      });
    });



    this.userInfo$ = this.authService.userInfo;


  }



  ngOnDestroy() {
  }



  get photos() {
    return this.itemForm.get('photos') as FormArray;
  }


  onShown() {
    this.cropper = new Cropper(this.imageToCrop, {
      aspectRatio: 1.5,
    });
  }

  onHidden() {
    this.cropper.destroy();
    this.cropper = null;
  }

  addPhoto(e, index) {

    this.imageToCrop = this.imageToCropElementRef.nativeElement;
    let files = e.target.files as File[];
    
    if (files && files.length > 0) {

      let file = files[0];
      if (!file.type.match(/image-*/)) {
        this.toast.error('', 'Invalid format', this.toastOptions);
      } else {
        this.file.type = file.type;

        let reader = new FileReader();

        reader.onloadend = (e) => {
          let url = reader.result;
          this.imageToCrop.src = url;
          this.cropImageModal.show();
        };
        reader.readAsDataURL(file);

      }
    }
    
    e.target.value = '';

  }

  uploadNewPhoto() {
    let canvas;

    if (this.cropper) {

      canvas = this.cropper.getCroppedCanvas({
        // width: 220,
        // height: 220,
      });

      canvas.toBlob(file => {

        this.cropImageModal.hide();

        let index = 0;

        this.imageCropped.localURL.push(canvas.toDataURL(this.file.type));

        let fileName = this.sharedService.newFileName(file.type);

        let subscriber = this.MyPropertiesService.uploadPhoto(this.userInfo$.getValue().uid, file, fileName).subscribe((uploadFilePercent) => {

          this.imageCropped.uploading[index] = true;
          this.imageCropped.uploadFilePercentage[index] = uploadFilePercent;

        }, (err) => {

          this.imageCropped.uploading[index] = false;
          this.imageCropped.localURL.splice(index, 1);
          this.photos.removeAt(index);
          if (this.photos.length < 1) this.photos.push(this.fb.control('', Validators.required)); else this.photos.push(this.fb.control(''));
          this.photos.updateValueAndValidity();
          console.error(err.message || err);
          this.toast.error('', 'Error uploading photo. Try again later', this.toastOptions);

        }, async () => {

          subscriber.unsubscribe();

          try {
            this.photos.controls[index].setValue(await this.MyPropertiesService.getImageURL(this.userInfo$.getValue().uid, fileName));
            this.imageCropped.uploading[index] = false;
            this.imageCropped.filePath[index] = this.MyPropertiesService.getFilePath(this.userInfo$.getValue().uid, fileName);

          } catch (err) {
            this.imageCropped.uploading[index] = false;
            this.imageCropped.localURL.splice(index, 1);
            this.photos.removeAt(index);
            if (this.photos.length < 1) this.photos.push(this.fb.control('', Validators.required)); else this.photos.push(this.fb.control(''));
            this.photos.updateValueAndValidity();
            console.error(err.message || err);
            this.toast.error('', 'Error uploading photo. Try again later', this.toastOptions);
          }

        });


      }, this.file.type);

    }

  }


  async deletePhoto(index) {
    this.imageCropped.deleting[index] = true;
    try {
      await this.sharedService.deletePhoto(this.imageCropped.filePath[index]);
      this.imageCropped.localURL.splice(index, 1);
      this.imageCropped.filePath.splice(index, 1);
      this.photos.removeAt(index);
      if (this.photos.length < 1) this.photos.push(this.fb.control('', Validators.required));
      this.photos.updateValueAndValidity();

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('', 'Error deleting photo. Try again later', this.toastOptions);
    }
    this.imageCropped.deleting[index] = false;
  }

  get photosUploading() {
    return this.imageCropped.uploading.find(uploading => uploading);
  }

  get photosDeleting() {
    return this.imageCropped.deleting.find(uploading => uploading);
  }



  async onSubmit() {

    this.submiting = true;


    let req = this.formatFields(this.itemForm.value);
    // req.typeOfContract = 'traditional contract';


    this.itemForm.disable();

    try {

      let response = await this.MyPropertiesService.new(req);

      if (response.success) {
        this.toast.success('Property successfully created','', this.toastOptions);
        this.router.navigate([`/my-properties/details/${response.data}`]);
      } else {
        console.error(response.error.message);
        this.toast.error('', response.error.message, this.toastOptions);
      }

    } catch (err) {
      this.toast.error('', 'Error submitting request. Try again later', this.toastOptions);
      console.error(err);
    }
    
    this.itemForm.enable();
    // this.itemForm.get("typeOfContract").disable();
    this.submiting = false;
  }



  private formatFields(fields: any) {
    let req = fields;
    req.type = this.propertyOptions.find(item => item.value == req.type).label;

    const geo = geofirex.init(firebase);
    req.location.geohash = geo.point(req.location.lat, req.location.lng).hash;

    return req;
  }


}
