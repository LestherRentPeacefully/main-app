import { Component, OnChanges, Input, ViewChild } from '@angular/core';
import {ToastService, TabsetComponent, ModalDirective} from 'ng-uikit-pro-standard';
import {SharedService} from '../../../shared/shared.service';
import { Router } from '@angular/router';
import {MyPropertiesService} from '../../my-properties.service';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnChanges {

  @Input() myProperty;
  @Input() userInfo;
  @Input() selectedPanel;
  @Input() staticTabs;
  @Input() tenantSelected;

  @ViewChild('endLeaseModal') public endLeaseModal:ModalDirective;
  @ViewChild('deletePropertyModal') public deletePropertyModal:ModalDirective;
  
  
  private toastOptions = this.sharedService.toastOptions;
  public endingLease;
  public cancelingCulmination;
  public deletingProperty;

  constructor(
    private router: Router,
    private toast: ToastService,
    private sharedService:SharedService,
    private myPropertiesService: MyPropertiesService) { }

  ngOnInit() {
  }

  async ngOnChanges(){

  }

  openEndLeaseModal(){
    this.endLeaseModal.show();
  }

  openDeletePropertyModal(){
    this.deletePropertyModal.show();
  }


  async endLease(){
    this.endingLease = true;
    try {
      const response = await this.myPropertiesService.endLease({myPropertyID:this.myProperty.id, tenantID:this.tenantSelected.uid});

      if (response.success) {
        this.endLeaseModal.hide();
        if (this.tenantSelected.info.status=='active') {
          this.toast.success('','Request sent successfully', this.toastOptions);
          this.tenantSelected.info.status = 'endingLease';
          this.tenantSelected.info.requestedBy = this.userInfo.uid;

        } else if(this.tenantSelected.info.status=='endingLease'){
          this.toast.success('','Lease ended successfully', this.toastOptions);
          this.router.navigate([`my-properties/all`]);
        }

      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'Error', this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.endingLease = false;
  }


  
  async cancelCulmination(){
    this.cancelingCulmination = true;
    try {
      const response = await this.myPropertiesService.cancelCulmination({myPropertyID:this.myProperty.id, tenantID:this.tenantSelected.uid});

      if (response.success) {
        this.toast.success('','Request canceled successfully', this.toastOptions);
        this.tenantSelected.info.status = 'active';
        delete this.tenantSelected.info.requestedBy;

      }else{
        console.error(response.error.message);
        this.toast.error(response.error.message,'Error', this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.cancelingCulmination = false;
  }


  async deleteProperty(){
    if(this.myProperty.tenantsID.length>0){
      this.toast.info('','You need to end lease with every tenant in order to delete the Property', this.toastOptions);
      
    } else if(!this.myProperty.createdByRealEstateAgent && this.myProperty.listingID){
      this.toast.info('','You need to delete the listing before deleting the property', this.toastOptions);

    } else{
      this.deletingProperty = true;

      try {
        const response = await this.myPropertiesService.delete({myPropertyID:this.myProperty.id});

        if (response.success) {
          this.deletePropertyModal.hide();
          this.toast.success('','Property deleted successfully', this.toastOptions);
          this.router.navigate([`my-properties/all`]); 
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'Error', this.toastOptions);
        }

      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }

      this.deletingProperty = false;

    }
  }




  
}
