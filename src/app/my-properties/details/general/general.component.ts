import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {ToastService, TabsetComponent, ModalDirective} from 'ng-uikit-pro-standard';
import {SharedService} from '../../../shared/shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {MyPropertiesService} from '../../my-properties.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent {

  @Input() myProperty;
  @Input() userInfo;
  @Input() selectedPanel;
  @Input() staticTabs:TabsetComponent;
  
  tenantSelected;

  tenantsOptions = [];
  tenants:any[];
  landlordInfo;

  selectTenantForm = this.fb.group({
    tenant:['',[ Validators.required ]]
  });
  
  private toastOptions = this.sharedService.toastOptions;
  

  constructor(
    private router: Router, 
    private fb: FormBuilder,
    public sharedService:SharedService,
    private myPropertiesService: MyPropertiesService,
    private toast: ToastService) { }

  async ngOnChanges() {
    if(this.selectedPanel==1 && this.myProperty.user.uid==this.userInfo.uid && this.myProperty.tenantsID.length>0){ //LANDLORD

      this.tenantSelected = null;
      this.tenants = null;
      this.tenantsOptions = [];

      const response = await this.myPropertiesService.getTenants({tenantsID:this.myProperty.tenantsID});
      
      if(response.success){
        this.tenants = response.data;
        
        for(let i in this.tenants){
          this.tenantsOptions.push({
            value: i,
            label: `${this.sharedService.capitalize(this.tenants[i].firstName)} ${this.sharedService.capitalize(this.tenants[i].lastName)}`
          });
        }


        this.selectTenantForm.get('tenant').setValue('0');
        this.tenantSelected = this.tenants['0'];
        this.tenantSelected.info = await this.myPropertiesService.getTenantSelected(this.myProperty.id, this.tenantSelected.uid);
        
        this.selectTenantForm.get('tenant').valueChanges.subscribe(async index=>{
          this.tenantSelected = this.tenants[index];
          this.tenantSelected.info = await this.myPropertiesService.getTenantSelected(this.myProperty.id, this.tenantSelected.uid);
        });
        
        
        
      }else{
        console.error(response.error.message);
        this.toast.error('', response.error.message, this.toastOptions);
      }


    }else if(this.selectedPanel==1 && (<string[]>this.myProperty.tenantsID).includes(this.userInfo.uid)){ //TENANT
      this.tenantSelected = null;
      this.landlordInfo = null;
      this.landlordInfo = await this.myPropertiesService.getLandlord(this.myProperty.user.uid);
      this.tenantSelected = this.userInfo;
      this.tenantSelected.info = await this.myPropertiesService.getTenantSelected(this.myProperty.id, this.tenantSelected.uid);
    }

  }


  listProperty(){
    if(
      (this.userInfo.membership.pricingPlan=='free' && this.userInfo.landlord.currentlyListed>=2) || 
      (this.userInfo.membership.pricingPlan=='basic' && this.userInfo.landlord.currentlyListed>=7) || 
      (this.userInfo.membership.pricingPlan=='plus' && this.userInfo.landlord.currentlyListed>=15) || 
      (this.userInfo.membership.pricingPlan=='professional' && this.userInfo.landlord.currentlyListed>=20)
    ){
      this.toast.error('Too many properties already listed', 'Upgrade to a higher plan', this.toastOptions);
      return;
    }

    // this.router.navigate([`listings/new/${this.myProperty.id}`]); 
    this.router.navigate([`listings/new`], {queryParams: {myPropertyID:this.myProperty.id}}); 
  }


  public get ticketIsOpen() {
    return this.tenantSelected && this.tenantSelected.info && this.tenantSelected.info.state && this.tenantSelected.info.state.ticketInfo && this.tenantSelected.info.state.ticketInfo.open;
  }

  public panelShown(panel){
    this.selectedPanel = panel;
    this.staticTabs.setActiveTab(panel);
  }


  
  editProperty(){
    this.router.navigate([`my-properties/edit/${this.myProperty.id}`]); 
  }

  viewListing(){
    this.router.navigate([`listings/details/${this.myProperty.listingID}`]); 
  }
  
  viewProfile(uid:string){
    this.router.navigate(['/user', uid], { queryParams: {role: 'tenant'}});
  }

  public isTenant(uid:string) : boolean {
    return this.tenantSelected.uid==uid;
  }

  public isLandlord(uid:string) : boolean {
    return this.myProperty.user.uid==uid;
  }


  public get openMessagesObj() {
    const obj = {
      chatName: `(${this.sharedService.capitalize(this.tenantSelected.firstName)} ${this.tenantSelected.lastName[0].toUpperCase()}) ${this.myProperty.name}`,
      id: this.tenantSelected.info.chatID,
      user: this.isLandlord(this.userInfo.uid)? this.tenantSelected : this.landlordInfo,
      myPropertyID: this.myProperty.id
    }
    return obj;
  }

}
