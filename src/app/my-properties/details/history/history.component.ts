import { Component, ViewChild, Input, OnChanges } from '@angular/core';
import {ToastService, ModalDirective} from 'ng-uikit-pro-standard';
import {SharedService} from '../../../shared/shared.service';
import { environment } from '../../../../environments/environment';
import {BigNumber} from 'bignumber.js';
import {MyPropertiesService} from '../../my-properties.service';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnChanges {


  @Input() myProperty;
  @Input() userInfo;
  @Input() selectedPanel;
  @Input() staticTabs;
  @Input() tenantSelected;

  @ViewChild('historyItemModal') public historyItemModal:ModalDirective;

  private toastOptions = this.sharedService.toastOptions;
  public activePage = 1;
  public firstVisibleIndex = 1;
  public offset = 10;
  public lastVisibleIndex = this.offset;
  public lastVisiblePage;
  public loadingHistory;
  public history;
  public historyItemSelected;

  constructor(
    private sharedService:SharedService,
    private myPropertiesService: MyPropertiesService,
    private toast: ToastService,
  ) { }

  async ngOnChanges() {

    if(this.selectedPanel==3 && this.tenantSelected && this.tenantSelected.info){
      this.loadingHistory = true;
      try {
        this.history = await this.myPropertiesService.getPropertyHistory(this.myProperty.id, this.tenantSelected.uid);
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','Error', this.toastOptions);
      }
      this.loadingHistory = false;
    }
  }


  public nextPage() {
    this.activePage++;
    this.firstVisibleIndex = this.activePage * this.offset - this.offset + 1;

    if (this.activePage==this.lastVisiblePage) { 
      this.lastVisibleIndex = this.history.length;
    } else {
      this.lastVisibleIndex = this.activePage * this.offset;
    }
  }

  public previousPage() {
    this.activePage --;
    this.firstVisibleIndex = this.activePage * this.offset - this.offset + 1;
    this.lastVisibleIndex = this.activePage * this.offset;
  }

  public firstPage() {
    this.activePage = 1;
    this.firstVisibleIndex = this.activePage * this.offset - this.offset + 1;
    this.lastVisibleIndex = this.activePage * this.offset;
  }

  public lastPage() {
    this.activePage = this.lastVisiblePage;
    this.firstVisibleIndex = this.activePage * this.offset - this.offset + 1;
    this.lastVisibleIndex = this.history.length;
  }

  public selectHistoryItem(item){
    this.historyItemSelected = item;
    this.historyItemModal.show();
  }

  public formatDateString(date:number):string{
    let formatedDate = this.sharedService.formatDate(date);
    let minutes = formatedDate.minutes<10? `0${String(formatedDate.minutes)}` : formatedDate.minutes;
    let hours = formatedDate.hours==0? 12:formatedDate.hours;
    hours = hours>12? hours-12:hours;
    return `${formatedDate.month} ${formatedDate.dayOfMonth}, ${formatedDate.year} at 
            ${hours}:${minutes} ${formatedDate.hours>=12? 'PM':'AM'}`;
  }

}
