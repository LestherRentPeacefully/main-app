import { Component, OnInit, Input, OnChanges, ViewChild } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import {ToastService, ModalDirective, TabsetComponent} from 'ng-uikit-pro-standard';
import {SharedService} from '../../../shared/shared.service';
import {MyPropertiesService} from '../../my-properties.service';
import { environment } from '../../../../environments/environment';
import {BigNumber} from 'bignumber.js';
import {ValidationsService} from '../../../validations.service';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent {

  @Input() myProperty;
  @Input() userInfo;
  @Input() selectedPanel;
  @Input() staticTabs;
  @Input() tenantSelected;

  @ViewChild('depositWithCryptoModal') public depositWithCryptoModal:ModalDirective;
  @ViewChild('ticketModal') public ticketModal:ModalDirective;
  @ViewChild('modifyRentModal') public modifyRentModal:ModalDirective;

  private toastOptions = this.sharedService.toastOptions;
  public currencySelected:any;
  public currencies;
  public depositingWithCrypto;
  public submittingTicket;
  public modifyingRent;

  public depositWithCryptoForm = this.fb.group({
    payment:['', [Validators.required, this.validationService.isGreaterThan(0)]]
  });

  public ticketForm = this.fb.group({
    details:['', [Validators.required, Validators.maxLength(50)]]
  });

  public modifyRentForm = this.fb.group({
    newRent:['', [Validators.required, this.validationService.isGreaterThan(0)]]
  });
  

  constructor(
    private toast: ToastService,
    private sharedService:SharedService,
    private fb: FormBuilder,
    private validationService:ValidationsService,
    private myPropertiesService: MyPropertiesService) {
     }


  ngOnChanges() {
    if(this.selectedPanel==2 && this.tenantSelected && this.tenantSelected.info){

    }
  }


  async openDepositWithCryptoModal(){
    this.currencies = null;
    this.currencySelected = null;
    this.depositWithCryptoForm.reset();

    this.depositWithCryptoModal.show();

    this.currencies = await this.sharedService.getCurrencies();
  }
  
  
  async selectCurrency(item:any){
    this.currencySelected = item;
  }


  async depositWithCrypto(){
    let req = this.depositWithCryptoForm.value;
    this.depositWithCryptoForm.disable();
    this.depositingWithCrypto = true;

    if ((this.userInfo.balances[this.currencySelected.currency] && new BigNumber(this.userInfo.balances[this.currencySelected.currency].available).isGreaterThanOrEqualTo(req.payment))) {
      try {
      
        const response = await this.myPropertiesService.depositWithCrypto({
          myPropertyID:this.myProperty.id,
          currency:this.currencySelected.currency,
          amount:req.payment
        });

        if (response.success) {
          this.depositWithCryptoModal.hide();

          const depositInfo = {
            creationTS: Date.now(),
            currencyForPayment:{
              currency:this.currencySelected.currency,
              amount:new BigNumber(req.payment).decimalPlaces(8).toString(10),
              name:this.currencySelected.name
            }
          }        
          
          this.tenantSelected.info.state? this.tenantSelected.info.state.depositInfo = depositInfo : this.tenantSelected.info.state = { depositInfo };

          this.toast.success('','Payment sent', this.toastOptions);

        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'Error', this.toastOptions);
        }


      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }

    } else {
      this.toast.error('Insufficient funds','', this.toastOptions);
    }

    this.depositWithCryptoForm.enable();
    this.depositingWithCrypto = false;

  }



  public get ticketIsOpen() {
    return this.tenantSelected.info.state && this.tenantSelected.info.state.ticketInfo && this.tenantSelected.info.state.ticketInfo.open;
  }

  ticketModalClosed(){
    this.ticketForm.reset();
  }


  async ticket(){

    let req = this.ticketForm.value;
    this.ticketForm.disable();
    this.submittingTicket = true;

    try {
      const open = this.ticketIsOpen;

      const response = await this.myPropertiesService.ticket({
        myPropertyID:this.myProperty.id,
        details:!open? req.details : null
      });

      if (response.success) {
        this.ticketModal.hide();

        const ticketInfo = {
          creationTS: Date.now(),
          details:!open? req.details : null,
          open:!open
        };
        
        this.tenantSelected.info.state? this.tenantSelected.info.state.ticketInfo = ticketInfo : this.tenantSelected.info.state = { ticketInfo };       

        this.toast.success('',`Ticket ${!open? 'open': 'closed'}`, this.toastOptions);
        
      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'Error', this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.ticketForm.enable();
    this.submittingTicket = false;
  }

  
  modifyRentModalClosed(){
    this.modifyRentForm.reset();
  }


  async modifyRent(){

    let req = this.modifyRentForm.value;
    this.modifyingRent = true;
    this.modifyRentForm.disable();

    try {
      const response = await this.myPropertiesService.modifyRent({
        myPropertyID:this.myProperty.id,
        tenantID:this.tenantSelected.uid,
        newRent:req.newRent
      });

      if (response.success) {
        this.modifyRentModal.hide();

        const rentModificationInfo = {
          creationTS: Date.now(),
        }
        
        this.tenantSelected.info.rentAmount = req.newRent;
        this.tenantSelected.info.state? this.tenantSelected.info.state.rentModificationInfo = rentModificationInfo : this.tenantSelected.info.state = { rentModificationInfo };  
        
        this.toast.success('','Rent modified', this.toastOptions);

      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'Error', this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.modifyRentForm.enable();
    this.modifyingRent = false;
  }




  public formatDateString(date:number):string{
    let formatedDate = this.sharedService.formatDate(date);
    let minutes = formatedDate.minutes<10? `0${String(formatedDate.minutes)}` : formatedDate.minutes;
    let hours = formatedDate.hours==0? 12:formatedDate.hours;
    hours = hours>12? hours-12:hours;
    return `${formatedDate.month} ${formatedDate.dayOfMonth}, ${formatedDate.year} at 
            ${hours}:${minutes} ${formatedDate.hours>=12? 'PM':'AM'}`;
  }

  public formatBalance(value){
    return this.sharedService.formatBalance(value, 0); 
  }

  

}
