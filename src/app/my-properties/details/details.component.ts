import { Component, OnInit, ViewChild } from '@angular/core';
import {ToastService, ModalDirective} from 'ng-uikit-pro-standard';
import { ActivatedRoute } from '@angular/router';
import {AuthService} from '../../auth.service';
import {SharedService} from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import { combineLatest } from 'rxjs';
import { filter, tap, map } from 'rxjs/operators';
import {MyPropertiesService} from '../my-properties.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  private toastOptions = this.sharedService.toastOptions;
  // public app = environment.app;
  private sub;
  public txHash:string;
  public selectedPanel = 1;
  public subscriber;
  public loadingMyProperty = true;
  public userInfo:any;
  public txSub;
  public loadedOnce;
  public userInProperty;
  public myPropertyID:string;
  public myProperty;

  

  constructor(private toast: ToastService,
              private authService:AuthService,
              private sharedService:SharedService,
              private myPropertiesService:MyPropertiesService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    

    this.sub = this.route.paramMap.subscribe((params)=>{
      this.myPropertyID = params.get('id');
      this.loadingMyProperty = true;

      this.subscriber = combineLatest(
        this.myPropertiesService.getMyProperty$(this.myPropertyID), this.authService.userInfo
        ).pipe(filter(([myProperty, userInfo])=>!!myProperty && !!userInfo)).subscribe(async([myProperty, userInfo])=>{

          this.myProperty = myProperty;
          this.userInfo = userInfo;

          this.loadingMyProperty = false;
          
          
          if (!this.loadedOnce) {
            this.loadedOnce = true;
          }


          
        });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.subscriber.unsubscribe();
  }




  public panelShown(panel){
    this.selectedPanel = panel;
  }

}
