import { Component, OnChanges, Input, ViewChild } from '@angular/core';
import {ToastService, TabsetComponent, ModalDirective} from 'ng-uikit-pro-standard';
import {SharedService} from '../../../shared/shared.service';
import { Router } from '@angular/router';
import {BigNumber} from 'bignumber.js';
import { environment } from 'src/environments/environment';
import {MyPropertiesService} from '../../my-properties.service';
import { Validators, FormBuilder } from '@angular/forms';
import { EthereumService } from 'src/app/my-wallet/identity-wallet/ethereum.service';

declare var HelloSign:any;

@Component({
  selector: 'app-esigned-docs',
  templateUrl: './esigned-docs.component.html',
  styleUrls: ['./esigned-docs.component.css']
})
export class EsignedDocsComponent implements OnChanges {

  @Input() myProperty;
  @Input() userInfo;
  @Input() selectedPanel;
  @Input() staticTabs;
  @Input() tenantSelected;

  @ViewChild('provideFeedbackModal') provideFeedbackModal:ModalDirective;
  @ViewChild('esignModal') esignModal:ModalDirective;

  @ViewChild('sendTX') public sendModal:ModalDirective;
  @ViewChild('txModal') public txModal:ModalDirective;

  public signatureRequestSelected:any;
  private toastOptions = this.sharedService.toastOptions;
  public currencies:any[];
  public payingForEsign;
  public currencySelected;
  public amountToPay;
  public doc:any = {};
  public loadingSignatures;

  public sigReqOffset = 5;
  public loadingMoreSigReq: boolean;
  public canLoadMoreSigReq: boolean;

  public txHash:string;
  public estimating: boolean;
  public minGas: number;
  public submittingDocOnBC: boolean;
  public balance: number;
  public etherscanURL = environment.etherscanExplorer;


  public itemForm = this.fb.group ({
    gasLimit: ['', [ Validators.required, Validators.min(21000) ]],
    gasPriceInGWei: ['', [ Validators.required, Validators.min(1) ]]
  });


  constructor(
    private toast: ToastService,
    private sharedService:SharedService,
    private myPropertiesService: MyPropertiesService,
    private ethereumService:EthereumService,
    private fb: FormBuilder,
    private router: Router) { }



  async ngOnChanges(){
    if(this.selectedPanel !== 4 || !this.tenantSelected) return;
    
    this.tenantSelected.signaturesRequest = null;
    this.tenantSelected.signaturesRequest = await this.myPropertiesService.getSignaturesRequest(this.myProperty.id, this.tenantSelected.uid, this.sigReqOffset, null);
   
    this.canLoadMoreSigReq = this.tenantSelected.signaturesRequest.length === this.sigReqOffset;

    if(this.tenantSelected.signaturesRequest.length === 0) return;

    const signaturesRequestID = (<any[]>this.tenantSelected.signaturesRequest)
    .map(signatureRequest=>signatureRequest.signatureRequestID)
    .filter(signatureRequestID=>signatureRequestID);

    if(signaturesRequestID.length === 0) return;

    this.loadingSignatures = true;
    
    const response = await this.myPropertiesService.getArrayOfSignatures({signaturesRequestID});

    if (!response.success) {
      console.error(response.error.message);
      this.toast.error('',response.error.message, this.toastOptions);
      return;
    }
    

    this.loadingSignatures = false;
    const arrayOfSignatures = response.data;

    this.tenantSelected.signaturesRequest = (<any[]>this.tenantSelected.signaturesRequest).map(signatureRequest=>{
      if(signatureRequest.signatureRequestID){
        for(let data of arrayOfSignatures){
          if(signatureRequest.signatureRequestID.toLowerCase()==data.signatureRequestID.toLowerCase()){
            signatureRequest.signatures = data.signatures;
          }
        }
      }

      return signatureRequest;
    });



  }



  async loadMoreSigReq() {
    if(this.loadingMoreSigReq) return;

    this.loadingMoreSigReq = true;

    const moreSigReq = await this.myPropertiesService.getSignaturesRequest(this.myProperty.id, this.tenantSelected.uid, this.sigReqOffset, this.tenantSelected.signaturesRequest[this.tenantSelected.signaturesRequest.length-1].creationTS);
    
    this.loadingMoreSigReq = false;
    
    this.canLoadMoreSigReq = moreSigReq.length === this.sigReqOffset;


    if(moreSigReq.length === 0) return;

    const signaturesRequestID: string[] = moreSigReq
      .map(signatureRequest => signatureRequest.signatureRequestID)
      .filter(signatureRequestID => signatureRequestID);

    if(signaturesRequestID.length === 0) return this.tenantSelected.signaturesRequest.push(...moreSigReq);

    this.loadingMoreSigReq = true;

    const response = await this.myPropertiesService.getArrayOfSignatures({signaturesRequestID});
    
    if (!response.success) {
      console.error(response.error.message);
      this.toast.error('',response.error.message, this.toastOptions);
      return;
    }

    this.tenantSelected.signaturesRequest.push(...moreSigReq);

    this.loadingMoreSigReq = false;

    const arrayOfSignatures = response.data;

    this.tenantSelected.signaturesRequest = this.tenantSelected.signaturesRequest.map(signatureRequest=>{
      if(signatureRequest.signatureRequestID){
        for(let data of arrayOfSignatures){
          if(signatureRequest.signatureRequestID.toLowerCase()==data.signatureRequestID.toLowerCase()){
            signatureRequest.signatures = data.signatures;
          }
        }
      }
      return signatureRequest;
    });
   
    
  }



  public tenantSignature(signatures:any[]) : any {
    return signatures.find(signature=>signature.signer_email_address.toLowerCase()==this.tenantSelected.email);
  }

  public landlordSignature(signatures:any[]) : any {
    return signatures.find(signature=>signature.signer_email_address.toLowerCase()==this.myProperty.user.email);
  }

  public isTenant(uid:string) : boolean {
    return this.tenantSelected.uid==uid;
  }

  public isLandlord(uid:string) : boolean {
    return this.myProperty.user.uid==uid;
  }
  



  async openEsignModal(){
    this.currencies = null;
    this.currencySelected = null;
    this.signatureRequestSelected = null;
    
    this.esignModal.show();

    this.currencies = await this.sharedService.getCurrencies();
  }


  async selectCurrency(item:any){
    if(!this.payingForEsign && (!this.currencySelected || this.amountToPay)){

      this.currencySelected = item;
      this.amountToPay = null;
      try {
        const response = await this.myPropertiesService.getCurrencyPriceForEsign({currency:this.currencySelected.currency});
        if (response.success) {
          this.amountToPay = new BigNumber(10).div(response.data.price.USD).decimalPlaces(8).toString(10);
          
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'',this.toastOptions);
        }
        
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }
    }
  }

  public formatBalance(value){
    return this.sharedService.formatBalance(value, 0); 
  }


  async payForEsignWithCrypto(){
    if (this.userInfo.balances[this.currencySelected.currency] && new BigNumber(this.userInfo.balances[this.currencySelected.currency].available).isGreaterThanOrEqualTo(this.amountToPay)) {
      
      this.payingForEsign = true;
      
      try {
        const response = await this.myPropertiesService.payForEsignWithCrypto({currency:this.currencySelected.currency, myPropertyID:this.myProperty.id, tenantID:this.tenantSelected.uid});
        
        if (response.success) {

          this.toast.success('','eSign paid successfully', this.toastOptions);
          
          this.signatureRequestSelected = {
            id: response.data,
            signatureRequestID: null,
            creationTS: Date.now()
          };

          (<any[]>this.tenantSelected.signaturesRequest).push(this.signatureRequestSelected);
          
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'',this.toastOptions);
        }

      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }
      this.payingForEsign = false;

    }else{
      this.toast.error('Insufficient funds','', this.toastOptions);
    }
  }




  async selectSignatureRequest(signatureRequestSelected){
    this.signatureRequestSelected = signatureRequestSelected;

    if(this.signatureRequestSelected.signatures){
      
      const signature = this.signatureRequestSelected.signatures.find(_signature=>_signature.signer_email_address.toLowerCase()==this.userInfo.email);

      const [response] = await Promise.all([
        this.myPropertiesService.getSignUrl({signatureID:signature.signature_id}), 
        this.sharedService.loadScript('hellosign')
      ]);
      
      if (response.success) {
        this.initHelloSign(response.data);
        
      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'',this.toastOptions);
      }
    } else{
      this.esignModal.show();
    }
  }



  onUploadOutput(e: any) {
    const files = e.target.files as File[];

    if (files && files.length > 0) {
      const file = files[0];
      this.doc = { file:file, tempName:file.name };
    }
    e.target.value = '';
  }


  async uploadDocument(){

    if (this.doc.file.type !== 'application/pdf') return this.toast.error('', 'Only PDF files are allowed',this.toastOptions);
    
    this.doc.uploading = true;
    this.doc.errorUploading = false;

    const formData = new FormData();
    formData.append('doc', this.doc.file);
    formData.append('myPropertyID', this.myProperty.id);
    formData.append('signatureRequestSelectedID',  this.signatureRequestSelected.id);
    formData.append('tenantID',  this.tenantSelected.uid);
    
    (await this.myPropertiesService.uploadDocument(formData, 
      uploadIDFilePercent=>{
        this.doc.uploading = true;
        this.doc.uploadPercent = uploadIDFilePercent;
        

      }, async response=>{
        this.doc.uploading = false;

        if (response.success) {
          const index = (<any[]>this.tenantSelected.signaturesRequest).findIndex(signaturesRequest=>signaturesRequest.id==this.signatureRequestSelected.id);
          
          this.signatureRequestSelected.signatureRequestID = response.data.signatureRequestID;
          this.signatureRequestSelected.signatures = response.data.signatures;

          this.tenantSelected.signaturesRequest[index].signatureRequestID = response.data.signatureRequestID;
          this.tenantSelected.signaturesRequest[index].signatures = response.data.signatures;

          await this.sharedService.loadScript('hellosign');
          this.esignModal.hide();
          this.toast.success('','Document uploaded', this.toastOptions);
          this.initHelloSign(response.data.signUrl);

        } else {
          this.doc.errorUploading = true;
          console.error(response.error.message);
          this.toast.error(response.error.message,'',this.toastOptions);
        }

      }, ()=>{
        this.doc.uploading = false;
        this.doc.errorUploading = true;
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }));
  }





  async gasAndGasPrice(signatureRequestSelected){

    this.signatureRequestSelected = signatureRequestSelected;

    if(!signatureRequestSelected.signatures) return this.toast.info('You need save and sign the document first','', this.toastOptions);
    if(this.landlordSignature(signatureRequestSelected.signatures).status_code!='signed' || this.tenantSignature(signatureRequestSelected.signatures).status_code!='signed') return this.toast.info('All the parties need to sign the document first','', this.toastOptions);
    if(!this.myProperty.smartContract) return this.toast.info('You need to deploy a smart contract for the property first','', this.toastOptions);
    if(this.myProperty.smartContract.status === 'pending') return this.toast.info('You need to wait until the smart contract is confirmed by the blockchain','', this.toastOptions);

    this.sendModal.show();
    this.estimating = true;
    try {

      const [response1, response2] = await Promise.all([
        this.myPropertiesService.estimateGasAndGasPriceDocSaving({myPropertyID: this.myProperty.id}),
        this.ethereumService.getBalances()
      ]);

      if(!response1.success) {
        console.error(response1.error.message);
        this.toast.error(response1.error.message,'',this.toastOptions);
        this.estimating = false;
        return;
      }

      if(!response2.success) {
        console.error(response2.error.message);
        this.toast.error(response2.error.message,'',this.toastOptions);
        this.estimating = false;
        return;
      }

      this.itemForm.get('gasPriceInGWei').setValue(new BigNumber(response1.data.gasPrice).times(new BigNumber(10).pow(-9)).toNumber());
      this.itemForm.get('gasLimit').setValue(new BigNumber(response1.data.estimateGas).times(1.20).integerValue().toNumber());
      this.minGas = new BigNumber(response1.data.estimateGas).toNumber();

      this.balance = response2.data.ETH;

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('Unexpected error estimating gas. Try again later','', this.toastOptions);
    }

    this.estimating = false;
  }



  async saveDocOnBlockchain(){

    if (this.itemForm.get('gasLimit').hasError('required') || this.itemForm.get('gasLimit').value<this.minGas) {
      return this.toast.error(`Gas limit must be greater or equal than ${this.minGas}`);
    } 
    if(this.itemForm.get('gasPriceInGWei').hasError('required') || this.itemForm.get('gasPriceInGWei').hasError('min')){
      return this.toast.error('Gas Price must be equal to or greater than 1 GWei','', this.toastOptions);
    }

    if(new BigNumber(this.itemForm.value.gasLimit).times(this.itemForm.value.gasPriceInGWei).times(new BigNumber(10).pow(9)).isGreaterThan(this.balance) ) {
      return this.toast.error('Insufficient funds in your Identity Wallet','', this.toastOptions);
    }

    const req = {
      gasLimit: this.itemForm.value.gasLimit,
      gasPrice: new BigNumber(this.itemForm.value.gasPriceInGWei).times(new BigNumber(10).pow(9)).toNumber(),
    };


    this.submittingDocOnBC = true;
    this.itemForm.disable();


    try {
      let response = await this.myPropertiesService.getDocUrl({signatureRequestID:this.signatureRequestSelected.signatureRequestID});
  
      if(!response.success) {
        console.error(response.error.message);
        this.toast.error(response.error.message,'',this.toastOptions);
        this.submittingDocOnBC = false;
        this.itemForm.enable();
        return;
      }
  
      const url = response.data;
  
  
      response = (await this.myPropertiesService.uploadToIPFS({url}));
  
      if(!response.success) {
        console.error(response.error.message);
        this.toast.error(response.error.message,'',this.toastOptions);
        this.submittingDocOnBC = false;
        this.itemForm.enable();
        return;
      }
  
      const ipfsHash = response.data;
  
  
      response = await this.myPropertiesService.saveDocOnBlockchain({
        ipfsHash,
        myPropertyID: this.myProperty.id,
        tenantID: this.tenantSelected.uid,
        signatureRequestSelectedID: this.signatureRequestSelected.id,
        gasLimit: req.gasLimit,
        gasPrice: req.gasPrice
      });
  
      if(!response.success) {
        console.error(response.error.message);
        this.toast.error(response.error.message,'',this.toastOptions);
        this.submittingDocOnBC = false;
        this.itemForm.enable();
        return;
      }
  
      this.txHash = response.data;
      
      const index = (<any[]>this.tenantSelected.signaturesRequest).findIndex(signatureRequest=>signatureRequest.id === this.signatureRequestSelected.id);
  
      this.tenantSelected.signaturesRequest[index].smartContract = {
        status: 'pending',
        txHash: this.txHash,
        ipfsHash
      };
  
      this.sendModal.hide();
      this.txModal.show();
      
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('Unexpected error estimating gas. Try again later','', this.toastOptions);
    }

    this.submittingDocOnBC = false;
    this.itemForm.enable();
     



  }



  private initHelloSign(signUrl:string){

    HelloSign.init(environment.hellosign.clientId);

    HelloSign.open({
      url: signUrl,
      allowCancel: true,
      skipDomainVerification:true,
      messageListener: (eventData) => {
        if(eventData.event=='signature_request_signed'){
          const signaturesRequestIndex = (<any[]>this.tenantSelected.signaturesRequest).findIndex(signaturesRequest=>signaturesRequest.id==this.signatureRequestSelected.id);

          const signatureIndex = (<any[]>this.signatureRequestSelected.signatures).findIndex(signature=>signature.signer_email_address.toLowerCase()==this.userInfo.email);

          this.signatureRequestSelected.signatures[signatureIndex].status_code = 'signed';

          this.signatureRequestSelected.signatures[signatureIndex].signed_at = Math.floor(Date.now()/1000);

          this.tenantSelected.signaturesRequest[signaturesRequestIndex].signatures[signatureIndex].status_code = 'signed';

          this.tenantSelected.signaturesRequest[signaturesRequestIndex].signatures[signatureIndex].signed_at = Math.floor(Date.now()/1000);


          this.toast.success('','Document signed successfully', this.toastOptions);
        }
      } 
    });
  }



  async viewDocument(signatureRequestSelected){

    if(signatureRequestSelected.smartContract && signatureRequestSelected.smartContract.status === 'confirmed') {
      return window.open(`https://ipfs.infura.io/ipfs/${signatureRequestSelected.smartContract.ipfsHash}`, "_blank");
    }

    
    signatureRequestSelected.loadingDocUrl = true;

    const response = await this.myPropertiesService.getDocUrl({signatureRequestID:signatureRequestSelected.signatureRequestID});

    if (response.success) {
      window.open(response.data, "_blank");
    } else {
      console.error(response.error.message);
      this.toast.error(response.error.message,'',this.toastOptions);
    }
    
    signatureRequestSelected.loadingDocUrl = false;
  
  }


  public formatSignedDateString(date:number):string{
    let formatedDate = this.sharedService.formatDate(date);
    return `${formatedDate.month} ${formatedDate.dayOfMonth}, ${formatedDate.year}`;
  }

  public newBigNumber(number: any){
    return new BigNumber(number);
  }



}
