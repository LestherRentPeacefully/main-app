import { Component, OnInit } from '@angular/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {AuthService} from '../../auth.service';
import { Router }  from '@angular/router';
import {SharedService} from '../../shared/shared.service';
import {MyPropertiesService} from '../my-properties.service';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {
	private toastOptions = this.sharedService.toastOptions;
  public userInfo:any;
  public myProperties:any[];
  public userInfoSubscriber: any;

  constructor(private toast: ToastService,
              private router: Router,
              private authService:AuthService,
              private myPropertiesService:MyPropertiesService,
              public sharedService:SharedService) { }

  async ngOnInit() {
    this.userInfoSubscriber = this.authService.userInfo.subscribe(async (userInfo) => {
      this.userInfo = userInfo;
      if(this.userInfo){
        this.myProperties = await this.myPropertiesService.getAllMyProperties(this.userInfo.uid, this.userInfo.role);
      }
    });
  }

  ngOnDestroy() {
    this.userInfoSubscriber.unsubscribe();
  }


  public newProperty(){
    //if (!this.userInfo.status.IDVerified) {
    //  this.toast.error('','Please, verify your Identity first', this.toastOptions);
    //} else {
      this.router.navigate(['my-properties/new']); 
    //}
  }

  public viewListing(listingID:string){
    this.router.navigate([`listings/details/${listingID}`]); 
  }

  public listProperty(myPropertyID:string){
    // this.router.navigate([`listings/new/${myPropertyID}`]); 
    this.router.navigate([`listings/new`], {queryParams: {myPropertyID}}); 
  }


  public createdAt(timestamp:number) : string {
    let date = new Date(timestamp);
    return `${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()}`;
  }

}
