import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


/* Components */

import { MyPropertiesComponent } from './my-properties.component';
import { AllComponent } from './all/all.component';
import { DetailsComponent } from './details/details.component';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';


const routes: Routes = [{
  path:'',
  component:MyPropertiesComponent,
  children:[
    {
      path:'all',
      component:AllComponent
    },{
      path:'new',
      component:NewComponent
    },{
      path:'edit/:id',
      component:EditComponent
    },{
      path:'details/:id',
      component:DetailsComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPropertiesRoutingModule { }
