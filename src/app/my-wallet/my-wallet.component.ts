import { Component, OnInit, ViewChild } from '@angular/core';
import {AuthService} from '../auth.service';
import {BehaviorSubject} from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import {TabsetComponent} from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-my-wallet',
  templateUrl: './my-wallet.component.html',
  styleUrls: ['./my-wallet.component.css']
})
export class MyWalletComponent implements OnInit {
	public userInfo$:BehaviorSubject<any>;
  public selectedPanel = 1;
  public sub;

  @ViewChild('staticTabs') staticTabs:TabsetComponent;


  constructor(
    private authService:AuthService, 
    private route: ActivatedRoute,) {
  }
          
  ngOnInit() {
  	this.userInfo$ = this.authService.userInfo;
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  ngAfterViewInit(){
		this.sub = this.route.queryParams.subscribe(params => {
			setTimeout(()=>{
				if (params['panel']) {
					this.selectedPanel = +params['panel'];
					this.staticTabs.setActiveTab(+params['panel']);
				};
			});
		});
	}






}
