import { Component, OnInit, Input } from '@angular/core';
import {SharedService} from '../../shared/shared.service';
import { ToastService, TabsetComponent } from 'ng-uikit-pro-standard';
import {DepositWalletService} from './deposit-wallet.service';
import { Observable} from 'rxjs';
import { Router }  from '@angular/router';
import {BigNumber} from 'bignumber.js'; 

@Component({
  selector: 'app-deposit-wallet',
  templateUrl: './deposit-wallet.component.html',
  styleUrls: ['./deposit-wallet.component.css']
})
export class DepositWalletComponent implements OnInit {

  @Input() userInfo;
  @Input() staticTabs:TabsetComponent;

  private toastOptions = this.sharedService.toastOptions;
  public creating:any = {};
  public currencies$:Observable<any[]>;

  constructor(private sharedService:SharedService, 
              private depositWalletService:DepositWalletService,
              private toast: ToastService,
              private router: Router) { }

  ngOnInit() {
    this.currencies$ = this.depositWalletService.getCurrencies$();
  }

  ngOnDestroy(){
  }


  public formatBalance(value){
    return this.sharedService.formatBalance(value, 0); 
  }

  async createWallet(currency:string){
    this.creating[currency] = true;
    try {
      let response = await this.depositWalletService.create({currency});
      if (response.success) {

        if(!this.userInfo.balances) this.userInfo.balances = {};

        this.userInfo.balances[currency] = {
          available:'0',
          onOrders:'0',
          onEscrow:'0',
          onWithdrawal:'0'
        };

        this.toast.success('','Wallet created successfully',this.toastOptions);
      } else {
        this.toast.error(response.error.message,'',this.toastOptions);
      }
      
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.creating[currency] = false;
  }


  viewTransactionHistory(){
    // this.router.navigate(['/my-wallet'], { queryParams: {panel:3} });
    this.staticTabs.setActiveTab(3);
  }

  balanceInUSD(currency: string, price: string) {
    const available = this.userInfo.balances[currency]? this.userInfo.balances[currency].available : '0';
    return new BigNumber(available).times(price);
  }

  totalBalanceInUSD(currencies: any[]){
    let balance = new BigNumber(0);
    for(let item of currencies) {
      balance = balance.plus(this.balanceInUSD(item.currency, item.price.USD));
    }
    return balance;
  }

  

  

}
