import { Injectable } from '@angular/core';
import { HttpClient }from '@angular/common/http';
import {SharedService} from '../../shared/shared.service';
import { environment } from '../../../environments/environment';
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DepositWalletService {
  private APIURL = environment.APIURL;

  constructor(private http: HttpClient,
              private af: AngularFirestore,
              private sharedService:SharedService) { }

  public async create(req:{currency:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/wallet/create`, req, _httpOptions).toPromise();
  }

  public getCurrencies$(){
    return this.af.collection(`currencies`,ref=>ref.orderBy('name','asc')).valueChanges();
  }

  public async getCurrencies(){
    let currencies = [];
    const db = firebase.firestore();
    let snapshots = await db.collection('currencies').orderBy('name','asc').get();
    for(let snapshot of snapshots.docs){
      currencies.push(snapshot.data());
    }
    return currencies;
  }

  public async getDepositWallet(currency:string, uid:string){
    const db = firebase.firestore();
    return (await db.collection('wallets/deposit/public')
      .where('uid','==',uid)
      .where('currency','==',currency)
      .limit(1)
      .get()).docs[0].data();
  }

  public async getTransactions(uid:string, currency:string, type:string, offset:number, lastVisibleTS:number){
    let transactions = [];
    const db = firebase.firestore();
    let query = db.collection('transactions').where('uid','==',uid).where('processed','==',true);

    if(currency) query = query.where('currency','==',currency);
    if(type) query = query.where('type','==',type);

    let docs: firebase.firestore.QueryDocumentSnapshot[];

    if (!lastVisibleTS) {
      docs = (await query.orderBy('creationTS','desc').limit(offset).get()).docs;
    } else {
      docs = (await query.orderBy('creationTS','desc').startAfter(lastVisibleTS).limit(offset).get()).docs;
    }

    docs.forEach(doc => transactions.push(doc.data()))
    return transactions;
  }

  public async getWithdrawalFee(req:{currency:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/wallet/getWithdrawalFee`, req, _httpOptions).toPromise();
  }

  public async checkWithdrawalAddress(req:{currency:string, address:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/wallet/checkWithdrawalAddress`, req, _httpOptions).toPromise();
  }

  public async sendWithdrawalCode(): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/wallet/sendWithdrawalCode`, {}, _httpOptions).toPromise();
  }

  public async withdraw(req:{currency:string, code:string, to:string, amount:number}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/wallet/withdraw`, req, _httpOptions).toPromise();
  }


}
