import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {DepositWalletService} from '../deposit-wallet.service';
import { Validators, FormBuilder } from '@angular/forms';
import {SharedService} from '../../../shared/shared.service';
import { ToastService, ModalDirective } from 'ng-uikit-pro-standard';
import {BigNumber} from 'bignumber.js';
import { Transaction } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  public currencies:any[];
  public searching;
  public txOffset = 10;
  public txs: Transaction[];
  public loadingMoreTxs: boolean;
  public canLoadMoreTxs: boolean;
  public txSelected:any;
  private toastOptions = this.sharedService.toastOptions;
  public sub;
  
  @Input() userInfo;

  @ViewChild('txModal') public txModal:ModalDirective;

  public itemForm = this.fb.group({
    currency: ['0', [ Validators.required ]],
    txType: ['0', [ Validators.required ]],
	});

  cryptosSelect:{value:string, label:string, currency?, icon:string}[];

  transactionTypeSelect = [
    {value:'0', label:'All transaction types', icon:`./assets/transactions/allTxTypes.png`},
    {value:'1', label:'Deposit', type:'deposit', icon:`./assets/transactions/deposit.png`},
    {value:'2', label:'Withdrawal', type:'withdrawal', icon:`./assets/transactions/withdrawal.png`},
    {value:'3', label:'Membership', type:'membership', icon:`./assets/transactions/membership.png`},
    {value:'4', label:'Payment sent', type:'payment sent', icon:`./assets/transactions/paymentSent.png`},
    {value:'5', label:'Payment received', type:'payment received', icon:`./assets/transactions/paymentReceived.png`},
    {value:'6', label:'Fees and Charges', type:'fees and charges', icon:`./assets/transactions/feesAndCharges.png`},
    {value:'7', label:'ESigned document', type:'eSign', icon:`./assets/transactions/eSign.png`}
  ];

  constructor(
    private depositWalletService:DepositWalletService, 
    private sharedService:SharedService,
    private toast: ToastService,
    private fb: FormBuilder) { }

  async ngOnInit() {
    this.currencies = await this.depositWalletService.getCurrencies();

    this.cryptosSelect = [{value:'0', label:'All Cryptocurrencies', icon:'./assets/coins/cryptos.png'}];

    for(let i in this.currencies){
      this.cryptosSelect.push({
        value: String(+i+1) ,
        label: this.currencies[i].name,
        currency: this.currencies[i].currency,
        icon: `./assets/coins/${this.currencies[i].currency}.png`
      });

    }

    this.search();

    this.sub = this.itemForm.valueChanges.subscribe(()=> this.search());
  }



  ngOnDestroy(){
    this.sub.unsubscribe();
  }


  get currencyFromSelect(){
    return this.cryptosSelect.find(item=>item.value==this.itemForm.get('currency').value).currency;
  }

  
  public get txTypeFromSelect() {
    return this.transactionTypeSelect.find(item =>item.value==this.itemForm.get('txType').value).type;
  }

  public iconFromtxType(type:string){
    return this.transactionTypeSelect.find(item=>item.type==type).icon;
  }


  public async search(){
    // this.itemForm.disable();
    this.searching = true;
    this.txs = null;
    try {
      this.txs = await this.depositWalletService.getTransactions(this.userInfo.uid, this.currencyFromSelect, this.txTypeFromSelect, this.txOffset, null);
      this.canLoadMoreTxs = this.txs.length === this.txOffset;

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    // this.itemForm.enable();
    this.searching = false;
  }


  async loadMoreTx() {
    if(this.loadingMoreTxs) return;

    this.loadingMoreTxs = true;

    const moreTxs = await this.depositWalletService.getTransactions(this.userInfo.uid, this.currencyFromSelect, this.txTypeFromSelect, this.txOffset, this.txs[this.txs.length-1].creationTS);
    this.txs.push(...moreTxs);

    this.loadingMoreTxs = false;

    this.canLoadMoreTxs = moreTxs.length === this.txOffset;
  }


  public formatPreviewTxDate(unFormatedDate:number | Date){
    let date = new Date(unFormatedDate);
    let obj:any = {}

    obj.dayOfMonth = this.sharedService.daysOfTheMonthOptions.find(option=>option.value==String(date.getDate())).value;

    obj.month = this.sharedService.monthsOptions.find(option=>option.value==String(date.getMonth()+1)).label.substr(0,3).toUpperCase();

    return `${obj.month} ${obj.dayOfMonth}`;
  }

  public formatDate(unFormatedDate:number | Date){
    let formatedDate = this.sharedService.formatDate(unFormatedDate);
    
    formatedDate.minutes = <any>(formatedDate.minutes<10? `0${String(formatedDate.minutes)}` : formatedDate.minutes);
    formatedDate.hours = formatedDate.hours==0? 12:formatedDate.hours;
    formatedDate.hours = formatedDate.hours>12? formatedDate.hours-12:formatedDate.hours;
    return `${formatedDate.month} ${formatedDate.dayOfMonth} ${formatedDate.year}, ${formatedDate.hours}:${formatedDate.minutes}:${formatedDate.seconds} ${formatedDate.hours>=12? 'PM':'AM'}`;
  }


  public formatAmount(value){
    return this.sharedService.formatBalance(value, 0); 
  }

  public selectTx(tx){
    this.txModal.show();
    this.txSelected = tx;
    this.txSelected.icon = this.iconFromtxType(this.txSelected.type);
  }

  public assetFromCurrency(currency:string){
    return this.currencies.find(asset=>asset.currency==currency);
  }

  public pricingInUSD(){
    return {basic:'4.95', plus:'9.95', professional:'14.95', enterprise:'19.95' };
  }

  public newBigNumber(value){
    return new BigNumber(value);
  }
  



}
