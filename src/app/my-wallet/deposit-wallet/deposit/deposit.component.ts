import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalDirective, ToastService } from 'ng-uikit-pro-standard';
import {DepositWalletService} from '../deposit-wallet.service';
@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {

  @Input() userInfo;
  @Input() currencyInfo;

  @ViewChild('depositModal') depositModal:ModalDirective;

  public depositWallet;

  constructor(private depositWalletService:DepositWalletService) { }

  ngOnInit() {
  }

  async openDepositModal(){
    this.depositModal.show();
    this.depositWallet = null;
    this.depositWallet = await this.depositWalletService.getDepositWallet(this.currencyInfo.currency, this.userInfo.uid);
  }

}
