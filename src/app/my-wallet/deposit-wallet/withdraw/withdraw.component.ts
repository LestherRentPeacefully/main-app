import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {SharedService} from '../../../shared/shared.service';
import { ModalDirective, ToastService } from 'ng-uikit-pro-standard';
import {DepositWalletService} from '../deposit-wallet.service';
import {environment} from '../../../../environments/environment';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import {ValidationsService} from '../../../validations.service';
import {BigNumber} from 'bignumber.js';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {

  app = environment.app;
  state = 0;
  withdrawalFee;
  isInternal;
  submitting:any = {};

  @Input() userInfo;
  @Input() currencyInfo;

  @ViewChild('withdrawalModal') withdrawalModal:ModalDirective;

  private toastOptions = this.sharedService.toastOptions;

  public withdrawalForm = this.fb.group({
    code: ['', [Validators.required]],
    to: ['', [Validators.required]],
    amount: ['']
  });

  constructor(private sharedService:SharedService, 
              private depositWalletService:DepositWalletService,
              private toast: ToastService,
              private validationsService:ValidationsService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.withdrawalForm.get('amount').setValidators([Validators.required, this.validationsService.isGreaterThan(0), this.hasEnoughBalance()]);
  }


  async next(){
    
    const req = this.withdrawalForm.value;
    this.submitting[this.state.toString()] = true;

    if(this.state==0){

      if(this.withdrawalForm.get('to').invalid) {
        this.toast.error('', 'Enter an address', this.toastOptions);
        this.submitting[this.state.toString()] = false;
        return;
      }

      this.withdrawalForm.disable();
      const response = await this.depositWalletService.checkWithdrawalAddress({
        currency: this.currencyInfo.currency,
        address: req.to
      });

      if (response.success) {
        this.state++;
        this.isInternal = response.data.isInternal;
        if(this.isInternal) this.withdrawalFee = '0';
      } else {
        console.error(response.error.message);
        this.toast.error('', response.error.message, this.toastOptions);
      }



    } else if(this.state==1){
      if (this.withdrawalForm.get('amount').invalid && (this.withdrawalForm.get('amount').errors.required || this.withdrawalForm.get('amount').errors.invalidAmount)) {
        this.toast.error('', 'Amount must be greater than 0', this.toastOptions);
      }else if(this.withdrawalForm.get('amount').invalid && this.withdrawalForm.get('amount').errors.insufficientFunds){
        this.toast.error('', 'Insufficient funds', this.toastOptions);
      } else {
        

        this.withdrawalForm.disable();

        const response = await this.depositWalletService.sendWithdrawalCode();
  
        if (response.success) {
          this.state++;
        } else {
          console.error(response.error.message);
          this.toast.error('', response.error.message, this.toastOptions);
        }
      }
      



    } else if(this.state==2){

      if (this.withdrawalForm.get('code').invalid){
        this.toast.error('', 'Enter code', this.toastOptions);
      }else{

        this.withdrawalForm.disable();
        const response = await this.depositWalletService.withdraw({
          currency: this.currencyInfo.currency,
          code: req.code,
          to: req.to,
          amount: req.amount
        });
  
        if (response.success) {
          this.withdrawalModal.hide();
          this.toast.success('', 'Withdrawal submitted', this.toastOptions);
        } else {
          console.error(response.error.message);
          this.toast.error('', response.error.message, this.toastOptions);
        }
      }
    }

    this.withdrawalForm.enable();
    this.submitting[this.state.toString()] = false;
  }


  async openWithdrawalModal(){
    this.withdrawalModalClosed();

    this.withdrawalModal.show();
    this.withdrawalFee = null;

    const response = await this.depositWalletService.getWithdrawalFee({currency:this.currencyInfo.currency});
    if (response.success) {
      this.withdrawalFee = response.data;
    } else {
      console.error(response.error.message);
      this.toast.error('', response.error.message, this.toastOptions);
    }
  }


  withdrawalModalClosed(){
    this.state = 0;
    this.withdrawalForm.reset();
    for(let i = 0; i<=this.state; i++){
      this.submitting[i.toString()] = false;
    }
  }

  get amountToPay(){
    return new BigNumber(this.withdrawalForm.get('amount').value).plus(this.withdrawalFee).toString(10);
  }

  get maxAmount(){
    if (new BigNumber(this.userInfo.balances[this.currencyInfo.currency].available).isGreaterThanOrEqualTo(this.withdrawalFee)) {
      return new BigNumber(this.userInfo.balances[this.currencyInfo.currency].available).minus(this.withdrawalFee).toString(10);
    }
    return '0';
  }


  public hasEnoughBalance(){
    return (control:AbstractControl)=>{
      const amountToPay = new BigNumber(control.value).plus(this.withdrawalFee).toString(10);
      return new BigNumber(this.userInfo.balances[this.currencyInfo.currency].available).isGreaterThanOrEqualTo(amountToPay)? null : {insufficientFunds:true};
    }
  }


}
