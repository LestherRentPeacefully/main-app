import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

/*
Components
*/
import {MyWalletComponent} from './my-wallet.component';


import { GuardsService } from '../guards/guards.service';




const appRoutes:Routes = [
	{
		path:'', //my-wallet
		component:MyWalletComponent,
		// canActivate: [GuardsService]
		// children:[
		// 	{
		// 		path:'send',
		// 		component:SendComponent
		// 	}
		// ]
	}
]



@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class myWalletRoutingModule {}