/* Angular 2 modules */

import { NgModule } from '@angular/core';


/*Routes*/

import {myWalletRoutingModule} from './myWalletRoutes';


/* Shared modules */

import { SharedModule } from '../shared/shared.module';


/* Components */

import { MyWalletComponent } from './my-wallet.component';
import { SendComponent } from './identity-wallet/send/send.component';
import { ReceiveComponent } from './identity-wallet/receive/receive.component';
import { TransactionsComponent as IdentityTransactionsComponent } from './identity-wallet/transactions/transactions.component';
import { IdentityWalletComponent } from './identity-wallet/identity-wallet.component';
import { DepositWalletComponent } from './deposit-wallet/deposit-wallet.component';
import { DepositComponent } from './deposit-wallet/deposit/deposit.component';
import { TransactionsComponent } from './deposit-wallet/transactions/transactions.component';
import { WithdrawComponent } from './deposit-wallet/withdraw/withdraw.component';

@NgModule({
  imports: [
    SharedModule,
    myWalletRoutingModule
  ],
  declarations: [
    MyWalletComponent, 
    SendComponent, 
    ReceiveComponent, 
    IdentityTransactionsComponent, 
    IdentityWalletComponent, 
    DepositWalletComponent, 
    DepositComponent, 
    TransactionsComponent, WithdrawComponent]
})
export class MyWalletModule { }
