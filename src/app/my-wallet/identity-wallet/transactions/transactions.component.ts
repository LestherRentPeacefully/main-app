import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {ToastService} from 'ng-uikit-pro-standard';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { environment } from '../../../../environments/environment';
import {EthereumService} from '../ethereum.service';
import {SharedService} from '../../../shared/shared.service';
import {BigNumber} from 'bignumber.js';

@Component({
  selector: 'app-identity-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  private toastOptions = this.sharedService.toastOptions;
  public txs:any;
  public txSelected:any;
  public etherscanURL = environment.etherscanExplorer;
  public loadingTxs = false;
  public page:number;
  public offset = 5;

  @Input() identityWallet;
  
  @ViewChild('txModal') public txModal:ModalDirective;

  constructor(private toast: ToastService,
              private sharedService:SharedService,
              private ethereumService:EthereumService) { }

  ngOnInit() {
    this.page = 1;
    this.getTransactions();

  }

  ngOnDestroy(){
  }


  public nextPage():void{
    this.page++;
    this.getTransactions();
  }

  public previousPage():void{
    this.page--;
    this.getTransactions();
  }

  private async getTransactions(){
    this.loadingTxs = true;
    try{
      let response = await this.ethereumService.getTransactions({address:this.identityWallet.address,page:this.page,offset:this.offset});
      if (response.status==1 || response.message=='No transactions found') {
        this.txs = response.result;
      } else {
        console.error(response.message);
       this.toast.error('','There was an error. Try again later', this.toastOptions);
      }

    }catch(err){
      console.error(err.message || err);
      this.toast.error('','There was an error. Try again later', this.toastOptions);
    }
    this.loadingTxs = false;
     
  }


  public formatValue(value){
    return new BigNumber(value).times(new BigNumber(10).pow(-18)).decimalPlaces(10).toString(10); 
  }

  public newBigNumber(number){
    return new BigNumber(number);
  }

  public selectTx(tx){
    this.txModal.show();
    this.txSelected=tx;
  }
  public MathPow(base:number,exp:number):number{
    return Math.pow(base,exp);
  } 

}
