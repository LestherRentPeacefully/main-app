import { Component, OnInit,ViewChild, Input } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ModalDirective, ToastService } from 'ng-uikit-pro-standard';
import {EthereumService} from './ethereum.service';
import {SharedService} from '../../shared/shared.service';
import {timer} from 'rxjs';
 
@Component({
  selector: 'app-identity-wallet',
  templateUrl: './identity-wallet.component.html',
  styleUrls: ['./identity-wallet.component.css']
})
export class IdentityWalletComponent implements OnInit {
  public chart:any;
  public exportingWallet;
  public sendingLink;
  private toastOptions = this.sharedService.toastOptions;
  public status = 0;
  public subscriber;
  public identityWallet;
  public balance;

  public itemForm = this.fb.group ({
    verificationCode: ['',[ Validators.required ]],
  });

  @Input() userInfo;
  
  @ViewChild('exportModal') public exportModal:ModalDirective; 

  constructor(private ethereumService:EthereumService,
    private toast: ToastService,
    private fb: FormBuilder,
    private sharedService:SharedService) {
  }

  async ngOnInit() {
    
    this.identityWallet = await this.ethereumService.getIdentityWallet(this.userInfo.uid);

    // this.subscriber = timer(0, 30*1000).subscribe(async()=>{

      try {
        let response = await this.ethereumService.getBalances();

        if(response.success) this.balance = response.data.ETH;
        else console.error(response.error.message);
      } catch (err) {
        console.error(err.message || err);
      }
    // });
      
  }

  ngOnDestroy(){
    // if(this.subscriber) this.subscriber.unsubscribe();
  }


  public formatBalance(value, decimals){
    return this.sharedService.formatBalance(value, decimals); 
  }
  

  public async openExportWalletModal(){
    this.sendingLink = true;
    this.exportingWallet = false;
    this.status = 0;
    this.exportModal.show();

    try {
      let response = await this.ethereumService.sendAndUploadWalletFileVerificationCode();
      
      if (response.success) {
        this.status = 1;
      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'', this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.sendingLink = false;
    
  }


  public async exportWallet(){
    this.exportingWallet = true;
    let req = this.itemForm.value;
    this.itemForm.disable();

    try {
      let response = await this.ethereumService.getWalletFileAndVerifyCode({code:req.verificationCode});
      
      if (response.success) {
        this.ethereumService.downloadWalletFile(response.data);
        this.exportModal.hide();
        this.toast.success('You may now access your wallet','Wallet File downloaded', this.toastOptions);
      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'', this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }

    this.exportingWallet = false;
    this.itemForm.enable();
  }

  
  public onHidden(){
    this.itemForm.reset();
    this.sendingLink = false;
    this.exportingWallet = false;
    this.status = 0;
  }  


  
}
