import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import { environment } from '../../../environments/environment';
import * as FileSaver  from 'file-saver';
import { AngularFireAuth } from '@angular/fire/auth';
import {SharedService} from '../../shared/shared.service';
import * as firebase from 'firebase/app';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class EthereumService {
	private APIURL = environment.APIURL;
  public balances = new BehaviorSubject(undefined);

  constructor(private http: HttpClient, 
              public afAuth: AngularFireAuth,
              private sharedService:SharedService) { }

  public async getIdentityWallet(uid:string){
    const db = firebase.firestore();
    return (await db.collection(`wallets/identity/public`).where('uid','==',uid).limit(1).get()).docs[0].data();
  }


  public async estimateGasAndGasPrice(req:{from:string,to:string,amount:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/ethereum/estimateGasAndGasPrice`, req, _httpOptions).toPromise();
  }

  public async send(req:{to:string,amount:string,gasPrice:string,gasLimit:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/ethereum/sendTx`, req, _httpOptions).toPromise();
  }

  public async getBalances(): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/ethereum/getBalances`,{},_httpOptions).toPromise();
  }

  public getBalancesWithObservable(): Observable<any>{
    return new Observable((observer)=>{
      this.afAuth.auth.currentUser.getIdToken(true).then((token)=>{
        let _httpOptions:any = {};
        _httpOptions.headers = httpOptions.headers.append('Authorization',token);
        this.http.post(`${this.APIURL}/ethereum/getBalances`,{},_httpOptions).subscribe(response=>observer.next(response));
      }).catch((err)=>observer.error(err.message));
    }); 
  }  

  public async getTransaction(req:{txHash:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/ethereum/getTransaction`,req,_httpOptions).toPromise(); 
  }

  public async sendAndUploadWalletFileVerificationCode(): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/ethereum/sendAndUploadWalletFileVerificationCode`,{}, _httpOptions).toPromise(); 
  }

  public async getWalletFileAndVerifyCode(req:{code:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/ethereum/getWalletFileAndVerifyCode`,req,_httpOptions).toPromise();  
  }

  public getTransactions(req:{address:string,page:number,offset:number}): Promise<any>{
    let path =`/api?module=account&action=txlist&address=${req.address}&sort=desc&page=${req.page}&offset=${req.offset}`;
    return this.http.get(`${environment.etherscanURL}/${path}`,httpOptions).toPromise();
  }

  // public getTransactions(req:{address:string,currency:string}): Promise<any>{
  //   return this.http.post(`${this.APIURL}/ethereum/getTransactions`,req,httpOptions).toPromise();
  // }

  public downloadWalletFile(V3:any):void{
    const fileName = `UTC--${new Date().toJSON().replace(/:/g,'-')}--${V3.address}`
    const blob = new Blob([JSON.stringify(V3)], {type: "text/json;charappend=utf-8"});
    FileSaver.saveAs(blob, fileName);
  }




}

