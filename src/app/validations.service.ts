import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import * as firebase from 'firebase/app';
import {BigNumber} from 'bignumber.js';


@Injectable({
  providedIn: 'root'
})
export class ValidationsService {

  constructor() { }



/*
Account creation
*/

  public uniqueEmail(control: AbstractControl): Promise<any>{
    return new Promise((resolve, reject) => {
      if(control.dirty) {
      	firebase.firestore().collection('users')
    		.where('email','==',control.value.toLowerCase())
        .limit(1)
    		.get().then((snapshot)=>{
          if (snapshot.empty) { 
            // console.log("email not found")
            resolve(null);	                    
          } else {
            // console.log("email found")
          	resolve({emailTaken: true});
          };
    		}).catch((err) => reject(err));	

      }
    });
  }

    public passwordValid(){
      return (control: AbstractControl)=>{
        if(control.dirty) {
          if (control.value.length >= 8) {
            const anUpperCase = /[A-Z]/;
            const aLowerCase = /[a-z]/;
            const aNumber = /[0-9]/;

            let numUpper = 0;
            let numLower = 0;
            let numNums = 0;

            for(let i = 0; i < control.value.length; i++){
              if(anUpperCase.test(control.value[i]))
                  numUpper++;
              else if(aLowerCase.test(control.value[i]))
                  numLower++;
              else if(aNumber.test(control.value[i]))
                  numNums++;
            }

            if(numUpper < 1 || numLower < 1 || numNums < 1){
              return {passwordInvalid: true}
            } else {
              return null;
            }
          }else {
            return {passwordInvalid: true}
          }
        }
      }
    }

    public isValidAddress(){
      return (control:AbstractControl)=>{
        if(control.dirty){
          if (/^0x[0-9a-fA-F]{40}$/.test(control.value)) { 
            return null;
          } else {
            return {invalidAddress:true};
          }
        }
      }
    }
      
      /*
    * Account Recovery
    * */

    public emailExists(control: AbstractControl): Promise<any> {
      return new Promise((resolve, reject) => {
        if(control.dirty) {
          firebase.firestore().collection('users')
          .where('email','==',control.value.toLowerCase())
          .limit(1)
          .get().then((snapshot)=>{
            if (snapshot.empty) { 
              resolve({emailNotFound: true});                        
            } else {
              resolve(null);
            };
          }).catch((err) => reject(err));
        }
      });
    }


    public emailIsNotVerified(control:AbstractControl):Promise<any>{
      return new Promise((resolve, reject) => {
        if(control.dirty) {
          firebase.firestore().collection('users')
          .where('email','==',control.value.toLowerCase())
          .limit(1)
          .get().then((snapshot)=>{
            if (snapshot.empty) { 
              // console.log("not found")
              resolve(null);
            } else {                            
              if (snapshot.docs[0].data().status.emailVerified) { 
                // console.log("verified")
                resolve(null);
              } else {
                // console.log("not verified")
                resolve({emailNotVerified:true});
              }                               
            }
            }).catch((err) => reject(err));
        }
      });
    }

  public checkTenantEmail(control: AbstractControl):Promise<any>{
    return new Promise((resolve,reject)=>{
      if(control.dirty) {
        firebase.firestore().collection('users')
        .where('email','==',control.value.toLowerCase())
        .limit(1)
        .get().then((snapshot)=>{
          if (snapshot.empty || snapshot.docs[0].data().status.banned || !snapshot.docs[0].data().status.emailVerified) {
            resolve({tenantEmailError:true});
          }else{
            resolve(null);
          }
        }).catch((err)=>reject(err.message));
      }
    });
  }

  public isValidEmail(email:string):boolean{
    const EMAIL_REGEXP = /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/;
    return EMAIL_REGEXP.test(email);
  }

  public isGreaterThan(amount : number | string | BigNumber){
    return (control:AbstractControl)=>{
      return new BigNumber(control.value).isGreaterThan(amount)? null : {invalidAmount:true};
    }
  }

}
