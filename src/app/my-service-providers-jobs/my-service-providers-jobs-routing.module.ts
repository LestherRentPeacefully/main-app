import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


/* Components */
import { MyServiceProvidersJobsComponent } from './my-service-providers-jobs.component';
import { AllComponent } from './all/all.component';
import { DetailsComponent } from './details/details.component';


const routes: Routes = [{
  path:'',
  component:MyServiceProvidersJobsComponent,
  children:[
    {
      path:'all',
      component:AllComponent
    },
    {
      path:':id',
      component:DetailsComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyServiceProvidersJobsRoutingModule { }
