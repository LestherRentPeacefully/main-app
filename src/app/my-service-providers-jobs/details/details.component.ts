import { Component, OnInit, ViewChild } from '@angular/core';
import {SharedService} from '../../shared/shared.service';
import {ToastService, ModalDirective } from 'ng-uikit-pro-standard';
import {AuthService} from '../../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import {MyServiceProvidersJobsService} from '../my-service-providers-jobs.service';
import { combineLatest } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';
import {BigNumber} from 'bignumber.js';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  private toastOptions = this.sharedService.toastOptions;
  public userInfo:any;
  public currencies:any[];
  public currencySelected:any;
  private jobSub;
  private loadedOnce;
  private sub;
  public serviceProviderJob;
  public jobLoaded;
  public employerInfo;
  public serviceProviderInfo;
  public usersLoaded;
  public milestones:any[];
  private serviceProviderSkills = this.sharedService.serviceProviderSkills;
  public acceptingJob;
  public proposingMilestone;
  public deletingProposedMilestone;
  public payingMilestone;
  public milestoneSelected;
  public currencyPriceToPay:string;
  public actionToReject;
  public rejectingAction;
  public isDirectPayment;
  public isReleaseMilestone;
  public providingFeedback;

  public milestoneForm = this.fb.group({
    amount:['', [Validators.required, Validators.min(5)]],
    details:['', [Validators.required, Validators.maxLength(50)]],
  });

  public provideFeedbackForm = this.fb.group({
    score: ['0',[Validators.required, Validators.min(1), Validators.max(5)]],
    qualityOfWork:['0'],
    finishedOnTime:['0'],
    paymentPromptness:['0'],
    review: ['',[Validators.maxLength(200) ]],
  });

  @ViewChild('rejectionModal') public rejectionModal:ModalDirective;
  @ViewChild('proposeMilestoneModal') public proposeMilestoneModal:ModalDirective;
  @ViewChild('milestonePaymentModal') public milestonePaymentModal:ModalDirective;
  @ViewChild('provideFeedbackModal') public provideFeedbackModal:ModalDirective;

  constructor(
    public sharedService:SharedService,
    private authService:AuthService,
    private toast: ToastService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private myServiceProvidersJobsService:MyServiceProvidersJobsService) { }

  ngOnInit() {

    this.sub = combineLatest(this.route.paramMap, this.authService.userInfo).subscribe(([paramMap, userInfo])=>{
      const id = paramMap.get('id');
      this.userInfo = userInfo;

      if(!this.loadedOnce && this.userInfo){
        this.loadedOnce = true
        this.jobSub = this.myServiceProvidersJobsService.getMyServiceProviderJob$(id).subscribe(async (job)=>{
          this.serviceProviderJob = job;
          if(this.serviceProviderJob) this.serviceProviderJob.skillsRequired = this.serviceProviderSkills.filter(skill=> this.serviceProviderJob.skillsRequired[skill.id]);
          this.jobLoaded = true;
          
          if(this.serviceProviderJob && !this.milestones){
            [this.employerInfo, this.serviceProviderInfo] = await Promise.all([
              this.myServiceProvidersJobsService.getUser(this.serviceProviderJob.employer.uid), 
              this.myServiceProvidersJobsService.getUser(this.serviceProviderJob.serviceProvider.uid)
            ]);
            this.usersLoaded = true;
            this.milestones = await this.myServiceProvidersJobsService.getMilestones(id);
          }

        });

      }
    })
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
    if(this.jobSub) this.jobSub.unsubscribe();
  }


  viewProfile(uid:string, role:string){
    this.router.navigate(['/user', uid], { queryParams: {role: role}});
  }


  async acceptJob(){
    this.acceptingJob = true;
    try {
      const response = await this.myServiceProvidersJobsService.acceptJob({serviceProviderJobID:this.serviceProviderJob.id});
      if (response.success) {
        this.serviceProviderJob.status = 'ongoing';
        this.toast.success("You're ready to start", 'Congratulations!', this.toastOptions);
      } else {
        console.error(response.error.message);
        this.toast.error('',response.error.message, this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('', 'Error accepting job. Try again later', this.toastOptions);
    }
    this.acceptingJob = false;
  }


  openRejectionModal(action:string, milestone){
    this.actionToReject = action;
    this.milestoneSelected = milestone;
    this.rejectionModal.show();
  }


  async rejectionAction(){
    this.rejectingAction = true;
    if(this.actionToReject=='cancelJob'){
      await this.cancelJob();
    }else if(this.actionToReject=='cancelCreatedMilestone'){
      await this.cancelCreatedMilestone(this.milestoneSelected);
    }
    this.rejectingAction = false;
  }

  
  rejectionModalClosed(){
    this.milestoneSelected = null;
  }


  private async cancelJob(){
    try {
      const response = await this.myServiceProvidersJobsService.cancelJob({serviceProviderJobID:this.serviceProviderJob.id});
      if (response.success) {
        this.serviceProviderJob.status = 'canceled';
        this.rejectionModal.hide();
        this.toast.success('', 'Job canceled', this.toastOptions);
      } else {
        console.error(response.error.message);
        this.toast.error('',response.error.message, this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('', 'Error cancelling job. Try again later', this.toastOptions);
    }
  }
  


  async proposeMilestone(){
    if (!this.milestoneForm.get('amount').valid) {
      this.toast.error('', 'Amount must be greater than or equal to 5 USD', this.toastOptions);
    } else if(!this.milestoneForm.get('details').valid){
      this.toast.error('', 'Description required and must have max 50 characters', this.toastOptions);
    }else {
      
      this.proposingMilestone = true;
      let req = this.milestoneForm.value;
      this.milestoneForm.disable();
      
      try {
        const response = await this.myServiceProvidersJobsService.proposeMilestone({
          serviceProviderJobID:this.serviceProviderJob.id, 
          amount:req.amount, 
          details:req.details
        });

        if (response.success) {
          this.milestones.unshift({
            amount:req.amount, 
            details:req.details,
            status:'proposed',
            id:response.data
          });
          this.proposeMilestoneModal.hide();
          this.toast.success('', 'Milestone Proposed', this.toastOptions);

        } else {
          console.error(response.error.message);
          this.toast.error('',response.error.message, this.toastOptions);
        }
        
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('', 'Error proposing milestone. Try again later', this.toastOptions);
      }
      this.proposingMilestone = false;
      this.milestoneForm.enable();
    }
  }


  milestoneProposalModalClosed(){
    this.milestoneForm.reset();
  }


  async deleteProposedMilestone(milestone:any){
    this.deletingProposedMilestone = true;
    try {
      const response = await this.myServiceProvidersJobsService.deleteProposedMilestone({milestoneID:milestone.id});
      if (response.success) {
        const index = this.milestones.findIndex((item)=>item.id==milestone.id);
        this.milestones.splice(index, 1);
        this.toast.success('', 'Milestone deleted', this.toastOptions);

      } else {
        console.error(response.error.message);
        this.toast.error('',response.error.message, this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('', 'Error deleting milestone. Try again later', this.toastOptions);
    }
    this.deletingProposedMilestone = false;
  }


  async openMilestonePaymentModal(milestone, isDirectPayment){
    this.milestonePaymentModal.show();
    this.isDirectPayment = isDirectPayment;
    this.isReleaseMilestone = milestone && milestone.status=='created';
    this.milestoneSelected = milestone;

    if(milestone){
      this.milestoneForm.get('amount').setValue(milestone.amount);
      this.milestoneForm.get('details').setValue(milestone.details);
      this.milestoneForm.disable();
    }else{
      this.milestoneForm.enable();
    }

    if(this.isReleaseMilestone) {
      this.currencySelected = {
        currency:milestone.currencyForPayment.currency,
        name:milestone.currencyForPayment.name,
      }
      this.currencies = [this.currencySelected];
      
    }
    else {
      this.currencySelected = null;
      this.currencies = await this.sharedService.getCurrencies();
    }
    
  }


  milestonePaymentModalClosed(){
    this.currencies = null;
    this.currencySelected = null;
    this.milestoneSelected = null;
    this.currencyPriceToPay = null;
    this.milestoneForm.reset();
  }


  async selectCurrency(item:any){
    if (!this.isReleaseMilestone && !this.payingMilestone && (!this.currencySelected || (this.currencySelected && this.currencyPriceToPay))) {

      this.currencySelected = item;
      this.currencyPriceToPay = null;
      try {
        const response = await this.myServiceProvidersJobsService.getCurrencyPrice({currency:this.currencySelected.currency});
        if (response.success) {
          this.currencyPriceToPay = response.data.price.USD;
          
        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'',this.toastOptions);
        }
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('There was an error. Try again later','', this.toastOptions);
      }
    }
  }


  async milestonePayment(){
    if (!this.milestoneSelected && !this.milestoneForm.get('amount').valid) {
      this.toast.error('', 'Amount must be greater than or equal to 5 USD', this.toastOptions);
    } else if(!this.milestoneSelected && !this.milestoneForm.get('details').valid){
      this.toast.error('', 'Description required and must have max 50 characters', this.toastOptions);
    } else {
     
      const amountToPay = this.isReleaseMilestone? 
        this.milestoneSelected.currencyForPayment.amount : 
        new BigNumber(this.milestoneForm.get('amount').value).div(this.currencyPriceToPay).decimalPlaces(8).toString(10);

      if (this.isReleaseMilestone || (this.userInfo.balances[this.currencySelected.currency] && new BigNumber(this.userInfo.balances[this.currencySelected.currency].available).isGreaterThanOrEqualTo(amountToPay))) {
        
        this.payingMilestone = true;
        this.milestoneForm.disable();
        try {

            let response;

            if(this.isDirectPayment){
              response = await this.myServiceProvidersJobsService.sendPayment({
                currency:this.currencySelected.currency,
                serviceProviderJobID:this.serviceProviderJob.id,
                amount:this.milestoneForm.value.amount,
                details:this.milestoneForm.value.details
              });

            } else if(this.isReleaseMilestone){
              response = await this.myServiceProvidersJobsService.releaseMilestone({
                milestoneID:this.milestoneSelected.id,
                serviceProviderJobID:this.serviceProviderJob.id
              });

            } else {
              response = await this.myServiceProvidersJobsService.createMilestone({
                currency:this.currencySelected.currency,
                milestoneID:this.milestoneSelected? this.milestoneSelected.id : null,
                serviceProviderJobID:this.serviceProviderJob.id,
                amount:this.milestoneForm.value.amount,
                details:this.milestoneForm.value.details
              });
            }

            if (response.success) {
              if(this.isDirectPayment){
                this.milestones.unshift({
                  amount:this.milestoneForm.value.amount,
                  details:this.milestoneForm.value.details,
                  status:'released',
                  id:response.data,
                  currencyForPayment:{
                    currency:this.currencySelected.currency,
                    amount:amountToPay,
                    name:this.currencySelected.name
                  }
                });

                this.serviceProviderJob.amountPaid = new BigNumber(this.serviceProviderJob.amountPaid).plus(this.milestoneForm.value.amount).toString(10);
                
                if(this.serviceProviderJob.status=='ongoing' && new BigNumber(this.serviceProviderJob.amountPaid).isGreaterThanOrEqualTo(this.serviceProviderJob.price)){
                  this.serviceProviderJob.status = 'completed';
                  this.serviceProviderJob.culminationTS = new Date().getTime();
                }
                this.toast.success('', 'Payment sent successfully', this.toastOptions);


              } else if(this.isReleaseMilestone){
                const index = this.milestones.findIndex((item)=>item.id==this.milestoneSelected.id);
                this.milestones[index].status = 'released';
                
                this.serviceProviderJob.amountPaid = new BigNumber(this.serviceProviderJob.amountPaid).plus(this.milestoneForm.value.amount).toString(10);
                
                if(this.serviceProviderJob.status=='ongoing' && new BigNumber(this.serviceProviderJob.amountPaid).isGreaterThanOrEqualTo(this.serviceProviderJob.price)){
                  this.serviceProviderJob.status = 'completed';
                  this.serviceProviderJob.culminationTS = new Date().getTime();
                }
                this.toast.success('', 'Milestone released successfully', this.toastOptions);


              } else{
              
                if(this.milestoneSelected){

                  const index = this.milestones.findIndex((item)=>item.id==this.milestoneSelected.id);
                  this.milestones[index].status = 'created';
                  this.milestones[index].currencyForPayment = {
                    currency:this.currencySelected.currency,
                    amount:amountToPay,
                    name:this.currencySelected.name
                  }
                  
                }else{
                  this.milestones.unshift({
                    amount:this.milestoneForm.value.amount,
                    details:this.milestoneForm.value.details,
                    status:'created',
                    id:response.data,
                    currencyForPayment:{
                      currency:this.currencySelected.currency,
                      amount:amountToPay,
                      name:this.currencySelected.name
                    }
                  });
                }
                this.toast.success('', 'Milestone created successfully', this.toastOptions);
              }

              this.milestonePaymentModal.hide();
            
            }else{
              console.error(response.error.message);
              this.toast.error(response.error.message,'',this.toastOptions);
            }


        } catch (err) {
          console.error(err.message || err);
          this.toast.error('There was an error. Try again later','', this.toastOptions);
        }
        this.payingMilestone = false;
        this.milestoneForm.enable();

      }else{
        this.toast.error('Insufficient funds','', this.toastOptions);
      }
    }   
  }



  private async cancelCreatedMilestone(milestone){
    try {
      const response = await this.myServiceProvidersJobsService.cancelCreatedMilestone({milestoneID:milestone.id});
      if (response.success) {
        const index = this.milestones.findIndex((item)=>item.id==milestone.id);
        this.milestones[index].status = 'canceled';
        this.rejectionModal.hide();
        this.toast.success('', 'Milestone canceled', this.toastOptions);

      } else {
        console.error(response.error.message);
        this.toast.error('',response.error.message, this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('', 'Error canceling milestone. Try again later', this.toastOptions);
    }
  }



  openProvideFeedbackModal(){
    this.provideFeedbackModal.show();
    if (this.userInfo.uid==this.employerInfo.uid) {
      this.provideFeedbackForm.get('qualityOfWork').setValidators([Validators.required, Validators.min(1), Validators.max(5)]);
      this.provideFeedbackForm.get('qualityOfWork').updateValueAndValidity();
      this.provideFeedbackForm.get('finishedOnTime').setValidators([Validators.required, Validators.min(1), Validators.max(5)]);
      this.provideFeedbackForm.get('finishedOnTime').updateValueAndValidity();
      
    }else if(this.userInfo.uid==this.serviceProviderInfo.uid){
      this.provideFeedbackForm.get('paymentPromptness').setValidators([Validators.required, Validators.min(1), Validators.max(5)])
      this.provideFeedbackForm.get('paymentPromptness').updateValueAndValidity();
    }
  }


  provideFeedbackModalClosed(){
    this.provideFeedbackForm.reset();
  }


  selectScore(score:number, action:string){
    this.provideFeedbackForm.get(action).setValue(score);
  }


  async provideFeedBack(){
    this.providingFeedback = true;

    if (!this.provideFeedbackForm.get('qualityOfWork').valid || 
        !this.provideFeedbackForm.get('finishedOnTime').valid || 
        !this.provideFeedbackForm.get('paymentPromptness').valid) {

      if (this.userInfo.uid==this.employerInfo.uid) this.toast.error('','Please, rate the Service Provider', this.toastOptions);
      else if (this.userInfo.uid==this.serviceProviderInfo.uid) this.toast.error('','Please, rate the Employer', this.toastOptions);
      
    } else if(!this.provideFeedbackForm.get('review').valid){
      this.toast.error('',"You can't type more than 200 characteres", this.toastOptions);
      
    } else {
      let req = {
        serviceProviderJobID:this.serviceProviderJob.id,
        feedback:this.provideFeedbackForm.value
      };

      this.provideFeedbackForm.disable();
      try {

        const response = await this.myServiceProvidersJobsService.provideFeedBack(req);

        if (response.success) {
          if(this.userInfo.uid==this.employerInfo.uid) this.serviceProviderJob.employerProvidedFeedback = true;
          else if(this.userInfo.uid==this.serviceProviderInfo.uid) this.serviceProviderJob.serviceProviderProvidedFeedback = true;
          this.toast.success('','Feedback provided successfully', this.toastOptions);
          this.provideFeedbackModal.hide();
          
        } else {
          console.error(response.error.message);
          this.toast.error('',response.error.message, this.toastOptions);
        }
        
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('','Unexpected error submitting feedback', this.toastOptions);
      }
      this.provideFeedbackForm.enable();


    }
    this.providingFeedback = false;
  }



  public formatBalance(value){
    return this.sharedService.formatBalance(value, 0); 
  }

  public newBigNumber(value){
    return new BigNumber(value);
  }

  public formatDateString(date:number):string{
    let formatedDate = this.sharedService.formatDate(date);
    return `${formatedDate.month} ${formatedDate.dayOfMonth}, ${formatedDate.year}`;
  }

  public get openMessagesObj() {
    let obj = {
      chatName: this.serviceProviderJob.name,
      id: this.serviceProviderJob.chatID,
      user : this.serviceProviderInfo.uid!=this.userInfo.uid? this.serviceProviderInfo : this.employerInfo
    }
    return obj;
  }
  



  
}
