import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import {SharedService} from '../shared/shared.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MyServiceProvidersJobsService {

  private APIURL = environment.APIURL;

  constructor(
    private sharedService:SharedService,
    private http: HttpClient,
    private afs: AngularFirestore) { }


    getMyServiceProviderJob$(id:string){
      return this.afs.doc(`myServiceProvidersJobs/${id}`).valueChanges();
    }

    async getMyServiceProviderJobs(uid:string, role:string){
      const db = firebase.firestore();
      let jobs = [];
      let query:string;
      if(role=='tenant' || role=='landlord') query ='employer';
      if(role=='service provider') query ='serviceProvider';
      let snapshots = await db.collection(`myServiceProvidersJobs`).where(`${query}.uid`, '==', uid).orderBy('creationTS','desc').get();
      for(let doc of snapshots.docs){
        jobs.push(doc.data());
      }
      return jobs;
    }

    public async getUser(uid:string){
      const db = firebase.firestore();
      return (await db.doc(`users/${uid}`).get()).data();
    }

    async getMilestones(serviceProviderJobID:string){
      const db = firebase.firestore();
      let milestones = [];
      let snapshots = await db.collection(`milestones`).where('serviceProviderJobID', '==', serviceProviderJobID).orderBy('creationTS','desc').get();
      for(let doc of snapshots.docs){
        milestones.push(doc.data());
      }
      return milestones;
    }

    async acceptJob(req:{serviceProviderJobID:string}):Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/acceptJob`, req, _httpOptions).toPromise();
    }

    async cancelJob(req:{serviceProviderJobID:string}):Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/cancelJob`, req, _httpOptions).toPromise();
    }

    async proposeMilestone(req:{serviceProviderJobID:string, amount:number, details:string}):Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/proposeMilestone`, req, _httpOptions).toPromise();
    }

    async deleteProposedMilestone(req:{milestoneID:string}):Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/deleteProposedMilestone`, req, _httpOptions).toPromise();
    }

    public async getCurrencyPrice(req:{currency:string}): Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/getCurrencyPrice`, req, _httpOptions).toPromise();
    }

    public async createMilestone(req:{currency:string, milestoneID:string, serviceProviderJobID:string, amount:number, details:string}): Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/createMilestone`, req, _httpOptions).toPromise();
    }

    public async cancelCreatedMilestone(req:{milestoneID:string}): Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/cancelCreatedMilestone`, req, _httpOptions).toPromise();
    }

    public async releaseMilestone(req:{milestoneID:string, serviceProviderJobID:string}): Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/releaseMilestone`, req, _httpOptions).toPromise();
    }

    public async sendPayment(req:{currency:string, serviceProviderJobID:string, amount:number, details:string}): Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/sendPayment`, req, _httpOptions).toPromise();
    }

    public async provideFeedBack(req:{serviceProviderJobID:string, feedback:string}): Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/provideFeedBack`, req, _httpOptions).toPromise();
    }

    public async startChat(req:{serviceProviderJobID:string, content:string}): Promise<any>{
      let _httpOptions = await this.sharedService.getAuthHeader();
      return this.http.post(`${this.APIURL}/my-service-providers-jobs/startChat`, req, _httpOptions).toPromise();
    }




}
