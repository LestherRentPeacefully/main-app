import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../auth.service';
import {MyServiceProvidersJobsService} from '../my-service-providers-jobs.service';
import {SharedService} from '../../shared/shared.service';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {
  private toastOptions = this.sharedService.toastOptions;
  public userInfo$:any;
  public serviceProviderJobs:any[];

  constructor(
    public sharedService:SharedService,
    private myServiceProvidersJobsService:MyServiceProvidersJobsService,
    private authService:AuthService,) { }



  async ngOnInit() {
    this.userInfo$ = this.authService.userInfo.pipe(filter((userInfo=>userInfo)), tap(async (userInfo)=>{
      this.serviceProviderJobs = await this.myServiceProvidersJobsService.getMyServiceProviderJobs(userInfo.uid, userInfo.role)

    }));
  }


  formatDate(unformatedDate:number){
    let date = this.sharedService.formatDate(unformatedDate);
    return `${date.month} ${date.dayOfMonth}, ${date.year}`;
  }

}
