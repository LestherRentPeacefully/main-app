/* Angular 2 modules */

import { NgModule } from '@angular/core';


/* Routes */

import { MyServiceProvidersJobsRoutingModule } from './my-service-providers-jobs-routing.module';


/* Shared modules */

import { SharedModule } from '../shared/shared.module';


/* Components */

import { MyServiceProvidersJobsComponent } from './my-service-providers-jobs.component';
import { AllComponent } from './all/all.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  declarations: [MyServiceProvidersJobsComponent, AllComponent, DetailsComponent],
  imports: [
    SharedModule,
    MyServiceProvidersJobsRoutingModule
  ]
})
export class MyServiceProvidersJobsModule { }
