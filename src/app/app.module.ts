/* Angular 2 modules */

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


/* Firebase modules */

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';


/* Google Analytics modules */

import { GtagModule } from 'angular-gtag';


/*Bootrap modules  */

import {ToastModule,
        WavesModule,
        InputsModule,
        ModalModule,
        TabsModule,
        ButtonsModule,
        TooltipModule,
        CollapseModule,
        DropdownModule,
        CardsFreeModule,
        CharCounterModule,
        CarouselModule} from 'ng-uikit-pro-standard';


/* Shared modules*/

import { SharedModule } from './shared/shared.module';


/* Lazy loaded modules */

import { AppRoutingModule } from './app-routing.module';


/*Components */

import { AppComponent } from './app.component';
import { IntroComponent } from './intro/intro.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from './footer/footer.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';
import { NotificationsComponent } from './notifications/notifications.component';
 


@NgModule({
  declarations: [
    AppComponent,
    IntroComponent,
    NavBarComponent,
    FooterComponent,
    NotificationsComponent,
    HowItWorksComponent
  ],
  imports: [
    GtagModule.forRoot(environment.gaConfig),
    AngularFireModule.initializeApp(environment.firebase),
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    ToastModule.forRoot(),
    WavesModule.forRoot(),
    InputsModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    TooltipModule.forRoot(),
    CollapseModule.forRoot(),
    DropdownModule.forRoot(),
    CardsFreeModule.forRoot(),
    CharCounterModule.forRoot(),
    CarouselModule.forRoot(),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AppRoutingModule //It must go at the bottom
  ],
  providers: [],
  schemas: [ NO_ERRORS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule {

  // constructor(router: Router) {
  //     router.events.subscribe(d=>d)
  //   console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  // }

}
