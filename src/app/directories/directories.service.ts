import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import {SharedService} from '../shared/shared.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DirectoriesService {

  private APIURL = environment.APIURL;

  constructor(
    private http: HttpClient,
    private sharedService:SharedService) { }


  public getTotalAndServiceProviders(filter:any, offset:number):Promise<any>{
    return this.http.post(`${this.APIURL}/directory/getTotalAndServiceProviders`, {filter, offset}, httpOptions).toPromise();
  }

  public getTotalAndRealEstateAgents(filter:any, offset:number):Promise<any>{
    return this.http.post(`${this.APIURL}/directory/getTotalAndRealEstateAgents`, {filter, offset}, httpOptions).toPromise();
  }
}
