import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {RealEstateAgentsComponent} from './real-estate-agents.component';

const routes: Routes = [{
  path:'',
  component:RealEstateAgentsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RealEstateAgentsRoutingModule { }
