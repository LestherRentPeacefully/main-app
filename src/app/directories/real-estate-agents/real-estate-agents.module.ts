/* Angular 2 modules */

import { NgModule } from '@angular/core';

/* Google maps module */

import { AgmCoreModule } from '@agm/core';

import {environment} from '../../../environments/environment';


/*Routes*/

import { RealEstateAgentsRoutingModule } from './real-estate-agents-routing.module';


/* Shared modules */

import { SharedModule } from '../../shared/shared.module';


/* Components */


import { RealEstateAgentsComponent } from './real-estate-agents.component';


@NgModule({
  declarations: [RealEstateAgentsComponent],
  imports: [
    SharedModule,
    AgmCoreModule.forRoot({apiKey: environment.googleMaps.apiKey, libraries: ["places", "geometry"]}),
    RealEstateAgentsRoutingModule
  ]
})
export class RealEstateAgentsModule { }
