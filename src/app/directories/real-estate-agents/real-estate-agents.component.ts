import { Component, OnInit, ViewChild, NgZone, ElementRef } from '@angular/core';
import {SharedService} from '../../shared/shared.service';
import {ToastService, ModalDirective } from 'ng-uikit-pro-standard';
import { ActivatedRoute, Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import {BigNumber} from 'bignumber.js';
import {DirectoriesService} from '../directories.service';


@Component({
  selector: 'app-real-estate-agents',
  templateUrl: './real-estate-agents.component.html',
  styleUrls: ['./real-estate-agents.component.css']
})
export class RealEstateAgentsComponent implements OnInit {

  @ViewChild('filterModal') public filterModal:ModalDirective;
  @ViewChild("search")  public searchElementRef: ElementRef<HTMLInputElement>;
  @ViewChild("search2")  public searchElementRef2: ElementRef<HTMLInputElement>;

  private toastOptions = this.sharedService.toastOptions;
  public submitting: boolean;
  public total: number;
  public addressEntered: boolean;
  public location;
  public filter: any = {};
  public realEstateAgents: any[];
  

  constructor(
    private sharedService:SharedService,
    private router: Router,
    private toast: ToastService,
    private route: ActivatedRoute,
    private mapsAPILoader: MapsAPILoader,
    private directoriesService:DirectoriesService,
    private ngZone: NgZone
  ) { }

  async ngOnInit() {

    this.route.queryParams.subscribe(async params => {
      this.filter = params;
      this.realEstateAgents = null;
      this.total = null;
      
      if (!this.filter.lat || !this.filter.lng || !this.filter.radius) return this.addressEntered = false;

      this.submitting = true;

      this.addressEntered = true;

      this.location = {lat: this.filter.lat, lng: this.filter.lng, radius: this.filter.radius};

      const filter = {...this.filter};
      
      try {
        let response = await this.directoriesService.getTotalAndRealEstateAgents(filter, 0);
        if (response.success) {
          this.realEstateAgents = response.data.realEstateAgents;
          this.total = response.data.total;

        } else {
          console.error(response.error.message);
          this.toast.error(response.error.message,'Error', this.toastOptions);
        }
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('Unexpected error loading properties. Try again later','Error', this.toastOptions);
      }

      this.submitting = false;

    });

    await this.mapsAPILoader.load();
    this.newAutocompleteElement(this.searchElementRef.nativeElement);
    this.newAutocompleteElement(this.searchElementRef2.nativeElement);
  }




  private newAutocompleteElement(nativeElement:HTMLInputElement){

    let autocomplete = new google.maps.places.Autocomplete(nativeElement, {
      types: ["geocode"]
    });

    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {

        const place: google.maps.places.PlaceResult = autocomplete.getPlace();

        if(!place.geometry) return;
 
        const center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
        const corner = new google.maps.LatLng(place.geometry.viewport.getNorthEast().lat(), place.geometry.viewport.getNorthEast().lng())
        let radius: number | BigNumber = new BigNumber(google.maps.geometry.spherical.computeDistanceBetween(center, corner)).dividedBy(1000); //in Km
        radius = radius.plus(radius.dividedBy(10)).decimalPlaces(3).toNumber(); // + 10%

        this.router.navigate(['/real-estate-agents'], {
          queryParams: {
            lat: place.geometry.location.lat(), 
            lng: place.geometry.location.lng(),
            radius:radius
          },
          queryParamsHandling: 'merge' 
        });

        nativeElement.value = '';

      });
    });
  }



  public clearFilters(){
    this.filterModal.hide();
    this.router.navigate(['/real-estate-agents'], { queryParams: this.location});
  }

  public get filterUsed(){
    for(let key of Object.keys(this.filter)){
      if(key !='lat' && key!='lng' && key!='radius') return true;
    }
    return false;
  }



  viewProfile(uid:string){
    this.router.navigate(['/user', uid], { queryParams: {role: 'real estate agent'}});
  }





}
