/* Angular 2 modules */

import { NgModule } from '@angular/core';

/* Google maps module */

import { AgmCoreModule } from '@agm/core';

import {environment} from '../../../environments/environment';


/*Routes*/

import { ServiceProvidersRoutingModule } from './service-providers-routing.module';


/* Shared modules */

import { SharedModule } from '../../shared/shared.module';


/* Components */

import { ServiceProvidersComponent } from './service-providers.component';

@NgModule({
  declarations: [ServiceProvidersComponent],
  imports: [
    SharedModule,
    AgmCoreModule.forRoot({apiKey: environment.googleMaps.apiKey, libraries: ["places","geometry"]}),
    ServiceProvidersRoutingModule
  ]
})
export class ServiceProvidersModule { }
