import { Component, OnInit, ViewChild, NgZone, ElementRef } from '@angular/core';
import {SharedService} from '../../shared/shared.service';
import {ToastService, ModalDirective } from 'ng-uikit-pro-standard';
import { ActivatedRoute, Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import {BigNumber} from 'bignumber.js';
import {DirectoriesService} from '../directories.service';

@Component({
  selector: 'app-service-providers',
  templateUrl: './service-providers.component.html',
  styleUrls: ['./service-providers.component.css']
})
export class ServiceProvidersComponent implements OnInit {

  public serviceProviderSkills = this.sharedService.serviceProviderSkills;
  private toastOptions = this.sharedService.toastOptions;
  private sub:any;
  public submitting;
  public total:number;
  public addressEntered;
  public location;
  public filter:any = {};
  public serviceProviders:any[];

  @ViewChild('filterModal') public filterModal:ModalDirective;
  @ViewChild("search")  public searchElementRef: ElementRef<HTMLInputElement>;
  @ViewChild("search2")  public searchElementRef2: ElementRef<HTMLInputElement>;

  constructor(
    public sharedService:SharedService,
    private router: Router,
    private toast: ToastService,
    private route: ActivatedRoute,
    private mapsAPILoader: MapsAPILoader,
    private directoriesService:DirectoriesService,
    private ngZone: NgZone) { }

  ngOnInit() {

    this.sub = this.route.queryParams.subscribe(async params => {

      this.filter = params;
      this.filterModalModalClosed();
      this.serviceProviders = null;
      this.total = null;
      this.submitting = true;

      if (this.filter.lat && this.filter.lng && this.filter.radius) {

        this.addressEntered = true;
        this.location = {lat:this.filter.lat, lng:this.filter.lng, radius:this.filter.radius};

        let filter = {...this.filter};
        if(this.filter.skills && this.filter.skills.length>0) filter.skills = this.filter.skills.split(',');
        
        try {

          let response = await this.directoriesService.getTotalAndServiceProviders(filter, 0);
          if (response.success) {
            this.serviceProviders = response.data.serviceProviders;

            for(let i in this.serviceProviders){
              this.serviceProviders[i].skills = this.serviceProviderSkills.filter(item=>this.serviceProviders[i].skills[item.id]);
            }
            this.total = response.data.total;

          } else {
            console.error(response.error.message);
            this.toast.error(response.error.message,'Error', this.toastOptions);
          }
        } catch (err) {
          console.error(err.message || err);
          this.toast.error('Unexpected error loading properties. Try again later','Error', this.toastOptions);
        }
      }else{
        this.addressEntered = false;
      }
      this.submitting = false;
    });


    this.mapsAPILoader.load().then(() => {
      this.newAutocompleteElement(this.searchElementRef.nativeElement);
      this.newAutocompleteElement(this.searchElementRef2.nativeElement);
    });
  
  
  }



  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  
  public get selectedServiceProviderSkills() {
    return this.serviceProviderSkills.filter(skill=>(<any>skill).selected);
  }
  



  private newAutocompleteElement(nativeElement:HTMLInputElement){

    let autocomplete = new google.maps.places.Autocomplete(nativeElement, {
      types: ["geocode"]
    });

    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {

        let place: google.maps.places.PlaceResult = autocomplete.getPlace();

        if(place.geometry){
          let center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
          let corner = new google.maps.LatLng(place.geometry.viewport.getNorthEast().lat(), place.geometry.viewport.getNorthEast().lng())
          let _radius = new BigNumber(google.maps.geometry.spherical.computeDistanceBetween(center, corner)).dividedBy(1000); //in Km
          let radius = _radius.plus(_radius.dividedBy(10)).decimalPlaces(3).toNumber(); // + 10%

          this.router.navigate(['/service-providers'], {
            queryParams: {
              lat: place.geometry.location.lat(), 
              lng: place.geometry.location.lng(),
              radius:radius
            },
            queryParamsHandling: 'merge' 
          });

          nativeElement.value = '';

        }

      });
    });
  }




  public get filterUsed(){
    for(let key of Object.keys(this.filter)){
      if(key !='lat' && key!='lng' && key!='radius') return true;
    }
    return false;
  }


  selectServiceProviderSkill(skill:{name:string, id:string}){
    let index = this.serviceProviderSkills.findIndex(item => item.id == skill.id);
    (<any>this.serviceProviderSkills[index]).selected = !(<any>this.serviceProviderSkills[index]).selected;    
  }


  filterModalModalClosed(){
    if (this.filter.skills) {
      let skillsArray = this.filter.skills.split(',');
      this.serviceProviderSkills = this.serviceProviderSkills.map(skill=>{
        if( skillsArray.find(skillID=>skillID==skill.id) ) (<any>skill).selected = true;
        else (<any>skill).selected = false;
        return skill;
      });

    } else {
      this.serviceProviderSkills = this.serviceProviderSkills.map(skill=>{
        (<any>skill).selected = false;
        return skill;
      });
    }
  }

  
  editSkills(){
    this.filterModal.hide();
    let filterValues = {skills:''};
    for(let skill of this.serviceProviderSkills){
      if((<any>skill).selected) filterValues.skills += `,${skill.id}`
    }
    filterValues.skills = filterValues.skills.substring(1);
    Object.keys(filterValues).forEach(key => !filterValues[key] ? delete filterValues[key] : '');
    this.router.navigate(['/service-providers'], { queryParams: filterValues, queryParamsHandling: 'merge' });
  }


  

  public clearFilters(){
    this.filterModal.hide();
    this.router.navigate(['/service-providers'], { queryParams: this.location});
  }



  viewProfile(uid:string){
    this.router.navigate(['/user', uid], { queryParams: {role: 'service provider'}});
  }

}
