import { Component, OnInit } from '@angular/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {SharedService} from '../../../shared/shared.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder,Validators } from '@angular/forms';
import {RequestsService} from '../../admin.service';
import {BigNumber} from 'bignumber.js';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  public submitting;
  private toastOptions = this.sharedService.toastOptions;
  public actionSelected;
  private sub;
  public withdrawalID;
  public withdrawal;
  public senderInfo;
  public currencyInfo;

  public itemForm = this.fb.group({
    rejectionReason: ['', [Validators.required]],
    txHash: ['', [Validators.required]],
  });


  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private requestsService:RequestsService,
    private sharedService:SharedService,
    private router: Router,
    private toast: ToastService,
  ) { }

  ngOnInit() {
    this.sub = this.route.paramMap.subscribe(async (params)=>{
      this.withdrawalID = params.get('id');
      this.withdrawal = await this.requestsService.getWithdrawalRequest(this.withdrawalID),
      [this.senderInfo, this.currencyInfo] = await Promise.all([
        this.sharedService.getUser(this.withdrawal.uid),
        this.requestsService.getCurrency(this.withdrawal.currency)
      ]);
    });
  }


  ngOnDestroy(){
  	this.sub.unsubscribe();
  }

  public selectAction(action:string){
    this.actionSelected = action;
    this.itemForm.reset();
  }
  
  public formatDate(unFormatedDate:number | Date){
    let formatedDate = this.sharedService.formatDate(unFormatedDate);
    
    formatedDate.minutes = <any>(formatedDate.minutes<10? `0${String(formatedDate.minutes)}` : formatedDate.minutes);
    formatedDate.hours = formatedDate.hours==0? 12:formatedDate.hours;
    formatedDate.hours = formatedDate.hours>12? formatedDate.hours-12:formatedDate.hours;
    return `${formatedDate.month} ${formatedDate.dayOfMonth} ${formatedDate.year}, ${formatedDate.hours}:${formatedDate.minutes}:${formatedDate.seconds} ${formatedDate.hours>=12? 'PM':'AM'}`;
  }

  public fromCryptoToUSD(amount:string | number | BigNumber){
    return new BigNumber(amount).times(this.currencyInfo.price.USD).decimalPlaces(10).toString(10);
  }


  async onSubmit(){
    let req = {
      withdrawalID: this.withdrawalID,
      rejectionReason: this.itemForm.get('rejectionReason').value,
      txHash: this.itemForm.get('txHash').value,
    }
    
    this.submitting = true;
    this.itemForm.disable();

    try {
      if(this.actionSelected == 'reject'){
        const response = await this.requestsService.rejectWithdrawalRequest(req);

        if (response.success) {
          this.toast.success('Request rejected successfully','', this.toastOptions);
          this.router.navigate([`/admin/withdrawal/all`]);
        } else {
          this.toast.error(response.error.message,'', this.toastOptions);
        }



      }else if(this.actionSelected == 'process'){
        const response = await this.requestsService.processWithdrawalRequest(req);

        if (response.success) {
          this.toast.success('Request processed successfully','', this.toastOptions);
          this.router.navigate([`/admin/withdrawal/all`]);
        } else {
          this.toast.error(response.error.message,'', this.toastOptions);
        }
      }

    } catch (err) {
      console.error(err.message);
      this.toast.error('Unexpected error. Try again later','', this.toastOptions);
    }

    this.submitting = false;
    this.itemForm.enable();
  }



}
