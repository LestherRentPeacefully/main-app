import { Component, OnInit } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';
import {RequestsService} from '../../admin.service';
import {ToastService} from 'ng-uikit-pro-standard';
import {SharedService} from '../../../shared/shared.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
	public loadingRequest;
	public request;
	private dateSubscriber;
	private sub;
	public submiting;
	public page = 1;
	private toastOptions = this.sharedService.toastOptions;
	public actionSelected;

	public daysOfTheMonthOptions = this.sharedService.daysOfTheMonthOptions;
	public monthsOptions = this.sharedService.monthsOptions;
	public birthYearOptions = this.requestsService.birthYearOptions.sort((a,b)=>{
		return Number(b.value) - Number(a.value);
	});
	public expirationYearOptions = this.requestsService.expirationYearOptions;
  public IDTypeOptions = this.requestsService.IDTypeOptions;
  public countriesOptions = this.sharedService.countriesOptions;

  public requestID;

  public itemForm1 = this.fb.group({
    firstName: ['',[Validators.required]],
    lastName: ['',[Validators.required]],
    brokerage: [''],
    licenceNo: [''],
    dateOfBirth: this.fb.group({
	    month: ['',[Validators.required]],
	    day: ['',[Validators.required]],
	    year: ['',[Validators.required]]
  	}),
  });

  public itemForm2 = this.fb.group({
    country: ['',[Validators.required]],
    IDType: ['',[Validators.required]],
    IDExpirationDate: this.fb.group({
      month: ['',[Validators.required]],
      day: ['',[Validators.required]],
      year: ['',[Validators.required]]
    }),
  });

  public itemForm3 = this.fb.group({
  	rejectionReason: ['',[Validators.required]]
  });



  constructor(private requestsService:RequestsService,
  						private fb: FormBuilder,
  						private sharedService:SharedService,
  						private route: ActivatedRoute,
              private router: Router,
  						private toast: ToastService) { }

  ngOnInit() {
  	this.sub = this.route.paramMap.subscribe(async (params)=>{
  		this.requestID = params.get('id');
	  	this.loadingRequest = true;
	  	try{
	  		this.request = await this.requestsService.getIdVerificationRequest(this.requestID);

	  		if(this.request){

		  		this.itemForm1.patchValue({
		  			firstName:this.request.firstName.replace(/\b\w/g, l => l.toUpperCase()),
		  			lastName:this.request.lastName.replace(/\b\w/g, l => l.toUpperCase()),
		  			dateOfBirth:this.request.dateOfBirth
		  		});

		  		if(this.request.role=='real estate agent'){		  	
		  			this.itemForm1.get('brokerage').setValidators(Validators.required), this.itemForm1.get('licenceNo').setValidators(Validators.required);
          	this.itemForm1.get('brokerage').updateValueAndValidity(), this.itemForm1.get('licenceNo').updateValueAndValidity();        		

			  		this.itemForm1.patchValue({
			  			brokerage:this.request.brokerage,
			  			licenceNo:this.request.licenceNo,
			  		});
		  		}

		  		this.itemForm2.setValue({
	          country:this.sharedService.countriesOptions.find((element)=> element.label.toLowerCase()==this.request.country.toLowerCase()).value,
	          IDType:this.requestsService.IDTypeOptions.find((element)=> element.label.toLowerCase()==this.request.IDType.toLowerCase()).value,
	          IDExpirationDate:this.request.IDExpirationDate
	        });
	  		}

	  	}catch(err){
	  		console.log(err.message || err);
	  		this.toast.error('Unexpected error loading Requests','', this.toastOptions);
	  	}
	  	this.loadingRequest = false;
  	});




    this.dateSubscriber = this.itemForm1.get(`dateOfBirth.month`).valueChanges.subscribe((selectedMonth)=>{

      if (selectedMonth=='1' || selectedMonth=='3' || 
          selectedMonth=='5' || selectedMonth=='7' || 
          selectedMonth=='8' || selectedMonth=='10' || 
          selectedMonth=='12') { 
        this.daysOfTheMonthOptions = this.sharedService.daysOfTheMonthOptions;

      }else if(selectedMonth=='2'){
        this.daysOfTheMonthOptions = this.sharedService.daysOfTheMonthOptions.slice(0,28);
      }else{
        this.daysOfTheMonthOptions = this.sharedService.daysOfTheMonthOptions.slice(0,30);
      }
    });



  }

  ngOnDestroy(){
  	this.sub.unsubscribe();
  	this.dateSubscriber.unsubscribe();
  }

  public selectAction(action:string){
  	this.actionSelected = action;
  }


  public changePage(page:number){
    this.page = page;
  }

  nextPage(){
    this.page++;
  }

  previewsPage(){
    this.page--;
  }


  async onSubmit(){
  	if(this.page!=3) {
      this.page++;


    }else{
			this.submiting = true;

    	if (this.actionSelected=='reject') {
				let rejectionReason = this.itemForm3.get('rejectionReason').value;
				this.itemForm3.disable();

    		try{
					const response = await this.requestsService.rejectIDRequest({
						rejectionReason, 
						requestID:this.requestID, 
						role:this.request.role, 
						uid:this.request.uid
					});

					if (response.success) {
						this.toast.success('Request rejected successfully','', this.toastOptions);
						this.router.navigate([`/admin/id-verification/all`]);
					} else {
						console.error(response.error.message);
     				this.toast.error(response.error.message,'', this.toastOptions);
					}

    		}catch(err){
    			console.error(err.message);
    			this.toast.error('Unexpected error rejecting Request','', this.toastOptions);
				}
				this.itemForm3.enable();



    	} else if(this.actionSelected=='verify'){
    		let obj = {...this.itemForm1.value, ...this.itemForm2.value};

    		this.itemForm1.disable();
     		this.itemForm2.disable();

     		try{
					const response = await this.requestsService.verifyIDRequest(obj, this.requestID, this.request.role, this.request.uid);
     			if (response.success) { 
	    			this.toast.success('Request verified successfully','', this.toastOptions);
	     			this.router.navigate([`/admin/id-verification/all`]);
     			} else {
     				console.error(response.error.message);
     				this.toast.error(response.error.message,'', this.toastOptions);
					}
					 
     		}catch(err){
					console.error(err.message);
					this.toast.error('Unexpected error verifying Request','', this.toastOptions);
				}
					
				this.itemForm1.enable();
				this.itemForm2.enable();				
			}
			
			
			this.submiting = false;
    }
  }



  disableInput(){
  	if (this.actionSelected=='reject') {
  		return !this.itemForm3.valid;
  	}else if(this.actionSelected=='verify'){
  		return !this.itemForm1.valid || !this.itemForm2.valid;
  	}else{
  		return true;
  	}
  }



}
