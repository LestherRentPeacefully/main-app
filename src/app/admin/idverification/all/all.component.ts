import { Component, OnInit } from '@angular/core';
import {RequestsService} from '../../admin.service';
import {SharedService} from '../../../shared/shared.service';
import {ToastService} from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {
	public loadingRequests;
	public requests;
	private toastOptions = this.sharedService.toastOptions;

  constructor(private requestsService:RequestsService,
  						private sharedService:SharedService,
  						private toast: ToastService) { }

  async ngOnInit() {
  	this.loadingRequests = true;
  	try{
  		this.requests = await this.requestsService.getIdVerificationRequests();
  	}catch(err){
  		console.log(err.message || err);
  		this.toast.error('Unexpected error loading Requests','', this.toastOptions);
  	}
  	this.loadingRequests = false;


  }

  public formatDateSring(date:number){
  	let formatedDate = this.sharedService.formatDate(date);
  	return `${formatedDate.month} ${formatedDate.dayOfMonth}, ${formatedDate.year}`;
  }


}
