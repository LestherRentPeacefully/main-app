/* Angular 2 modules */

import { NgModule } from '@angular/core';


/*Routes*/

import { IDVerificationRoutingModule } from './idverification-routing.module';


/* Shared modules */

import { SharedModule } from '../../shared/shared.module';

/* Components */

import { IDVerificationComponent } from './idverification.component';
import { AllComponent } from './all/all.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  imports: [
    SharedModule,
    IDVerificationRoutingModule
  ],
  declarations: [IDVerificationComponent, AllComponent, DetailsComponent]
})
export class IDVerificationModule { }
