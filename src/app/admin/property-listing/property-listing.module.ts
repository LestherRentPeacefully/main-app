/* Angular 2 modules */

import { NgModule } from '@angular/core';


/* Google maps module */

import { AgmCoreModule } from '@agm/core';

import {environment} from '../../../environments/environment';

/*Routes*/

import { PropertyListingRoutingModule } from './property-listing-routing.module';



/* Shared modules */

import { SharedModule } from '../../shared/shared.module';


/* Components */

import { PropertyListingComponent } from './property-listing.component';
import { AllComponent } from './all/all.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  imports: [
    SharedModule,
    AgmCoreModule.forRoot({apiKey: environment.googleMaps.apiKey, libraries: ["places","geometry"]}),
    PropertyListingRoutingModule
  ],
  declarations: [PropertyListingComponent, AllComponent, DetailsComponent]
})
export class PropertyListingModule { }
