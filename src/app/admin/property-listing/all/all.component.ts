import { Component, OnInit } from '@angular/core';
import {RequestsService} from '../../admin.service';
import {ToastService} from 'ng-uikit-pro-standard';
import {SharedService} from '../../../shared/shared.service';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {
  private toastOptions = this.sharedService.toastOptions;
  public listings;

  constructor(private requestsService:RequestsService,
              private toast: ToastService,
              public sharedService:SharedService) { }

  async ngOnInit() {
    try{
      this.listings = await this.requestsService.getListings();
    }catch(err){
      console.error(err.message || err);
      this.toast.error('Unexpected error loading properties. Try again later','', this.toastOptions);
    }
  }

  public formatDateSring(date:number){
  	let formatedDate = this.sharedService.formatDate(date);
  	return `${formatedDate.month} ${formatedDate.dayOfMonth}, ${formatedDate.year}`;
  }

}
