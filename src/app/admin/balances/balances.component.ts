import { Component, OnInit, ViewChild } from '@angular/core';
import {RequestsService} from '../admin.service';
import { TotalUsersBalance } from 'src/app/shared/interfaces';
import { AuthService } from 'src/app/auth.service';
import { filter } from 'rxjs/operators';
import { Validators, FormBuilder } from '@angular/forms';
import { ModalDirective, ToastService } from 'ng-uikit-pro-standard';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-balances',
  templateUrl: './balances.component.html',
  styleUrls: ['./balances.component.css']
})
export class BalancesComponent implements OnInit {

  public state = 0;
  public withdrawalFee;

  public submitting: any = {};

  public totalUsersBalances: TotalUsersBalance[];

  private toastOptions = this.sharedService.toastOptions;

  @ViewChild('withdrawalModal') withdrawalModal: ModalDirective;

  public withdrawalForm = this.fb.group({
    // code: ['', [Validators.required]],
    currency: ['', [Validators.required]],
    to: ['', [Validators.required]],
    amount: [''],
    gasLimit: ['', [Validators.min(21000) ]],
    gasPriceInGWei: ['', [Validators.min(1) ]]
  });

  constructor(
    private requestsService: RequestsService,
    private fb: FormBuilder,
    private toast: ToastService,
    private sharedService:SharedService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.userInfo.pipe(filter(userInfo => !!userInfo)).subscribe(async userInfo => {
      let response: {success: boolean, data: {symbol: string, amount: string}[], error: {message: string}};

      [this.totalUsersBalances, response] = await Promise.all([
        this.requestsService.getTotalUsersBalance(),
        this.requestsService.getPlaformBalances()
      ]);
      
      this.totalUsersBalances.forEach(totalUsersBalance => {
        totalUsersBalance['platformBalance'] = response.data.find(platformBalance => platformBalance.symbol === totalUsersBalance.symbol).amount;
      });
    });

  }


  async openWithdrawalModal(currency: string){
    this.state = 0;
    this.withdrawalForm.reset();
    for(let i = 0; i <= this.state; i++){
      this.submitting[i.toString()] = false;
    }

    this.withdrawalModal.show();
    this.withdrawalForm.get('currency').setValue(currency);
    this.withdrawalFee = null;

  }

  async next(){

    const req = this.withdrawalForm.value;
    this.submitting[this.state.toString()] = true;

    if(this.state === 0){

      if(this.withdrawalForm.get('to').invalid) {
        this.toast.error('', 'Enter an address', this.toastOptions);
        this.submitting[this.state.toString()] = false;
        return;
      }

      this.withdrawalForm.disable();
      
      const response = await this.requestsService.estimatePlaformWithdrawalFee({currency:req.currency, to: req.to, amount: req.amount});
      if (response.success) {
        this.state++;
        this.withdrawalFee = response.data;
      } else {
        console.error(response.error.message);
        this.toast.error('', response.error.message, this.toastOptions);
      }


    } else if(this.state === 1) {

    }


  }

}
