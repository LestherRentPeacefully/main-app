import { NgModule } from '@angular/core';

import { BalancesRoutingModule } from './balances-routing.module';

import { BalancesComponent } from './balances.component';

import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [BalancesComponent],
  imports: [
    SharedModule,
    BalancesRoutingModule
  ]
})
export class BalancesModule { }
