import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

/*
Components
*/
import {UserComponent} from './user.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SettingsComponent} from './settings/settings.component';
import {ProfileComponent} from './profile/profile.component';


const appRoutes:Routes = [
	{
		path:'',
		component:UserComponent,
		children:[
			{
				path:'dashboard',
				component:DashboardComponent
			},{
				path:'settings',
				component:SettingsComponent
			},{
				path:':uid',
				component:ProfileComponent
			}
		]
	}
]



@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class userRoutingModule {}