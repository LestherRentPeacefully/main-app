import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { environment } from '../../../environments/environment';
import * as firebase from 'firebase/app';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import {SharedService} from '../../shared/shared.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private APIURL = environment.APIURL;

  constructor(private storage: AngularFireStorage,
              private http: HttpClient,
              private sharedService:SharedService,
              public afs: AngularFirestore) { }

  public getProfileInfo(uid:string){
  	return new Promise(async (resolve, reject)=>{
  		try{
        const db = firebase.firestore();
				let snapshot = await db.collection(`users/${uid}/profile`).get();
				let result:any = {};
				snapshot.forEach(doc=>	result[doc.id] = doc.data());
				resolve(result);

  		}catch(err){
  			reject(err.message);
  		}
  	});
  }

  public saveProfileInfo(obj:any, uid:string){
    const db = firebase.firestore();
    return db.doc(`users/${uid}/profile/editable`).update(obj);
  }

  public getFilePath(uid:string, fileName:string){
    return `users/${uid}/profile/images/${fileName}`;
  }

  public getImageURL(uid:string,fileName:string){
    let fileRef = this.storage.storage.ref(this.getFilePath(uid, fileName));
    return fileRef.getDownloadURL();
  }


  public uploadPhoto(uid:string, file:File, fileName:string){
    let fileRef = this.storage.ref(this.getFilePath(uid, fileName));
    let task = fileRef.put(file);
    return task.snapshotChanges().pipe(map(snapshot=>(snapshot.bytesTransferred / snapshot.totalBytes) * 100));
  }


  async uploadProfilePicture(obj:{photo:string, oldFiles:string[]}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/profile/uploadProfilePicture`, obj, _httpOptions).toPromise();
  }

  async addAreaServing(obj:{role:string, name:string, lat:number, lng:number, geohash:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/profile/addAreaServing`, obj, _httpOptions).toPromise();
  }

  async deleteAreaServing(obj:{id:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/profile/deleteAreaServing`, obj, _httpOptions).toPromise();
  }

  public getAreasServing(uid:string, role:string):Promise<any[]>{
    return new Promise(async (resolve, reject)=>{
  		try{
        const db = firebase.firestore();
				let snapshot = await db.collection(`users/${uid}/areasServing`).where('role','==',role).get();
				let result = [];
				snapshot.forEach(doc=>result.push(doc.data()));
				resolve(result);

  		}catch(err){
        console.log(err)
  			reject(err.message);
  		}
  	});
  }

  async editSkills(obj:{role:string, skills:any}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/profile/editSkills`, obj, _httpOptions).toPromise();
  }

  async getReviews(uid:string, role:string){
    const db = firebase.firestore();
    let reviews = [];
    let arrayOfPromises:Promise<any>[] = [];
    let reviewsSnapshot = await db.collection(`users/${uid}/reviews`).where('role','==',role).orderBy('creationTS','desc').get();
    
    if (!reviewsSnapshot.empty) {
      for(let reviewDoc of reviewsSnapshot.docs){
        reviews.push(reviewDoc.data());
        arrayOfPromises.push(db.doc(`users/${reviewDoc.data().providedBy.uid}`).get());
      }
      let usersSnapshot = await Promise.all(arrayOfPromises);
      

      reviews = reviews.map(review=>{
        let userInfo = (usersSnapshot.find(item=>item.data().uid==review.providedBy.uid)).data();
        review.user = {
          firstName : userInfo.firstName,
          lastName : userInfo.lastName,
          email : userInfo.email,
          photo : userInfo.photo
        };
        return review;
      });
    
    }

    return reviews;
  }

  async hireMe(obj:{role:string, serviceProviderID:string, name:string, details:string, price:string, skillsRequired:any}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/profile/hireMe`, obj, _httpOptions).toPromise();
  }


  public getUser(uid:string){
    return this.afs.doc(`users/${uid}`).valueChanges();
  }

  async contactMe(req:{agentId: string, message:string, email:string, name:string}):Promise<any>{
    return this.http.post(`${this.APIURL}/profile/contactMe`, req, httpOptions).toPromise();
  }



}
