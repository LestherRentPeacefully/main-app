import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import {SharedService} from '../../shared/shared.service';
import { FormBuilder,Validators } from '@angular/forms';
import {ToastService, ModalDirective, TooltipDirective } from 'ng-uikit-pro-standard';
import {AuthService} from '../../auth.service';
import {ProfileService} from './profile.service';
import * as Cropper from 'cropperjs/dist/cropper-rounded';
import { MapsAPILoader } from '@agm/core';
import * as geofirex from 'geofirex';
import * as firebase from 'firebase/app';
import { filter, map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, of } from 'rxjs';
import {MdbAutoCompleterComponent} from 'ng-uikit-pro-standard';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public app = environment.app;

	private toastOptions = this.sharedService.toastOptions;
 	// public userInfo:any = {role:'landlord',firstName:'lesther', lastName:'caballero', uid:'Mijuj20TzOWaHpAkDutAoVVFzRI2'};
  public userInfo:any;
  public authenticated:boolean;
  private userInfoSubscriber:any;
  private _userInfoSubscriber:any;
  private uploadPhotoSubscriber:any;
  private routerSub;
  private authenticatedSub;
  public profileInfo:any;
  public editing = {profile:false, photo:false, shortDescription:false, aboutMe:false, social:false};
  public defaultFields :any;
  public submitting = {profile:false, photo:false, shortDescription:false, aboutMe:false, social:false};
  public profileLoaded;
  public userLoaded:boolean;
  public isMyProfile:boolean;
  public myRole:string;
  public reviews:any[];

  public hideAreaInput = true;
  public areasServing:any[];
  public savingArea;
  public deletingArea;
  
  public serviceProviderSkills = this.sharedService.serviceProviderSkills;
  public savingSkills;

  public file:any = {};
  private oldFiles:string[];
  public cropper;
  public imageToCrop;
  public imageCropped = {localURL:'', uploading:false, uploadFilePercentage:0, filePath:''};
  public uid:string;
  
  public autoCompleteSkillsResults;
  public skillsSelectedFromAutocomplete = [];
  public hiring;
  public skillsAutoCompleterSub;
  public contacting: boolean;

  public itemForm = this.fb.group({
    photo:['', [Validators.required,]],
    shortDescription:['', [Validators.required, Validators.maxLength(140)]],
    aboutMe:['',[Validators.required, Validators.maxLength(2000)]],
    social:this.fb.group({
      website:this.fb.group({
        enabled:[''],
        url:['']
      }),
      facebook:this.fb.group({
        enabled:[''],
        url:['']
      }),
      twitter:this.fb.group({
        enabled:[''],
        url:['']
      }),
      instagram:this.fb.group({
        enabled:[''],
        url:['']
      }),
      linkedin:this.fb.group({
        enabled:[''],
        url:['']
      }),
    })
  });

  public hireMeForm = this.fb.group({
    price:['', [Validators.required, Validators.min(5)]],
    name:['', [Validators.required, Validators.maxLength(80)]],
    details:['Hi, I would like to offer you a job', [Validators.required, Validators.maxLength(1000)]],
    autoCompleteSkillsInput:['', [Validators.required,]],
  });

  public contactMeForm = this.fb.group({
    message: ['', [Validators.required, Validators.maxLength(200) ]],
    email: ['', [Validators.required, Validators.email ]],
    name: ['', [Validators.required ]],
  });


  @ViewChild('cropImageModal') public cropImageModal:ModalDirective;
  @ViewChild('imageToCrop') public imageToCropElementRef: ElementRef;
  @ViewChild('popToolTip') public popToolTip: TooltipDirective;
  @ViewChild('socialModal') public socialModal: ModalDirective;
  @ViewChild('serviceProviderSkillModal') public serviceProviderSkillModal: ModalDirective;
  @ViewChild('hireMeModal') public hireMeModal: ModalDirective;
  @ViewChild('contactMeModal') public contactMeModal: ModalDirective;
  @ViewChild("areasServingSearch") public searchElementRef: ElementRef<HTMLInputElement>;
  @ViewChild("auto") completer: MdbAutoCompleterComponent;
  // @ViewChild(MdbAutoCompleterComponent) completer: MdbAutoCompleterComponent;


  constructor(public sharedService:SharedService,
  						private authService:AuthService,
  						private profileService:ProfileService,
              private toast: ToastService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder) { }



  ngOnInit() {
    // console.log(this.data)
    this.autoCompleteSkillsResults = this.searchEntries('');
    // console.log(this.autoCompleteSkillsResults.value)

    this.hireMeForm.get('autoCompleteSkillsInput').valueChanges.subscribe(value=>{
      this.autoCompleteSkillsResults = this.searchEntries(value || '');
    });


    this.authenticatedSub = this.authService.authenticated.pipe(filter(auth=>auth==true || auth==false)).subscribe(auth=>{

      if(this.routerSub) this.routerSub.unsubscribe();

      this.routerSub = combineLatest(this.route.paramMap, this.route.queryParams).subscribe(([paramMap, queryParams])=>{
      // .pipe(filter(([paramMap, queryParams])=>paramMap && queryParams && paramMap.get('uid') && queryParams['role']))

        if(this.userInfoSubscriber) this.userInfoSubscriber.unsubscribe();
        if(this._userInfoSubscriber) this._userInfoSubscriber.unsubscribe();

        this.uid = paramMap.get('uid');
        let role = queryParams['role'];
        
        this.userInfo = null;
        this.profileInfo = null;
        this.profileLoaded = false;
        this.userLoaded = false;
        this.isMyProfile = false;
        this.reviews = null;
      

        this.authenticated = auth;

        if (this.authenticated) {
          this.userInfoSubscriber = this.authService.userInfo.subscribe(async (userInfo)=>{
            if (userInfo && userInfo.uid==this.uid) {
              this.userInfo = userInfo;
              this.isMyProfile = true;
              if(!this.profileLoaded){
                this.defaultFields = {
                  photo:'./assets/noProfilePicture.png', 
                  shortDescription:`Nice to meet you! I'm ${this.userInfo.firstName.replace(/\b\w/g, l => l.toUpperCase())} ${this.userInfo.lastName.replace(/\b\w/g, l => l.toUpperCase())}.`,
                  aboutMe:`I'm a great RentPeacefully user`
                };
                this.profileLoaded = true;
                if(role) this.userInfo.role = role;
                this.getProfileInfo();
                if(this.userInfo.role=='service provider' || this.userInfo.role=='real estate agent') this.addAreaServing();
              }

            } else if(userInfo && userInfo.uid!=this.uid && !this.userLoaded) {
              this._userInfoSubscriber = this.profileService.getUser(this.uid).subscribe(async (_userInfo)=>{
                this.userInfo = _userInfo;
                this.userLoaded = true;
                this.myRole = userInfo.role;
                if(this.userInfo && !this.profileLoaded){
                  this.defaultFields = {
                    photo:'./assets/noProfilePicture.png', 
                    shortDescription:`Nice to meet you! I'm ${this.userInfo.firstName.replace(/\b\w/g, l => l.toUpperCase())} ${this.userInfo.lastName.replace(/\b\w/g, l => l.toUpperCase())}.`,
                    aboutMe:`I'm a great RentPeacefully user`
                  };
                  this.profileLoaded = true;
                  this.userInfo.role = role;
                  this.getProfileInfo();
                }
              });
            }
          });
          

        } else if(this.authenticated==false) {
          this._userInfoSubscriber = this.profileService.getUser(this.uid).subscribe(async (userInfo)=>{
            this.userInfo = userInfo;
            if(this.userInfo && !this.profileLoaded){
              this.defaultFields = {
                photo:'./assets/noProfilePicture.png', 
                shortDescription:`Nice to meet you! I'm ${this.userInfo.firstName.replace(/\b\w/g, l => l.toUpperCase())} ${this.userInfo.lastName.replace(/\b\w/g, l => l.toUpperCase())}.`,
                aboutMe:`I'm a great RentPeacefully user`
              };
              this.profileLoaded = true;
              this.userInfo.role = role;
              this.getProfileInfo();
            }
          });
        }
      });


    
    });
  }

  ngOnDestroy(){
    if(this.userInfoSubscriber) this.userInfoSubscriber.unsubscribe();
    if(this._userInfoSubscriber) this._userInfoSubscriber.unsubscribe();
    if(this.uploadPhotoSubscriber) this.uploadPhotoSubscriber.unsubscribe();
    if(this.authenticatedSub) this.authenticatedSub.unsubscribe();
    if(this.routerSub) this.routerSub.unsubscribe();
    if(this.skillsAutoCompleterSub) this.skillsAutoCompleterSub.unsubscribe();
  }



  
  private searchEntries(term: string) {
    return of(this.serviceProviderSkills.filter((skill) => skill.name.toLowerCase().includes(term.toLowerCase())));
  }
  


  ngAfterViewInit() {
    this.skillsAutoCompleterSub = this.completer.selectedItemChanged().subscribe((data: {text:{id:string, name:string}}) => {
      let skill = data.text;
      if(!this.skillsSelectedFromAutocomplete.find(item=>item.id==skill.id)){
        this.skillsSelectedFromAutocomplete.push(data.text);
      }
      this.hireMeForm.get('autoCompleteSkillsInput').setValue('');
      this.autoCompleteSkillsResults = this.searchEntries('');
    });
   }


   deleteSkillSelectedFromAutocomplete(skill:{id:string, name:string}){
    let index = this.skillsSelectedFromAutocomplete.findIndex(item => item.id == skill.id);
    this.skillsSelectedFromAutocomplete.splice(index, 1);
   }




  


  public async getProfileInfo(){
  	try{

      
      if(this.userInfo.role === 'service provider' || this.userInfo.role === 'real estate agent'){

        [this.profileInfo, this.areasServing] = await Promise.all([
          this.profileService.getProfileInfo(this.userInfo.uid), 
          this.profileService.getAreasServing(this.userInfo.uid, this.userInfo.role)
        ]);
        
        if(this.userInfo.role === 'service provider') {
          this.profileInfo.serviceProvider = this.profileInfo.serviceProvider || {};
          this.profileInfo.serviceProvider.skills = this.profileInfo.serviceProvider.skills || {};
          this.serviceProviderSkills = this.serviceProviderSkills.map(skill=>{
            if(this.profileInfo.serviceProvider.skills[skill.id]) (<any>skill).selected = true;
            return skill;
          });
          this.profileInfo.serviceProvider.skills = this.selectedServiceProviderSkills;
        }
          
      }else{
        this.profileInfo = await this.profileService.getProfileInfo(this.userInfo.uid);
      }
          
      this.itemForm.get('shortDescription').setValue(this.profileInfo.editable.shortDescription || this.defaultFields.shortDescription);
      this.itemForm.get('aboutMe').setValue(this.profileInfo.editable.aboutMe || this.defaultFields.aboutMe);
      this.itemForm.get('social').patchValue(this.profileInfo.editable.social);
        
      if(this.userInfo){

        if ((this.userInfo.role=='tenant' && this.userInfo.tenant.reviews==0) || 
            (this.userInfo.role=='landlord' && this.userInfo.landlord.reviews==0) || 
            (this.userInfo.role=='service provider' && this.userInfo.serviceProvider.reviews==0) || 
            (this.userInfo.role=='real estate agent' && this.userInfo.realEstateAgent.reviews==0)) {
          this.reviews = [];
        } else {
          this.reviews = await this.profileService.getReviews(this.userInfo.uid, this.userInfo.role);
        }
      }



    }catch(err){
  		console.error(err.message || err);
  		this.toast.error('Unexpected error getting profile info. Try again later','', this.toastOptions);
  	}
  }

  public get memberSince() : string {
    let formatDate = this.sharedService.formatDate(this.userInfo.creationTS);
    return `${formatDate.month} ${formatDate.dayOfMonth}, ${formatDate.year}`;
  }


  editProfile(){
    this.editing.profile=!this.editing.profile;
    this.editing.shortDescription = false;
    this.editing.aboutMe = false;
    this.editing.photo = false;
    this.hideAreaInput = true;
    if(this.editing.profile) this.popToolTip.show(); else this.popToolTip.hide();
  }

  editField(field:string){
    if(this.editing.profile){
      this.editing[field] = true;
    }
  }

  cancelFieldEdition(field:string){
    if(this.editing.profile){
      this.editing[field] = false;
      this.itemForm.get(field).setValue(this.profileInfo.editable[field]);
    }
  }


  async saveFieldEdition(field:string){
    let obj = {
      [field] : this.itemForm.get(field).value
    };
    this.submitting[field] = true;
    this.itemForm.get(field).disable();

    try{
      await this.profileService.saveProfileInfo(obj, this.userInfo.uid);
      this.profileInfo.editable[field] = obj[field];
      this.editing[field] = false;
      if(field=='social') this.socialModal.hide();
      this.toast.success('Profile updated successfully','',this.toastOptions);
    }catch(err){
      console.error(err.message);
      this.toast.error('','Unexpected error updating profile',this.toastOptions);
    }
    this.submitting[field] = false;
    this.itemForm.get(field).enable();
  }


  editProfilePicture(e){

    this.imageToCrop = this.imageToCropElementRef.nativeElement;
    let files = e.target.files as File[];

    if (files && files.length > 0) {

      let file = files[0];
      if (!file.type.match(/image-*/)) {
        this.toast.error('','Invalid format',this.toastOptions);
      }else{
          this.file.type = file.type;

          let reader = new FileReader();

          reader.onloadend = (e) => {
            let url = reader.result;
            this.imageToCrop.src = url;
            this.cropImageModal.show();
          };
          reader.readAsDataURL(file);
      }
    }

    e.target.value = '';
    
  }


  cropImageModalOnShown(){
    this.cropper = new Cropper(this.imageToCrop, {
      aspectRatio: 1
    });         
  }

  cropImageModalOnHidden(){
    this.cropper.destroy();
    this.cropper = null;
  }


  uploadProfilePicture(){
    let canvas;

    if(this.cropper){
      canvas = this.cropper.getCroppedCanvas({
      width: 214,
      height: 214,
      });

      canvas.toBlob( file => {

        this.cropImageModal.hide();

        this.imageCropped.localURL = canvas.toDataURL(this.file.type);

        let fileName = this.sharedService.newFileName(file.type);

        this.uploadPhotoSubscriber = this.profileService.uploadPhoto(this.userInfo.uid, file, fileName).subscribe((uploadFilePercent)=>{

          this.imageCropped.uploading = true;
          this.imageCropped.uploadFilePercentage = uploadFilePercent;

        }, (err)=>{
          
          console.error(err.message || err);
          this.toast.error('','Error uploading photo. Try again later', this.toastOptions);
          this.imageCropped.uploading = false;

        }, async ()=>{

          try{
            let getImageURL = await this.profileService.getImageURL(this.userInfo.uid,fileName);
            this.itemForm.get('photo').setValue(getImageURL);
            let photo = getImageURL;
            this.oldFiles = this.userInfo.photo? [this.sharedService.photoPathFromURL(this.userInfo.photo)] : null;
            let response = await this.profileService.uploadProfilePicture({photo:photo,oldFiles:this.oldFiles});
            if (response.success) { 
              this.imageCropped.filePath = this.profileService.getFilePath(this.userInfo.uid,fileName);
              this.userInfo.photo = photo;
              this.toast.success('Profile updated successfully','',this.toastOptions);
            } else {
              console.error(response.error.message);
              this.toast.error('', response.error.message, this.toastOptions);
            }
          }catch(err){
            console.error(err.message || err);
            this.toast.error('','Error uploading photo. Try again later', this.toastOptions);
          }
          this.uploadPhotoSubscriber.unsubscribe();
          this.imageCropped.uploading = false;

        });



      }, this.file.type);
    }
  }


  socialModalOnShown(){
    this.popToolTip.hide();
  }

  socialModalOnHidden(){
    this.popToolTip.show();
  }

  url(unformatedUrl:string){
    return (unformatedUrl.indexOf('https://')==0 || unformatedUrl.indexOf('http://')==0)? unformatedUrl:`https://${unformatedUrl}`;
  }

  openServiceProviderSkillModal(){
    this.popToolTip.hide();
    this.serviceProviderSkillModal.show();
  }

  selectServiceProviderSkill(skill:{name:string, id:string}){
    let index = this.serviceProviderSkills.findIndex(item => item.id == skill.id);
    (<any>this.serviceProviderSkills[index]).selected = !(<any>this.serviceProviderSkills[index]).selected;    
  }

  public get selectedServiceProviderSkills() {
    return this.serviceProviderSkills.filter(skill=>(<any>skill).selected);
  }


  async editSkills(){
    this.savingSkills = true;
    try {
      let skills = {};
      for(let skill of this.serviceProviderSkills){
        if((<any>skill).selected) skills[skill.id] = true;
      }

      const response = await this.profileService.editSkills({role:this.userInfo.role, skills});
      if (response.success) {
        this.profileInfo.serviceProvider.skills = this.selectedServiceProviderSkills;
        this.serviceProviderSkillModal.hide();
        
      } else {
        console.error(response.error.message);
        this.toast.error('', response.error.message, this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('','Error Modifying Skills. Try again later', this.toastOptions);
    }
    this.savingSkills = false;
  }


  async serviceProviderSkillModalClosed(){
    this.popToolTip.show();
    this.serviceProviderSkills = this.serviceProviderSkills.map(skill=>{
      if(this.profileInfo.serviceProvider.skills.find(_skill=>_skill.id==skill.id)) (<any>skill).selected = true;
      else (<any>skill).selected = false;
      return skill;
    });
  }



  private async addAreaServing(){
    await this.mapsAPILoader.load();

    let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      types: ["geocode"]
    });

    autocomplete.addListener("place_changed", () => {
      
      this.ngZone.run(async () => {

        let place: google.maps.places.PlaceResult = autocomplete.getPlace();

        if (place.geometry) {

          const geo = geofirex.init(<any>firebase);

          try {
            const lat = place.geometry.location.lat(); const lng = place.geometry.location.lng();

            this.savingArea = true;
            this.searchElementRef.nativeElement.disabled = true;

            const response = await this.profileService.addAreaServing({
              name:this.searchElementRef.nativeElement.value,
              role:this.userInfo.role,
              lat,
              lng,
              geohash: geo.point(lat, lng).hash
            });
  
            if (response.success) {
              this.areasServing.push({
                name:this.searchElementRef.nativeElement.value,
                role:this.userInfo.role,
                geohash: geo.point(lat, lng).hash,
                geopoint:new firebase.firestore.GeoPoint(lat, lng),
                id:response.data,
                creationTS:new Date().getTime()
              });
              
            } else {
              console.error(response.error.message);
              this.toast.error('', response.error.message, this.toastOptions);
            }
            
          } catch (err) {
            console.error(err.message || err);
            this.toast.error('','Error adding Area. Try again later', this.toastOptions);
          }

          this.savingArea = false;
          this.searchElementRef.nativeElement.value='';
          this.searchElementRef.nativeElement.disabled = false;

        }

      });
    });
  }

  async deleteAreaServing(area:any){
    if(this.editing.profile && !this.hideAreaInput && !this.savingArea && !this.deletingArea){

      this.deletingArea = true;
      this.searchElementRef.nativeElement.disabled = true;

      try {
        const response = await this.profileService.deleteAreaServing({id:area.id});
        if (response.success) {
          let index = this.areasServing.findIndex(item => item.id == area.id);
          this.areasServing.splice(index, 1);
        } else {
          console.error(response.error.message);
          this.toast.error('', response.error.message, this.toastOptions);
        }
      } catch (err) {
        console.error(err.message || err);
        this.toast.error('','Error deleting Area. Try again later', this.toastOptions);
      }

      this.deletingArea = false;
      this.searchElementRef.nativeElement.disabled = false;
    }
  }


  viewProfile(uid:string, role:string){
    this.router.navigate(['/user', uid], { queryParams: {role: role}});
  }


  openHireMeModal(){
    if(!this.authenticated) {
      this.toast.info('','Please log in first.',this.toastOptions);
      this.authService.redirectUrl = this.router.url;
      this.router.navigate(['/account/login']);

    } else if(this.myRole!='tenant' && this.myRole!='landlord') {
      this.toast.info('',"You must either have the Tenant or Lanlord role set",this.toastOptions);
    } else {
      this.hireMeModal.show();
    }
  }

  hireMeModalClosed(){
    this.hireMeForm.reset();
    this.skillsSelectedFromAutocomplete = [];
  }

  async hireMe(){
    if(!this.hireMeForm.get('price').valid){
      this.toast.error('', 'Amount must be greater than or equal to 5 USD', this.toastOptions);
    } else if(!this.hireMeForm.get('name').valid){
      this.toast.error('', 'Job name required and must have max 80 characters!', this.toastOptions);
    } else if(!this.hireMeForm.get('details').valid){
      this.toast.error('', 'Job description required and must have max 1000 characters!', this.toastOptions);
    } else if(!(this.skillsSelectedFromAutocomplete.length>0)){
      this.toast.error('', 'Please, selected at least 1 Skill', this.toastOptions);
    } else{

      this.hiring = true;
      let req = this.hireMeForm.value;
      req.skillsRequired = {};
      for(let skill of this.skillsSelectedFromAutocomplete){
        req.skillsRequired[skill.id] = true;
      }

      this.hireMeForm.disable();

      try {
        const response = await this.profileService.hireMe({
          role:this.myRole,
          serviceProviderID:this.userInfo.uid,
          name:req.name,
          details:req.details,
          price:req.price,
          skillsRequired:req.skillsRequired
        });

        if (response.success) {
          this.hireMeModal.hide();
          this.toast.success('', 'Service Provider Hired!', this.toastOptions);
          this.router.navigate(['/my-service-providers-jobs', response.data]);
        } else {
          console.error(response.error.message);
          this.toast.error('', response.error.message, this.toastOptions);
        }

      } catch (err) {
        console.error(err.message || err);
        this.toast.error('','Error Hiring. Try again later', this.toastOptions);
      }
      this.hiring = false;
      this.hireMeForm.enable();
    }


  }


  
  openContactMeModal() {
    this.contactMeModal.show();
  }

  async contactMe() {
    const req = this.contactMeForm.value;

    this.contacting = true;
    this.contactMeForm.disable();

    try {
      const response = await this.profileService.contactMe({
        agentId: this.userInfo.uid,
        message: req.message,
        email: req.email,
        name: req.name
      });

      if(response.success) {
        this.toast.success('Email sent to the Agent','', this.toastOptions);
        this.contactMeModal.hide();
      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'',this.toastOptions);
      }

    } catch (err) {
      console.error(err.message || err);
        this.toast.error('','Error Contacting. Try again later', this.toastOptions);
    }
    this.contacting = false;
    this.contactMeForm.enable();
  }

  get profileLink() {
    let value = `https://rentpeacefully.com/user/${this.userInfo.uid}?role=`;

    if(this.userInfo.role == 'tenant') value+='tenant';
    if(this.userInfo.role == 'landlord') value+='landlord';
    if(this.userInfo.role == 'service provider') value+='service%20provider';
    if(this.userInfo.role == 'real estate agent') value+='real%20estate%20agent';

    return value;
  }


  copyToClipboard() {
    this.sharedService.copyToClipboard(this.profileLink);
    this.toast.info(null, 'Link copied', this.toastOptions);
  }


  



}
