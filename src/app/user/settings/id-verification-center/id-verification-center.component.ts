import { Component, OnInit,EventEmitter } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';
import { combineLatest } from 'rxjs';
import {ToastService  } from 'ng-uikit-pro-standard';
import {SharedService} from '../../../shared/shared.service';
import {SettingsService} from '../settings.service';
import {AuthService} from '../../../auth.service';

@Component({
  selector: 'app-id-verification-center',
  templateUrl: './id-verification-center.component.html',
  styleUrls: ['./id-verification-center.component.css']
})
export class IdVerificationCenterComponent implements OnInit {

	public daysOfTheMonthOptions = this.sharedService.daysOfTheMonthOptions;
	public monthsOptions = this.sharedService.monthsOptions;
	public birthYearOptions = this.settingsService.birthYearOptions.sort((a,b)=>{
		return Number(b.value) - Number(a.value);
	});
	public expirationYearOptions = this.settingsService.expirationYearOptions;
  public IDTypeOptions = this.settingsService.IDTypeOptions;
  private toastOptions = this.sharedService.toastOptions;
  public countriesOptions = this.sharedService.countriesOptions;
  public roleOptions = this.sharedService.roleOptions;
  public dateSubscriber:any;
  public page = 1;
  public submiting;
  public loadingVerificationFields;
  public rejectionReason:string;
  public requestStatus:string;
  public userInfo:any;
  public files:any = { ID:{}, selfie:{} };
  public userInfoSubscriber;

  public itemForm1 = this.fb.group({
    firstName: ['',[Validators.required]],
    lastName: ['',[Validators.required]],
    brokerage: [''],
    licenceNo: [''],
    dateOfBirth: this.fb.group({
	    month: ['',[Validators.required]],
	    day: ['',[Validators.required]],
	    year: ['',[Validators.required]]
  	}),
  });

  public itemForm2 = this.fb.group({
    IDFile: ['',[Validators.required]],
    country: ['',[Validators.required]],
    IDType: ['',[Validators.required]],
    IDExpirationDate: this.fb.group({
      month: ['',[Validators.required]],
      day: ['',[Validators.required]],
      year: ['',[Validators.required]]
    }),
  });

  public itemForm3 = this.fb.group({
    selfieFile: ['',[Validators.required]],
  });

  constructor(private fb: FormBuilder,
  						private sharedService:SharedService,
              private settingsService:SettingsService,
              private authService:AuthService,
  					  private toast: ToastService) {


  }

  async ngOnInit() {

    this.userInfoSubscriber = this.authService.userInfo.subscribe(async (userInfo)=>{
      this.userInfo = userInfo;
      if(this.userInfo){

        if (this.userInfo.role=='real estate agent') {
          this.itemForm1.get('brokerage').setValidators(Validators.required);
          this.itemForm1.get('licenceNo').setValidators(Validators.required);

        }else{
          this.itemForm1.get('brokerage').clearValidators();
          this.itemForm1.get('licenceNo').clearValidators();
        }

        this.itemForm1.get('brokerage').updateValueAndValidity();
        this.itemForm1.get('licenceNo').updateValueAndValidity();


        if( (this.userInfo.role=='real estate agent' && !this.userInfo.status.realEstateAgentIDVerified) || 
             (this.userInfo.role!='real estate agent' && !this.userInfo.status.IDVerified) ) {   // ID not verified yet


          try{
            this.loadingVerificationFields = true;
            let requestSnapshot = await this.settingsService.getVerificationRequestFields(this.userInfo.uid, this.userInfo.role);
            this.loadingVerificationFields = false;

            this.itemForm1.patchValue({
              firstName:this.userInfo.firstName.replace(/\b\w/g, l => l.toUpperCase()),
              lastName:this.userInfo.lastName.replace(/\b\w/g, l => l.toUpperCase()),
            });
            
            if(this.userInfo.role=='real estate agent'){
              this.itemForm1.patchValue({
                brokerage:userInfo.realEstateAgent? userInfo.realEstateAgent.brokerage:null,
                licenceNo:userInfo.realEstateAgent? userInfo.realEstateAgent.licenceNo:null
              });
            }            

            this.page = 1;
            this.requestStatus = null;
            this.rejectionReason = null;


            if (requestSnapshot.exists) {   //Request already submitted
              let requestFields = requestSnapshot.data();
              this.requestStatus = requestFields.status;

              if (requestFields.status=='pending') {  //Request pending to approve
                this.page = 4;

              } else if(requestFields.status=='rejected'){ //Request rejected
                this.page = 1;
                this.rejectionReason = requestFields.rejectionReason;

                requestFields.country = this.sharedService.countriesOptions.find((element)=> element.label.toLowerCase()==requestFields.country.toLowerCase()).value;
                requestFields.IDType = this.settingsService.IDTypeOptions.find((element)=> element.label.toLowerCase()==requestFields.IDType.toLowerCase()).value;
                
                this.itemForm1.setValue({
                  firstName:requestFields.firstName.replace(/\b\w/g, l => l.toUpperCase()),
                  lastName:requestFields.lastName.replace(/\b\w/g, l => l.toUpperCase()),
                  brokerage:requestFields.brokerage || null,
                  licenceNo:requestFields.licenceNo || null,
                  dateOfBirth:requestFields.dateOfBirth
                });
                this.itemForm2.setValue({
                  IDFile:requestFields.IDFile,
                  country:requestFields.country,
                  IDType:requestFields.IDType,
                  IDExpirationDate:requestFields.IDExpirationDate
                });
                this.itemForm3.setValue({
                  selfieFile:requestFields.selfieFile
                });

               }
             }

          }catch(err){
            this.loadingVerificationFields = false;
            console.error(err.message || err);
            this.toast.error('','Error loading info, please try again',this.toastOptions);
          }

        }else{ //ID already verified
          this.page = 5;
        }

      }
    });




    this.dateSubscriber = this.itemForm1.get(`dateOfBirth.month`).valueChanges.subscribe((selectedMonth)=>{

      if (selectedMonth=='1' || selectedMonth=='3' || 
          selectedMonth=='5' || selectedMonth=='7' || 
          selectedMonth=='8' || selectedMonth=='10' || 
          selectedMonth=='12') { 
        this.daysOfTheMonthOptions = this.sharedService.daysOfTheMonthOptions;

      }else if(selectedMonth=='2'){
        this.daysOfTheMonthOptions = this.sharedService.daysOfTheMonthOptions.slice(0,28);
      }else{
        this.daysOfTheMonthOptions = this.sharedService.daysOfTheMonthOptions.slice(0,30);
      }
    });


  }

  ngOnDestroy(){
    this.userInfoSubscriber.unsubscribe();
    this.dateSubscriber.unsubscribe();
  }





  onUploadOutput(e: any) {
  	let file = e.target.files[0] as File;
    // console.log(file)
  	if (!file.type.match(/image-*/)) {
      this.toast.error('','Invalid format',this.toastOptions);
            
    }else{
      
      let fileName = this.settingsService.newFileName(file.type); 

      if(this.page==2){

        this.files.ID = { file:file, name:fileName, tempName:file.name };
        this.files.ID.uploaded = false;
        this.files.ID.errorUploading = false;

        this.settingsService.uploadPhoto(this.userInfo.uid, this.files.ID.file, this.files.ID.name).subscribe(uploadIDFilePercent=>{
          this.files.ID.uploading = true;
          this.files.ID.uploadIDFilePercent = uploadIDFilePercent;

        }, (err)=>{
          this.files.ID.uploading = false;
          this.files.ID.errorUploading = true;
          console.error(err.message);
          this.toast.error('','Error uploading file. Try again later', this.toastOptions);

        }, async ()=>{
          try{
            let getImageURL = await this.settingsService.getImageURL(this.userInfo.uid, this.files.ID.name);
            this.itemForm2.get('IDFile').setValue(getImageURL);
            this.files.ID.uploading = false;
            this.files.ID.uploaded = true;

          }catch(err){
            this.files.ID.uploading = false;
            this.files.ID.errorUploading = true;
            console.error(err.message || err);
            this.toast.error('','Error uploading file. Try again later', this.toastOptions);
          }
        });




    	} else if(this.page==3){

        this.files.selfie = { file:file, name:fileName, tempName:file.name };
        this.files.selfie.uploaded = false;
        this.files.selfie.errorUploading = false;

        this.settingsService.uploadPhoto(this.userInfo.uid, this.files.selfie.file, this.files.selfie.name).subscribe(uploadSelfieFilePercent=>{
          this.files.selfie.uploading = true;
          this.files.selfie.uploadIDFilePercent = uploadSelfieFilePercent;

        }, (err)=>{
          this.files.selfie.uploading = false;
          this.files.selfie.errorUploading = true;
          console.error(err.message);
          this.toast.error('','Error uploading file. Try again later', this.toastOptions);

        }, async ()=>{
          try{
            let getImageURL = await this.settingsService.getImageURL(this.userInfo.uid, this.files.selfie.name);
            this.itemForm3.get('selfieFile').setValue(getImageURL);
            this.files.selfie.uploading = false;
            this.files.selfie.uploaded = true;

          }catch(err){
            this.files.selfie.uploading = false;
            this.files.selfie.errorUploading = true;
            console.error(err.message || err);
            this.toast.error('','Error uploading file. Try again later', this.toastOptions);            
          }
        });

    	}

    }

  }

  async onSubmit(){

    if(this.page!=3) {
      this.page++;

    }else{

      this.submiting = true;

      let obj = {...this.itemForm1.value, ...this.itemForm2.value, ...this.itemForm3.value};

      this.itemForm1.disable();
      this.itemForm2.disable();
      this.itemForm3.disable();

      try{

        await this.settingsService.updloadIDVerificationRequestField(this.userInfo.uid, this.userInfo.email, obj, this.userInfo.role);

        this.submiting = false;
        this.page = 4;
        this.requestStatus = 'pending';

      } catch(err){
        this.submiting = false;
        console.error(err.message);
        this.toast.error('','Error submitting request. Try again later',this.toastOptions);

        this.itemForm1.enable();
        this.itemForm2.enable();
        this.itemForm3.enable();
      }

    }

  }


  public changePage(page:number){
    this.page = page;
  }

  nextPage(){
    this.page++;
  }

  previewsPage(){
    this.page--;
  }



}
