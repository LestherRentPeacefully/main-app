import { Component, OnInit, ViewChild } from '@angular/core';
import {SharedService} from '../../shared/shared.service';
import { Validators, FormBuilder } from '@angular/forms';
import {AuthService} from '../../auth.service';
import {SettingsService} from './settings.service';
import { ActivatedRoute } from '@angular/router';
import {TabsetComponent} from 'ng-uikit-pro-standard';
import { filter } from 'rxjs/operators';
import { ToastService } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

	private toastOptions = this.sharedService.toastOptions;
	public roleOptions = this.sharedService.roleOptions;
	private userInfoSubscriber;
	private roleSubscriber;
	public userInfo;
	public sub:any;
	public selectedPanel = 1;
	public switchingRole;

  public itemForm = this.fb.group({
    role: ['',[ Validators.required ]],
	});
	
	@ViewChild('staticTabs') staticTabs:TabsetComponent;

  constructor(private sharedService:SharedService,
							private authService:AuthService,
							private toast: ToastService,
							private settingsService:SettingsService,
							private route: ActivatedRoute,
  					  private fb: FormBuilder) { }

  ngOnInit() {

  	this.userInfoSubscriber = this.authService.userInfo.pipe(filter(userInfo=>userInfo)).subscribe((userInfo)=>{
			this.userInfo = userInfo;
			let value = this.roleOptions.find(item=>item.role==userInfo.role).value;
			if(!this.itemForm.get('role').value || userInfo.role!=this.role) this.itemForm.get('role').setValue(value);
  	});

  	this.roleSubscriber = this.itemForm.get('role').valueChanges.pipe(filter(value=>this.userInfo.role!=this.role)).subscribe(async (value)=>{
			// this.toast.success('', 'Role switched', { positionClass:'toast-bottom-center', toastClass: 'opacity' });
			this.switchingRole = true;
			this.userInfo.role = this.role;
			this.authService.userInfo.next(this.userInfo);
			this.itemForm.get('role').disable();
			await this.settingsService.setRole(this.userInfo.uid, this.role);
			this.toast.success('', 'Role switched', this.toastOptions);
			this.switchingRole = false;
			this.itemForm.get('role').enable();
		});
		
	}
	
	ngAfterViewInit(){
		this.sub = this.route.queryParams.subscribe(params => {
			setTimeout(()=>{
				if (params['panel']) {
					this.selectedPanel = +params['panel'];
					this.staticTabs.setActiveTab(+params['panel']);
				};
			});
		});
	}


  ngOnDestroy(){
  	this.roleSubscriber.unsubscribe();
		this.userInfoSubscriber.unsubscribe();
		this.sub.unsubscribe();
  }


  get role(){
    return this.roleOptions.find(item=>item.value==this.itemForm.get('role').value).role;
	}




}
