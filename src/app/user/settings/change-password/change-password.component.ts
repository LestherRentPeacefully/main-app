import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { FormBuilder,Validators, } from '@angular/forms';
import {ToastService  } from 'ng-uikit-pro-standard';
import {SettingsService} from '../settings.service';
import {ValidationsService} from '../../../validations.service';
import {SharedService} from '../../../shared/shared.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

	private toastOptions = this.sharedService.toastOptions;
	private repeatPasswordSubscriber;
	private newPasswordSubscriber;
  public submiting;
  public status = 0;

	public itemForm = this.fb.group({
		currentPassword: ['',[Validators.required]],
		newPassword: ['',[Validators.required, this.validationsService.passwordValid()]],
		repeatPassword: ['',[Validators.required, ]],
  })
  
  @Input() userInfo;

	@ViewChild('repeatPassword') inputRepeatPassword: ElementRef<HTMLInputElement>;
	@ViewChild('newPassword') inputNewPassword: ElementRef<HTMLInputElement>;

  constructor(private fb: FormBuilder,
  					  private settingsService:SettingsService,
  					  private toast: ToastService,
  					  public sharedService:SharedService,
  					  private validationsService: ValidationsService) { }
 // @Inject(DOCUMENT) public document
  ngOnInit() {


  	this.repeatPasswordSubscriber = this.itemForm.get('repeatPassword').valueChanges.subscribe((value)=>{

  		if(this.itemForm.get('newPassword').value!=value){
  			this.itemForm.get('repeatPassword').setErrors({differentPassword:true});
  		}
  	});

  	this.newPasswordSubscriber = this.itemForm.get('newPassword').valueChanges.subscribe((value)=>{

  		this.inputRepeatPassword.nativeElement.focus();
  		this.inputNewPassword.nativeElement.blur();
  		this.inputNewPassword.nativeElement.focus();

			this.itemForm.get('repeatPassword').updateValueAndValidity();
  	});

  }

  ngOnDestroy(){
    this.repeatPasswordSubscriber.unsubscribe();
    this.newPasswordSubscriber.unsubscribe();
  }


  async onSubmit(){
    this.submiting = true;
    let req = this.itemForm.value;
    req.email = this.userInfo.email;
    this.itemForm.disable();
    try{
      let response = await this.settingsService.resetPassword(req);
      if (response.success) { 
        this.status = 1;
      } else {
        this.toast.error(response.error.message,'', this.toastOptions);
      }
    }catch(err){
      console.error(err.message);
      this.toast.error('Unexpected error occured. Try again later','', this.toastOptions);
    }
    this.itemForm.enable();
    this.submiting=false;

  }


  public newPasswordErrorMessage():string{
		  if (this.itemForm.get('newPassword').hasError('passwordInvalid')) {
		    return 'The password must contain at least: 8 characters, 1 capital letter and 1 number';
		  } else if(this.itemForm.get('newPassword').hasError('required')){
		    return 'This field is required';
		  } else{
        return ' ';
      }
  }

  public repeatPasswordErrorMessage():string{
		  if(this.itemForm.get('repeatPassword').hasError('required')){
		    return 'This field is required';
		  } else if(this.itemForm.get('repeatPassword').hasError('differentPassword')){
		  	return 'Passwords must match';
		  } else{
        return ' ';
      }
  }


}
