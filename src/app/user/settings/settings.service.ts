import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AngularFireStorage } from '@angular/fire/storage';
import {SharedService} from '../../shared/shared.service';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import * as firebase from 'firebase/app';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private APIURL = environment.APIURL;

  public birthYearOptions = [];

  public expirationYearOptions = [];  


  public IDTypeOptions = this.sharedService.IDTypeOptions;


  constructor(private storage: AngularFireStorage,
              private sharedService:SharedService,
              private http: HttpClient) {

    let lastYear = new Date().getFullYear()-3;
    for(let i = lastYear-100; i<lastYear;i++){
     this.birthYearOptions.push({ value: String(i), label: String(i) });
    }

    let currentYear = new Date().getFullYear();
    for(let i = currentYear; i<currentYear+100;i++){
     this.expirationYearOptions.push({ value: String(i), label: String(i) });
    }


  }


  public updloadIDVerificationRequestField(uid:string, email:string, fields:any, role:string){
    const db = firebase.firestore();


		const batch = db.batch();

    fields.firstName = fields.firstName.toLowerCase();
    fields.lastName = fields.lastName.toLowerCase();
    fields.country = this.sharedService.countriesOptions.find((element)=>element.value==fields.country).label.toLowerCase();
    fields.IDType = this.IDTypeOptions.find((element)=>element.value==fields.IDType).label.toLowerCase();
		fields.status = 'pending';
    fields.uid = uid;
    fields.email = email;
    fields.role = role;
    fields.creationTS = Date.now();

    let id = db.collection('uniqueID').doc().id;

    if (role=='real estate agent') {
      batch.set(db.doc(`users/${uid}/requests/realEstateAgentIDVerification`), fields);
    }else{
      delete fields.brokerage, delete fields.licenceNo;
      batch.set(db.doc(`users/${uid}/requests/IDVerification`), fields);
    }
    batch.set(db.doc(`admin/requests/verifications/${id}`), fields);
    
		return batch.commit();
  }


  public getVerificationRequestFields(uid:string, role:string){
    const db = firebase.firestore();

    if (role=='real estate agent') {
      return db.doc(`users/${uid}/requests/realEstateAgentIDVerification`).get();
    }else{
  	  return db.doc(`users/${uid}/requests/IDVerification`).get();
    }
  }


  public uploadPhoto(uid:string,file:File,fileName:string){
 		let fileRef = this.storage.ref(this.getFilePath(uid, fileName));
    let task = fileRef.put(file, {customMetadata:{fileName: file.name }});
    return task.snapshotChanges().pipe(map(snapshot=>(snapshot.bytesTransferred / snapshot.totalBytes) * 100));
  }


  public newFileName(type:string):string{
    const db = firebase.firestore();
    let id = db.collection('uniqueID').doc().id;
    return `${id}.${this.getFileExtension(type)}`;
  }

  private getFilePath(uid:string,fileName:string){
    return `users/${uid}/requests/IDVerification/images/${fileName}`;
  }

  private getFileExtension(type: any): string{
      switch (type) {
          case 'image/jpeg':
              return 'jpeg';
          case 'image/jpg':
              return 'jpg';
          case 'image/gif':
              return 'gif';
          case 'image/png':
              return 'png';
          case 'image/svg+xml':
              return 'svg';
          default:
              return '';
              break;
      }
  }

  public getImageURL(uid:string,fileName:string){
    let fileRef = this.storage.storage.ref(this.getFilePath(uid, fileName));
    return fileRef.getDownloadURL();
  }
  
  public resetPassword(user:{currentPassword:string, newPassword:string,email:string}):Promise<any>{
    return this.http.post(`${this.APIURL}/settings/resetPassword`,user,httpOptions).toPromise();
  }

  public setRole(uid:string, role:string){
    const db = firebase.firestore();
    return db.doc(`users/${uid}/settings/preferences`).update({role});
  }


}
