import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {SharedService} from '../../../shared/shared.service';
import {ToastService, ModalDirective} from 'ng-uikit-pro-standard';
import {PricingService} from '../../../shared/pricing.service';
import {AuthService} from '../../../auth.service';
import { Router }  from '@angular/router';

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.css']
})
export class MembershipComponent implements OnInit {

  public canceling;
  public selectingPaymentMethod;
  public currencySelected;
  public currencies:any[];
  private toastOptions = this.sharedService.toastOptions;

  @Input() userInfo;

  @ViewChild('cancelationModal') public cancelationModal:ModalDirective;
  @ViewChild('paymentMethodModal') public paymentMethodModal:ModalDirective;

  constructor(
    private sharedService:SharedService, 
    private toast: ToastService, 
    private pricingService:PricingService,
    private router: Router,
    private authService:AuthService) { }

  ngOnInit() {
  }

  public pricingInUSD(){
    return{free:'0.00', basic:'4.95', plus:'9.95', professional:'14.95', enterprise:'19.95' };
  }

  get billingDate(){
    let formatDate = this.sharedService.formatDate(this.userInfo.membership.nextPayment);
    return `${formatDate.month}, ${formatDate.dayOfMonth} ${formatDate.year}`;
  }

  async openPaymentMethodModal(){
    this.paymentMethodModal.show();
    this.currencySelected = {name: this.userInfo.membership.paymentMethod.name, currency: this.userInfo.membership.paymentMethod.currency};
    this.currencies = await this.sharedService.getCurrencies();
  }

  selectCurrency(item){
    if(!this.selectingPaymentMethod) this.currencySelected = item;
  }

  async selectPaymentMethod(){
    if (this.userInfo.membership.paymentMethod.currency==this.currencySelected.currency) {
      this.toast.error('Payment Method already selected','', this.toastOptions); return;
    }

    if(!this.userInfo.balances[this.currencySelected.currency]){
      this.toast.error('You have to create this wallet first','', this.toastOptions); return;
    }
      
    this.selectingPaymentMethod = true;
    let userInfo = this.userInfo;

    try {
      let response = await this.pricingService.selectPaymentMethod({currency: this.currencySelected.currency});

      if (response.success) {

        this.paymentMethodModal.hide();

        userInfo.membership.paymentMethod = {
          currency: this.currencySelected.currency,
          name: this.currencySelected.name,
        }
        this.authService.userInfo.next(userInfo);
        this.toast.success(`${this.currencySelected.name} selected as Payment Method`,'Payment Method selected', this.toastOptions);

      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'',this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.selectingPaymentMethod = false;

  }



  public openCancelationModal(){
    this.cancelationModal.show();
  }

  public async cancelPricingPlan(){
    this.canceling = true;
    let userInfo = this.userInfo;

    try {
      let response = await this.pricingService.cancelPricingPlan();

      if (response.success) {

        this.cancelationModal.hide();

        userInfo.membership.pricingPlan = 'free';
        this.authService.userInfo.next(userInfo);

        this.toast.success(`You're now a free Member`,'Pricing Plan downgraded', this.toastOptions);

      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'',this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.canceling = false;
  }

  viewTransactionHistory(){
    this.router.navigate(['/my-wallet'], { queryParams: {panel:3} });
  }



}
