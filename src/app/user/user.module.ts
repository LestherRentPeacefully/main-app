/* Angular 2 modules */

import { NgModule } from '@angular/core';


/* Google maps module */

import { AgmCoreModule } from '@agm/core';


/* Routes */

import {userRoutingModule} from './userRoutes';


/* Shared modules */


import { SharedModule } from '../shared/shared.module';

import {environment} from '../../environments/environment';

/* Components */

import { UserComponent } from './user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { ProfileComponent } from './profile/profile.component';
import { IdVerificationCenterComponent } from './settings/id-verification-center/id-verification-center.component';
import { ChangePasswordComponent } from './settings/change-password/change-password.component';
import { MembershipComponent } from './settings/membership/membership.component';



@NgModule({
  imports: [
    SharedModule,
    AgmCoreModule.forRoot({apiKey: environment.googleMaps.apiKey, libraries: ["places","geometry"]}),
    userRoutingModule
    ],

  declarations: [DashboardComponent, UserComponent, SettingsComponent, ProfileComponent, IdVerificationCenterComponent, ChangePasswordComponent, MembershipComponent]
})
export class UserModule { }
