import { Injectable } from '@angular/core';
import { HttpClient }from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';
import { SharedService } from '../../shared/shared.service';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class RentalAgreementService {

	private APIURL = environment.APIURL;


  /*GENERAL*/

  // public leaseDateOptions = [
  //           { value: '1', label: 'Ends on a set date' },
  //           { value: '2', label: 'Has no set end date' },
  // ];
  
  // public renewalOptions = [
  //           { value: '1', label: 'Monthly' },
  //           { value: '2', label: 'Yearly' },
  // ];

  public accessToParkingOptions = [
    { value: '1', label: 'Do not specify' },
    { value: '2', label: 'Yes' },
    { value: '3', label: 'No' },
  ];

  public occupantsOptions = [
    { value: '1', label: 'Do not specify' },
    { value: '2', label: 'Yes' },
    { value: '3', label: 'No' },
];

  /*TERMS*/


  public rentIncreaseOptions = [
            { value: '1', label: 'Do not specify' },
            { value: '2', label: 'Specify number of days' },
            // { value: '3', label: 'Legal minimum' },
  ];


  public smokingAllowanceOptions = [
            { value: '1', label: 'Do not specify' },
            { value: '2', label: 'Yes' },
            { value: '3', label: 'No' },
  ];

  public latePaymentOptions = [
    { value: '1', label: 'Do not specify' },
    { value: '2', label: 'Percentage of rent' },
    { value: '3', label: 'Fixed charge per day' },
    { value: '4', label: 'Fixed charge per late payment' },
   ];

  public utilitiesDetailsOptions = [
    { value: '1', label: 'Do not specify' },
    { value: '2', label: 'Tenant pays for all utilities' },
    { value: '3', label: 'Specify' }
  ];

  public utilities=[{
        name:"Electricity",
        id:"1"
      },{
        name:"Water/Sewer",
        id:"2"
      },{
        name:"Internet",
        id:"3"
      },{
        name:"Cable",
        id:"4"
      },{
        name:"Telephone",
        id:"5"      
      },{
        name:"Natural Gas",
        id:"6"
      },{
        name:"Heating Oil/Propane",
        id:"7"
      },{
        name:"Garbage Collection",
        id:"8"
      },{
        name:"Alarm/Security System",
        id:"9"
  }];

  public disputeResolutionOptions = [
    { value: '1', label: 'Do not specify' },
    { value: '2', label: 'Mediation' },
    { value: '3', label: 'Arbitration' },
    { value: '4', label: 'Mediation then Arbitration' }
  ];

  public disputeResolutionCostOptions = [
    { value: '1', label: 'Both equally' },
    { value: '2', label: 'Landlord' },
    { value: '3', label: 'Tenant' }
  ];

  public additionalClauseOptions = [
    { value: '1', label: 'Yes' },
    { value: '2', label: 'No' }
  ];


  constructor(private http: HttpClient, 
              private afs:AngularFirestore,
              public afAuth: AngularFireAuth, 
              private sharedService:SharedService) {

  }




  public formatClauses(unformatedClauses:string[], rentalAgreement:any, propertyRented:any){
    const clauses = {

      general:{
        property:{
          type:unformatedClauses[0],
          address:propertyRented.property.address,
        },
        leaseDetails:{
          dateLeaseStarts:(+unformatedClauses[1])*1000
        }
      },
      property:{
        details:{
          selectedAccessToParking:unformatedClauses[2]
        }
      },
      parties:{
        tenant:propertyRented.tenant,
        landlord:propertyRented.landlord,
        selectedOccupantsOptions:unformatedClauses[5]
      },
      terms:{
        rent:{
          amount:unformatedClauses[6],
          paymentDateDay:unformatedClauses[7]
        },
        rentIncrease:{
          selectedIncrementNotice:unformatedClauses[8],
          daysNoticeBeforeIncreasingRent:unformatedClauses[9]
        },
        securityDeposit:{
          amount:unformatedClauses[10]
        },
        useOfProperty:{
          selectedPetsAllowance:unformatedClauses[11],
          selectedSmokingAllowance:unformatedClauses[12]
        },
        latePayments:{
          selectedLatePayment:unformatedClauses[13],
          amount:unformatedClauses[14],
          percentage:(+unformatedClauses[15])/10
        },
        utilitiesDetails:{
          selectedUtilitiesDetails:unformatedClauses[16],
          utilities:unformatedClauses[17].split(',')
        }
      },
      finalDetails:{
        disputeResolution:{
          selectedDisputeResolution:unformatedClauses[18],
          selectedDisputeResolutionCost:unformatedClauses[19]
        },
        additionalClauses:{
          selectedAdditionalClause:unformatedClauses[20],
          clause:rentalAgreement.additionalClauses.clause
        }
      },
      contract:{...rentalAgreement.contract,...{txHash:rentalAgreement.txHash}}
    };

    return clauses;


  }


  public async getIdentityWallet(uid:string){
    const db = firebase.firestore();
    return (await db.collection(`wallets/identity/public`).where('uid','==',uid).limit(1).get()).docs[0].data();
  }

  public getRentalAgreement$(propertyRentedID:string){
    return this.afs.collection('rentalAgreements', ref=>ref.where('propertyRentedID','==', propertyRentedID).limit(1)).valueChanges();
  }


  public async deployContract(req:any):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/rental-agreement/new`,req,_httpOptions).toPromise();        
  }

  public async smartContractCodePreview(req:any):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/rental-agreement/smartContractCodePreview`,req,_httpOptions).toPromise();  
  }

  public async estimateGasAndGasPriceContractCreation(req:any):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/rental-agreement/estimateGasAndGasPriceContractCreation`,req,_httpOptions).toPromise(); 
  }

  public async estimateGasAndGasPriceRentalAgreementAction(req:any):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/rental-agreement/estimateGasAndGasPriceRentalAgreementAction`,req,_httpOptions).toPromise();  
  }

  public async rentalAgreementAction(req:any):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/rental-agreement/rentalAgreementAction`,req,_httpOptions).toPromise(); 
  }

  public async viewClauses(req:{contractAddress:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/rental-agreement/viewClauses`,req,_httpOptions).toPromise(); 
  }    

  public async getRentalAgreementActions(req:{contractAddress:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/rental-agreement/getRentalAgreementActions`,req,_httpOptions).toPromise();  
  }

  public async viewContractState(req:{contractAddress:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/rental-agreement/viewContractState`, req ,_httpOptions).toPromise();
  }

  public async checkApproval(req:{contractAddress:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/rental-agreement/checkApproval`,req,_httpOptions).toPromise(); 
  }



}
