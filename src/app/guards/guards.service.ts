import { Injectable, NgZone } from '@angular/core';
import { CanActivate, Router, Route, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, CanActivateChild } from '@angular/router';
import { map, filter} from 'rxjs/operators';
import {AuthService} from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class GuardsService implements CanActivate, CanActivateChild, CanLoad {

  constructor(private router: Router,private authService: AuthService, private ngZone: NgZone) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let url: string = state.url;

    return this.checkLogin(url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }

  canLoad(route: Route) {
    let url = `/${route.path}`;

    return this.checkLogin(url);
  }

  checkLogin(url: string){

    // this.authService.redirectUrl = url;

    return this.authService.authenticated.pipe(filter(isAuthenticated => isAuthenticated !== undefined), map(isAuthenticated=>{
      
      if(isAuthenticated) return true;
      this.ngZone.run(() => this.router.navigateByUrl('/account/login') );
      
      return false;
    }));
  }


}
