import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.component.html',
  styleUrls: ['./how-it-works.component.css']
})
export class HowItWorksComponent implements OnInit {

	public app = environment.app;
	public tenantOptionStep = 1;
	public landlordOptionStep = 1;
	public serviceProviderOptionStep = 1;


  constructor() { }

  ngOnInit() {
  }

}
