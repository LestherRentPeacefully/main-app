import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { SharedService } from '../shared/shared.service';

@Injectable({
  providedIn: 'root'
})
export class AgreementsService {

  private APIURL = environment.APIURL;

  constructor(
    private http: HttpClient,
    private afs: AngularFirestore,
    private sharedService: SharedService
  ) { }


  async getAgreements(uid: string, role: string){
    const db = firebase.firestore();
    let snapshot: firebase.firestore.QuerySnapshot;
    let docs = [];
    if(role=='landlord'){
      snapshot = await db.collection('agreements').where('landlord.uid','==',uid).get();
    } else if(role=='real estate agent'){
      snapshot = await db.collection('agreements').where('realEstateAgent.uid','==',uid).get();
    }
    snapshot.forEach(doc=> docs.push(doc.data()));
    return docs;
  }

  getMyAgreement$(agreementId: string){
    return this.afs.doc(`agreements/${agreementId}`).valueChanges();
  }

  async getSignaturesRequest(agreementId: string, offset:number, lastVisibleTS:number){
    const db = firebase.firestore();
    let signaturesRequest = [];

    let docs: firebase.firestore.QueryDocumentSnapshot[];

    let query = db.collection(`agreements/${agreementId}/signaturesRequest`).orderBy('creationTS','desc');

    if(!lastVisibleTS) {
      docs = (await query.limit(offset).get()).docs;
    } else {
      docs = (await query.startAfter(lastVisibleTS).limit(offset).get()).docs;
    }
    
    docs.forEach(doc=> signaturesRequest.push(doc.data()));
    return signaturesRequest;
  }

  public async getArrayOfSignatures(req:{signaturesRequestID: string[]}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/agreements/getArrayOfSignatures`, req, _httpOptions).toPromise(); 
  }

  public async getCurrencyPriceForEsign(req:{currency: string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/agreements/getCurrencyPriceForEsign`,req,_httpOptions).toPromise(); 
  }

  public async payForEsignWithCrypto(req:{currency: string, agreementId: string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/agreements/payForEsignWithCrypto`,req,_httpOptions).toPromise(); 
  }

  public async getSignUrl(req:{signatureID:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/agreements/getSignUrl`, req, _httpOptions).toPromise(); 
  }


  public async uploadDocument(req:FormData, cb1, cb2, cb3){
    const token = await this.sharedService.getToken();

    const xhr = new XMLHttpRequest();

    xhr.upload.addEventListener('progress', oEvent=> {if (oEvent.lengthComputable) cb1(oEvent.loaded / oEvent.total * 100)});
    xhr.addEventListener('load', ()=> cb2(JSON.parse(xhr.response)));
    xhr.addEventListener('error', ()=> cb3());
    
    xhr.open('POST', `${this.APIURL}/agreements/uploadDocument`);
    xhr.setRequestHeader('Authorization', token);
    xhr.send(req);
  }


  async estimateGasAndGasPriceDocSaving(req: {agreementId: string}) {
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:{estimateGas:number, gasPrice:number}, error:{message?:string}}>(`${this.APIURL}/agreements/estimateGasAndGasPriceDocSaving`, req, _httpOptions).toPromise(); 
  }


  public async getDocUrl(req:{signatureRequestID:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/agreements/getDocUrl`, req, _httpOptions).toPromise(); 
  }

  async uploadToIPFS(req: {url: string}) {
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:string, error:{message?:string}}>(`${this.APIURL}/agreements/uploadToIPFS`, req, _httpOptions).toPromise(); 
  }

  async saveDocOnBlockchain(req: {ipfsHash: string, agreementId: string, landlordId: string, signatureRequestSelectedID: string, gasPrice: number, gasLimit: number}) {
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:string, error:{message?:string}}>(`${this.APIURL}/agreements/saveDocOnBlockchain`, req, _httpOptions).toPromise(); 
  }


  public async estimateGasAndGasPriceContractCreation(){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/agreements/estimateGasAndGasPriceContractCreation`, {}, _httpOptions).toPromise(); 
  }

  public async createSmartContract(req: { agreementId: string, gasPrice: number, gasLimit: number}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:string, error:{message?:string}}>(`${this.APIURL}/agreements/createSmartContract`, req, _httpOptions).toPromise(); 
  }

  public async endLease(req: {agreementId: string}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:string, error:{message?:string}}>(`${this.APIURL}/agreements/endLease`, req, _httpOptions).toPromise(); 
  }

  public async cancelCulmination(req: {agreementId: string}){
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:string, error:{message?:string}}>(`${this.APIURL}/agreements/cancelCulmination`, req, _httpOptions).toPromise(); 
  }

  async startChat(req: {content: string, agreementId: string}) {
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post<{success:boolean, data:{chatID:string, messageID:string}, error:{message?:string}}>(`${this.APIURL}/agreements/startChat`, req, _httpOptions).toPromise(); 
  }



}
