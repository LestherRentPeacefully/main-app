import { NgModule } from '@angular/core';


import { SharedModule } from '../shared/shared.module';


import { AgreementsRoutingModule } from './agreements-routing.module';


import { AgreementsComponent } from './agreements.component';
import { AllComponent } from './all/all.component';
import { DetailsComponent } from './details/details.component';
import { GeneralComponent } from './details/general/general.component';
import { SettingsComponent } from './details/settings/settings.component';
import { EsignedDocsComponent } from './details/esigned-docs/esigned-docs.component';
import { SmartContractComponent } from './details/smart-contract/smart-contract.component';


@NgModule({
  declarations: [AgreementsComponent, AllComponent, DetailsComponent, GeneralComponent, SettingsComponent, EsignedDocsComponent, SmartContractComponent],
  imports: [
    SharedModule,
    AgreementsRoutingModule
  ]
})
export class AgreementsModule { }
