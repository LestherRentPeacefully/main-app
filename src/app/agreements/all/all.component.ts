import { Component, OnInit } from '@angular/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {AuthService} from '../../auth.service';
import { Router }  from '@angular/router';
import {SharedService} from '../../shared/shared.service';
import {AgreementsService} from '../agreements.service';
import { filter } from 'rxjs/operators';
import { MyUser } from '../../shared/interfaces';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {

  private toastOptions = this.sharedService.toastOptions;
  public userInfo: MyUser;
  public agreements:any[];
  public userInfoSubscriber: any;

  constructor(
    private toast: ToastService,
    private router: Router,
    private authService:AuthService,
    private agreementsService: AgreementsService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {

    this.userInfoSubscriber = this.authService.userInfo.pipe(filter(userInfo=>!!userInfo)).subscribe(async (userInfo) => {
      this.userInfo = userInfo;
      this.agreements = await this.agreementsService.getAgreements(this.userInfo.uid, this.userInfo.role);
    });
  }

  ngOnDestroy() {
    this.userInfoSubscriber.unsubscribe();
  }

  public createdAt(timestamp: number) : string {
    let date = new Date(timestamp);
    return `${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()}`;
  }

}
