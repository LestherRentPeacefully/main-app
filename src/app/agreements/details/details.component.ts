import { Component, OnInit, OnDestroy } from '@angular/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {AuthService} from '../../auth.service';
import {SharedService} from '../../shared/shared.service';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import {AgreementsService} from '../agreements.service';
import { Agreement, User } from '../../shared/interfaces';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit, OnDestroy {

  private sub: Subscription;
  public selectedPanel = 1;
  private subscriber: Subscription;
  public loadingAgreement = true;
  public userInfo:User;
  public loadedOnce: boolean;
  public agreementId:string;
  public agreement: Agreement;

  constructor(
    private toast: ToastService,
    private authService:AuthService,
    private sharedService: SharedService,
    private agreementsService: AgreementsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.sub = this.route.paramMap.subscribe((params)=>{
      this.agreementId = params.get('id');
      this.loadingAgreement =true;

      this.subscriber = combineLatest(
        this.agreementsService.getMyAgreement$(this.agreementId), this.authService.userInfo
      ).pipe(filter(([agreement, userInfo])=>!!agreement && !!userInfo)).subscribe(async([agreement, userInfo])=>{

        this.agreement = <Agreement>agreement;
        this.userInfo = userInfo;

        this.loadingAgreement = false;

      });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.subscriber.unsubscribe();
  }

  public panelShown(panel: number){
    this.selectedPanel = panel;
  }

}
