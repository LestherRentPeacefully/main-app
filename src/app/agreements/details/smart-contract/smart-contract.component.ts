import { Component, OnChanges, Input, ViewChild } from '@angular/core';
import { Agreement } from 'src/app/shared/interfaces';
import { ModalDirective, ToastService } from 'ng-uikit-pro-standard';
import { environment } from '../../../../environments/environment';
import {BigNumber} from 'bignumber.js';
import { EthereumService } from 'src/app/my-wallet/identity-wallet/ethereum.service';
import {SharedService} from '../../../shared/shared.service';
import { Validators, FormBuilder } from '@angular/forms';
import {AgreementsService} from '../../agreements.service';

@Component({
  selector: 'app-smart-contract',
  templateUrl: './smart-contract.component.html',
  styleUrls: ['./smart-contract.component.css']
})
export class SmartContractComponent implements OnChanges {

  @Input() agreement: Agreement;
  @Input() selectedPanel: number;
  @Input() realEstateAgent: any;
  @Input() landlord: any;
  @Input() userInfo;


  @ViewChild('sendTX') public sendModal:ModalDirective;
  @ViewChild('txModal') public txModal:ModalDirective;

  private toastOptions = this.sharedService.toastOptions;
  public estimating: boolean;
  public minGas: number;
  public submitting: boolean;
  public balance: number;

  public etherscanURL = environment.etherscanExplorer;

  public itemForm = this.fb.group ({
    gasLimit: ['', [ Validators.required, Validators.min(21000) ]],
    gasPriceInGWei: ['', [ Validators.required, Validators.min(1) ]]
  });

  constructor(
    private toast: ToastService,
    private sharedService:SharedService,
    private fb: FormBuilder,
    private ethereumService:EthereumService,
    private agreementsService: AgreementsService
  ) { }

  ngOnChanges() {

    if(this.selectedPanel !== 3 || !this.realEstateAgent || !this.landlord) return;
  }

  public newBigNumber(number: any){
    return new BigNumber(number);
  }


  public isRealEstateAgent(uid: string) {
    return this.agreement.realEstateAgent.uid === uid;
  }

  public isLandlord(uid: string) {
    return this.agreement.landlord.uid === uid;
  }


  async gasAndGasPrice(){
    this.sendModal.show();
    this.estimating = true;
    try {

      const [response1, response2] = await Promise.all([
        this.agreementsService.estimateGasAndGasPriceContractCreation(),
        this.ethereumService.getBalances()
      ]);

      if(!response1.success) {
        console.error(response1.error.message);
        this.toast.error(response1.error.message,'',this.toastOptions);
        this.estimating = false;
        return;
      }

      if(!response2.success) {
        console.error(response2.error.message);
        this.toast.error(response2.error.message,'',this.toastOptions);
        this.estimating = false;
        return;
      }

      this.itemForm.get('gasPriceInGWei').setValue(new BigNumber(response1.data.gasPrice).times(new BigNumber(10).pow(-9)).toNumber());
      this.itemForm.get('gasLimit').setValue(new BigNumber(response1.data.estimateGas).times(1.20).integerValue().toNumber());
      this.minGas = new BigNumber(response1.data.estimateGas).toNumber();

      this.balance = response2.data.ETH;

    } catch (err) {
      console.error(err.message || err);
      this.toast.error('Unexpected error estimating gas. Try again later','', this.toastOptions);
    }

    this.estimating = false;
  }




  async createSmartContract(){
    if (this.itemForm.get('gasLimit').hasError('required') || this.itemForm.get('gasLimit').value<this.minGas) {
     return this.toast.error(`Gas limit must be greater or equal than ${this.minGas}`);
    } 
    if(this.itemForm.get('gasPriceInGWei').hasError('required') || this.itemForm.get('gasPriceInGWei').hasError('min')){
     return this.toast.error('Gas Price must be equal to or greater than 1 GWei','', this.toastOptions);
    }
    if(new BigNumber(this.itemForm.value.gasLimit).times(this.itemForm.value.gasPriceInGWei).times(new BigNumber(10).pow(9)).isGreaterThan(this.balance) ) {
      return this.toast.error('Insufficient funds in your Identity Wallet','', this.toastOptions);
    }

    const req = {
      agreementId: this.agreement.id,
      gasLimit: this.itemForm.value.gasLimit,
      gasPrice: new BigNumber(this.itemForm.value.gasPriceInGWei).times(new BigNumber(10).pow(9)).toNumber(),
    };

    this.submitting = true;
    this.itemForm.disable();

    try {
      const response = await this.agreementsService.createSmartContract(req);

     if(response.success) {
      this.agreement.smartContract = {
        status: 'pending',
        txHash: response.data,
        address: null,
        gasPrice: null,
        gasUsed: null

      }
      this.sendModal.hide();
      this.txModal.show();

     } else {
      console.error(response.error.message);
      this.toast.error(response.error.message,'', this.toastOptions);
     }


    } catch (err) {
      console.error(err.message || err);
      this.toast.error('Unexpected error estimating gas. Try again later','', this.toastOptions);
    }

    this.submitting = false;
    this.itemForm.enable();
    
  }




}
