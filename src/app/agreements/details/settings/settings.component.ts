import { Component, OnChanges, Input, ViewChild } from '@angular/core';
import { Agreement, User } from 'src/app/shared/interfaces';
import {SharedService} from '../../../shared/shared.service';
import {AgreementsService} from '../../agreements.service';
import { Router } from '@angular/router';
import {ToastService, TabsetComponent, ModalDirective} from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnChanges {

  @Input() agreement: Agreement;
  @Input() selectedPanel: number;
  @Input() userInfo: User;

  @ViewChild('endLeaseModal') public endLeaseModal:ModalDirective;
  
  
  private toastOptions = this.sharedService.toastOptions;
  public endingLease;
  public cancelingCulmination;
  public deletingProperty;


  constructor(
    private router: Router,
    private toast: ToastService,
    private sharedService:SharedService,
    private agreementsService: AgreementsService
  ) { }

  ngOnChanges() {
  }


  openEndLeaseModal(){
    this.endLeaseModal.show();
  }


  public isRealEstateAgent(uid: string) {
    return this.agreement.realEstateAgent.uid === uid;
  }

  public isLandlord(uid: string) {
    return this.agreement.landlord.uid === uid;
  }


  async endLease(){
    this.endingLease = true;
    try {
      const response = await this.agreementsService.endLease({agreementId: this.agreement.id});

      if (response.success) {
        this.endLeaseModal.hide();
        if (this.agreement.status=='active') {
          this.toast.success('','Request sent successfully', this.toastOptions);
          this.agreement.status = 'endingLease';
          this.agreement.requestedBy = this.userInfo.uid;

        } else if(this.agreement.status=='endingLease'){
          this.toast.success('','Lease ended successfully', this.toastOptions);
          this.router.navigateByUrl(`agreements/all`);
        }

      } else {
        console.error(response.error.message);
        this.toast.error(response.error.message,'Error', this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.endingLease = false;
  }


  async cancelCulmination(){
    this.cancelingCulmination = true;
    try {
      const response = await this.agreementsService.cancelCulmination({agreementId: this.agreement.id});

      if (response.success) {
        this.toast.success('','Request canceled successfully', this.toastOptions);
        this.agreement.status = 'active';
        delete this.agreement.requestedBy;

      }else{
        console.error(response.error.message);
        this.toast.error(response.error.message,'Error', this.toastOptions);
      }
    } catch (err) {
      console.error(err.message || err);
      this.toast.error('There was an error. Try again later','', this.toastOptions);
    }
    this.cancelingCulmination = false;
  }

}
