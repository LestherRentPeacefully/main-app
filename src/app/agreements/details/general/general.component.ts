import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {SharedService} from '../../../shared/shared.service';
import { Router } from '@angular/router';
import {AgreementsService} from '../../agreements.service';
import { Agreement, MyUser } from '../../../shared/interfaces';
import { TabsetComponent, ToastService } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnChanges {

  @Input() agreement: Agreement;
  @Input() userInfo: MyUser;
  @Input() selectedPanel: number;
  @Input() staticTabs: TabsetComponent;

  realEstateAgent: any;
  landlord: any;

  private toastOptions = this.sharedService.toastOptions;

  constructor(
    private router: Router, 
    public sharedService:SharedService,
    private toast: ToastService
  ) { }

  async ngOnChanges() {
    if(this.selectedPanel !== 1) return;

    this.realEstateAgent = null;
    this.landlord = null;
    
    if(this.userInfo.role === 'real estate agent'){
      this.realEstateAgent = this.userInfo;
      this.landlord = await this.sharedService.getUser(this.agreement.landlord.uid);

    } else if(this.userInfo.role === 'landlord') {
      this.realEstateAgent = await this.sharedService.getUser(this.agreement.realEstateAgent.uid);
      this.landlord = this.userInfo;
    }
    
  }


  viewProfile(uid: string, role: string){
    this.router.navigateByUrl( `/user/${uid}`, { queryParams: {role}});
  }

  public isRealEstateAgent(uid: string) {
    return this.agreement.realEstateAgent.uid === uid;
  }

  public isLandlord(uid: string) {
    return this.agreement.landlord.uid === uid;
  }

  viewListing(){
    this.router.navigate([`listings/details/${this.agreement.listingID}`]); 
  }

  newListing() {
    this.router.navigate([`listings/new`], {queryParams: {agreementId:this.agreement.id}}); 
  }

  public panelShown(panel: number){
    this.selectedPanel = panel;
    this.staticTabs.setActiveTab(panel);
  }

  public get openMessagesObj() {
    const obj = {
      chatName: `(${this.sharedService.capitalize(this.landlord.firstName)} ${this.landlord.lastName[0].toUpperCase()}) Property Agreement`,
      id: this.agreement.chatID,
      user: this.isLandlord(this.userInfo.uid)? this.realEstateAgent : this.landlord,
      agreementId: this.agreement.id
    }
    return obj;
  }


}
