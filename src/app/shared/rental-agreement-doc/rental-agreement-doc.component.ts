import { Component, OnInit, Input } from '@angular/core';
import {SharedService} from '../shared.service';
import {RentalAgreementService} from '../../services/rental-agreement/rental-agreement.service';

@Component({
  selector: 'app-rental-agreement-doc',
  templateUrl: './rental-agreement-doc.component.html',
  styleUrls: ['./rental-agreement-doc.component.css']
})
export class RentalAgreementDocComponent implements OnInit {

  public propertyOptions = this.sharedService.propertyOptions;

  @Input() clauses:any;


  constructor(private sharedService:SharedService,
              private rentalAgreementService:RentalAgreementService) { }

  ngOnInit() {

  }


  get propertyType() {
    return this.propertyOptions.find(item => item.value == this.clauses.general.property.type).label.toLocaleLowerCase();
  }

  get tenantPays(){
    let tenantPays = [];
    for(let i = 0; i<this.clauses.terms.utilitiesDetails.utilities; i++){
      if(this.clauses.terms.utilitiesDetails.utilities[i]=='1'){
        tenantPays.push(this.rentalAgreementService.utilities[i].name.toLocaleLowerCase());
      }
    }
    return tenantPays.join(', ');
  }

  get landlordPays(){
    let landlordPays = [];
    for(let i = 0; i<this.clauses.terms.utilitiesDetails.utilities; i++){
      if(this.clauses.terms.utilitiesDetails.utilities[i]=='2'){
        landlordPays.push(this.rentalAgreementService.utilities[i].name.toLocaleLowerCase());
      }
    }
    return landlordPays.join(', ');
  }



  get paymentDateDay(){ 
    return this.sharedService.daysOfTheMonthOptions.find(item=> item.value==this.clauses.terms.rent.paymentDateDay).label;
  }

  get dateLeaseStarts(){
    let currentDate = this.clauses.general.leaseDetails? this.clauses.general.leaseDetails.dateLeaseStarts: new Date().getTime();
    return this.sharedService.formatDate(currentDate);
  }

  get approvalDate(){
    return this.sharedService.formatDate((+this.clauses.approval.timestamp)*1000);
  }

  

  

}
