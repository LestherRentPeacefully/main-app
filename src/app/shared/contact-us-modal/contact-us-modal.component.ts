import { Component, OnInit, ViewChild } from '@angular/core';
import {SharedService} from '../shared.service';
import {ToastService} from 'ng-uikit-pro-standard';
import { Validators, FormBuilder } from '@angular/forms';
import {environment} from '../../../environments/environment';
import { ModalDirective } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-contact-us-modal',
  templateUrl: './contact-us-modal.component.html',
  styleUrls: ['./contact-us-modal.component.css']
})
export class ContactUsModalComponent implements OnInit {
	public submiting=false;
  private toastOptions = this.sharedService.toastOptions;
	public captchaProcessed=false;  
  public recapchaKey = environment.recapchaKey;

	public itemForm = this.fb.group ({
    name: ['',[ Validators.required ]],
    email: ['',[ Validators.required, Validators.email ]],
    subject: ['',[ Validators.required ]],
    message: ['',[ Validators.required ]],
  });

  @ViewChild('contactUs') public contactUsModal:ModalDirective;

  constructor(private fb: FormBuilder,public sharedService:SharedService,private toast: ToastService) { }

  ngOnInit() {
  }


	public handleCorrectCaptcha(e){
    this.captchaProcessed = true;
  } 



  public emailErrorMessage():string{
    if (this.itemForm.get('email').hasError('email')) {
      return 'Invalid format';
    } else if(this.itemForm.get('email').hasError('required')){
      return 'This field is required';
    } else{
      return ' ';
    }
  }

  async onSubmit(){
  	this.submiting = true;
  	let obj = this.itemForm.value;
  	this.itemForm.disable();
    try{
      let response = await this.sharedService.contactSupport(obj);
      this.submiting = false;
      if (response.success) { 
        this.contactUsModal.hide();
        this.itemForm.enable();
        this.toast.success(`Message sent. Our support team will contact you as soon as possible `,'', this.toastOptions);
      } else {
        this.itemForm.enable();
        this.toast.error(response.error.message,'', this.toastOptions);
      }
    }catch(err){
      this.submiting = false;
      this.itemForm.enable();
      this.toast.error('Unexpected error. Try again later','', this.toastOptions);
    }

  }


	public onHidden(){
		this.itemForm.reset();
  }



}
