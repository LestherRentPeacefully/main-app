/* Angular modules */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReCaptchaModule } from 'angular2-recaptcha';



/*Bootrap modules  */

import {NavbarModule, 
        WavesModule,
        PreloadersModule,
        InputsModule,
        InputUtilitiesModule,
        ModalModule,
        ChartsModule,
        TabsModule,
        DatepickerModule,
        SelectModule,
        ButtonsModule,
        ChartSimpleModule,
        AccordionModule,
        CollapseModule,
        TooltipModule,
        DropdownModule,
        CardsFreeModule,
        LightBoxModule,
        CharCounterModule,
        CarouselModule,
        BadgeModule,
        IconsModule,
        MaterialChipsModule,
        AutoCompleterModule,
        TableModule  } from 'ng-uikit-pro-standard'; 
 

 /* Components */

import { ContactUsModalComponent } from './contact-us-modal/contact-us-modal.component';
import { RentalAgreementDocComponent } from './rental-agreement-doc/rental-agreement-doc.component';
import { ScoreComponent } from './score/score.component';
import {ChatComponent} from './chat/chat.component';


/* Directives */

import { ScrollableDirective } from './scrollable/scrollable.directive';
import { MembershipIconComponent } from './membership-icon/membership-icon.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReCaptchaModule,
    WavesModule,
    InputsModule,
    ModalModule,
    SelectModule,
    ButtonsModule,
    CardsFreeModule,
    TooltipModule,
    PreloadersModule,
    IconsModule
  ],
  declarations: [ContactUsModalComponent, RentalAgreementDocComponent, ScoreComponent, ChatComponent, ScrollableDirective, MembershipIconComponent],
  exports:[
    ContactUsModalComponent,
    RentalAgreementDocComponent,
    ScoreComponent,
    ChatComponent,
    MembershipIconComponent,
    ScrollableDirective,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReCaptchaModule,
    PreloadersModule,
    NavbarModule,
    ChartsModule,
    DatepickerModule,
    SelectModule,
    ChartSimpleModule,
    AccordionModule,  
    LightBoxModule,
    WavesModule,
    InputsModule,
    InputUtilitiesModule,
    ModalModule,
    TabsModule,
    ButtonsModule,
    TooltipModule,
    CollapseModule,
    DropdownModule,
    CardsFreeModule,
    CharCounterModule,
    CarouselModule,
    BadgeModule,
    IconsModule,
    MaterialChipsModule,
    AutoCompleterModule,
    TableModule
  ]
})
export class SharedModule {

}
