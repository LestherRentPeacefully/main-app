
export interface MyUser extends User {
  preferences: Preferences;
  balances: { [currency: string]: Balance; };
  role: Preferences['role'];
}

export interface User { //-- /users/{userId} /public/{userId} /private/{userId}
  firstName: string;
  lastName: string;
  email: string;
  creationTS: number;
  uid: string;
  photo: string;
  status:{
    banned: boolean;
    emailVerified: boolean;
    IDVerified: boolean;
    realEstateAgentIDVerified: boolean;
  };
  tenant:{
    score: number;
    reviews: number;
    currentlyRented: number;
    totalRented: number;
    ontimePayments: number;
    employer:{
      reviews: number;
      activeJobs: number;
      serviceProvidersHired: number;
      paymentPromptness: number
    }
  };
  landlord:{
    score: number;
    reviews: number;
    currentlyListed: number;
    currentTenants: number;
    totalTenants: number;
    totalRented: number;
    avgTimeOfTicketResolution: number;
    employer:{
      reviews: number;
      activeJobs: number;
      serviceProvidersHired: number;
      paymentPromptness: number;
    }
  };
  serviceProvider:{
    score: number;
    reviews: number;
    jobsCompleted: number;
    activeJobs: number;
    qualityOfWork: number;
    finishedOnTime: number;
  };
  realEstateAgent:{
    score: number;
    reviews: number;
    currentlyListed: number;
    totalTenants: number;
    totalLandlords: number;
    totalRented: number;
    brokerage: string;
    licenceNo: string;
  };
  membership: {
    pricingPlan: string;
    nextPayment?: number;
    paymentMethod?: {
      currency: string;
      name: string;
    };
  }
};

export interface AreaServing { //-- /users/{userId}/areasServing/{areaId}
  name: string;
  role: string;
  geohash: string;
  geopoint: string;
  id: string;
  creationTS: number;
};

export interface ServiceProviderSkills { //-- /users/{userId}/profile/serviceProvider
  skill: { [skillId: string]: true}
};

export interface IDVerification { //-- /users/{userId}/requests/IDVerification  write true
  firstName: string;
  lastName: string;
  country: string;
  IDType: 'passport' | 'drivers licence' | 'national id card';
  status: 'pending' | 'rejected' | 'verified';
  rejectionReason?: string;
  uid: string;
  email: string;
  role: string;
  creationTS: number;
  dateOfBirth: {
    month: string;
    day: string;
    year:string;
  };
  IDFile: string;
  IDExpirationDate: {
    month: string;
    day: string;
    year:string;
  };
  selfieFile: string;
};

export interface RealEstateAgentIDVerification extends IDVerification { //-- /users/{userId}/requests/realEstateAgentIDVerification write true
  brokerage: string;
  licenceNo: string;
}

export interface AdminRequestVerif extends IDVerification { //-- /admin/requests/verifications/{verifId} write true
  brokerage?: string;
  licenceNo?: string;
}


export interface Notification { //-- users/{userId}/notifications/{notifId}
  title: string;
  content: string;
  id: string;
  creationTS: number;
  hasButton: boolean;
  buttonText?: string;
  action? : 'provide property tenant feedback' | 'provide property landlord feedback' | 
            'provide agreement real estate agent feedback' | 'provide agreement landlord feedback' | 
            'confirm property listing';
  listingID?: string;
  realEstateAgentID?: string;
  feedbackFor?: string;
};


export interface Preferences { //-- /users/{userId}/settings/preferences
  role: string;
  counters:{
    unreads:{
      notifications: number;
      messages: number;
    }
  }
};


export interface Editable { //-- /users/{userId}/profile/editable
  shortDescription: string;
  aboutMe: string;
  social:{
    website:{
      url: string;
      enabled: boolean
    };
    facebook:{
      url: string;
      enabled: boolean;
    };
    twitter:{
      url: string;
      enabled: boolean
    };
    instagram:{
      url: string;
      enabled: boolean
    };
    linkedin:{
      url: string;
      enabled: boolean
    };
  }      
};


export interface Review { //-- /users/{userId}/reviews/{reviewId}
  id: string;
  role: string;
  reason?: 'serviceProvidersJob' | 'myProperty' | 'agreement',
  serviceProviderJobID?: string;
  feedback: {
    score: number;
    review: number;
    qualityOfWork?: number; //for service provider
    finishedOnTime?: number; //for service provider
    paymentPromptness?: number; //for service provider
    ontimePayments?: number; //for tenant
    avgTimeOfTicketResolution?: number; //for landlord
  };
  providedBy: {
    uid: string;
    role: string;
  };
  creationTS: number;
};



export interface IdentityWallet { //-- /wallets/identity/public/{walletId} /wallets/identity/private/{walletId}
  currency: string;
  uid: string;
  id: string;
  searchable: string;
  address: string;
  walletFile: any;
  privateKey: string;
};

export interface EmailVerifCode { //-- /emailVerificationCodes/{emailCode} 
  uid: string;
  expirationDate: number;
};

export interface ResetPasswordCode { // -- /resetPasswordCodes/{code}
  uid: string;
  expirationDate: number;
};

export interface WalletFileVerifCode { //-- /walletFileVerificationCodes/{userId}
  code: string;
};

export interface WithdrawalCode { //-- /withdrawalCodes/{userId}
  code: string;
  uid: string;
  expirationDate: string;
}

export interface PublicWallet { //-- /wallets/deposit/public/{walletId}
  currency: string;
  uid: string;
  id: string;
  searchable: string;
  address: string;
  legacyAddress?: string;
};

export interface PrivateWallet extends PublicWallet { //-- /wallets/deposit/private/{walletId}
  privateKey: string;
  wif?: string;
};

export interface Balance { //-- /balances/{balanceId}
  currency: string;
  uid: string;
  id: string;
  available: string;
  onOrders: string;
  onEscrow: string;
};


export interface Agreement { //-- /agreements/{agreementId}
  id: string;
  status: 'active' | 'endingLease';
  chatID: string;
  requestedBy: string;
  listingID: string;
  isPropertyRented: boolean;
  creationTS: number;
  realEstateAgent: {
    firstName :string;
    lastName :string;
    uid :string;
    email: string;
  };
  landlord: {
    firstName :string;
    lastName :string;
    uid :string;
    email: string;
  };
  smartContract: {
    status: 'pending' | 'confirmed';
    address: string;
    txHash: string;
    // ipfsHash: string;
    gasPrice: number;
    gasUsed: number;
  }
};


export interface AgreementSignatureRequest { //-- /agreements/{agreementId}/signaturesRequest/{sigReqId}
  id: string;
  signatureRequestID: string;
  creationTS: number;
  smartContract: {
    status: 'pending' | 'confirmed';
    txHash: string;
    ipfsHash: string;
    gasPrice: number;
    gasUsed: number;
  };
};

export interface Chat { //-- /chats/{chatID}
  id: string;
  chatName: string;
  lastMessage: {
    content: string;
    uid: string;
    creationTS: number;
  };
  uids: string[];
};

export interface Message { //-- /chats/{chatID}/messages/{messageID}
  content: string;
  uid: string
  creationTS: number;
  id: string;
};


export interface Listing { //-- /listings/{listingId}
  id: string;
  user: {
    firstName: string;
    lastName: string;
    email: string;
    uid: string;
    role: 'landlord' | 'real estate agent'
  };
  includesLandlord?: boolean;
  agreementId?: string;
  myPropertyID?: string;
  landlord?: {
    firstName: string;
    lastName: string;
    email: string;
    uid: string;
  };
  address: string;
  location: {
    geopoint: firebase.firestore.GeoPoint;
    geohash: string;
  };
  unit: string;
  name: string;
  type: string;
  numOfBeds: string;
  numOfBaths: string;
  sqft: number;
  rentAmount: number
  securityDepositAmount: number;
  details: string;
  availableFrom: number;
  photos: string[];
  amenities: {[amenity: string]: boolean};
  creationTS: number;
  applicants: number;
  recommendedUntil: string
  status: 'wating for landlord confirmation' | 'pending' | 'listed' | 'rented' | 'rejected';
  rejectionReason?: string;
};


export interface ListingApplication { //-- /applications/{applicationId}
  id: string;
  listing: {
    name: string;
    address: string;
    creationTS: number;
    type: string;
    id: string;
    status: 'listed' | 'rented';
  };
  applicant: {
    offer: {rentAmount: number; securityDepositAmount: number;};
    isHighlighted: number;
    message: string;
    uid: string;
    status?: 'selected' | null;
    chatID: string;
  };
  creationTS: number;
};


export interface MyProperty { //-- /myProperties/{myPropertyId}
  id: string;
  user: {
    firstName: string;
    lastName: string;
    email: string;
    uid: string;
    role: string;
  };
  name: string;
  address: string
  location: {
    geopoint: firebase.firestore.GeoPoint;
    geohash: string;
  };
  unit: string;
  type: string;
  photos: string[];
  creationTS: number;
  tenantsID: string[];
  createdByRealEstateAgent: boolean;
  listingID: string;
  smartContract: {
    status: 'pending' | 'confirmed';
    txHash: string;
    address: string;
    gasPrice: number;
    gasUsed: number
  };
};


export interface PropertyTenant { //-- //myProperties/{myPropertyId}/tenants/{tenantId}
  id: string;
  firstName: string;
  lastName: string;
  uid: string;
  email: string;
  rentAmount: number;
  securityDepositAmount: number;
  creationTS: number;
  status: 'active' | 'endingLease';
  requestedBy?: string;
  chatID: string;
  state?: {
    depositInfo?: {
      creationTS: number;
      currencyForPayment: {
        currency: string,
        amount: string,
        name: string
      };
    };
    ticketInfo?: {
      creationTS: number;
      details: string;
      open: string;
    };
    rentModificationInfo?: {
      creationTS: number;
    };
  };
};



export interface PropertySignatureRequest { //-- /myProperties/{myPropertyId}/tenants/{tenantId}/signaturesRequest/{sigReqId}
  id: string
  signatureRequestID: string;
  creationTS: number;
  smartContract: {
    status: 'pending' | 'confirmed';
    txHash: string;
    ipfsHash: string;
    gasPrice: number;
    gasUsed: number;
  };
};


export interface PropertyHistory { //-- /myProperties/{myPropertyId}/tenants/{tenantId}/history/{historyId}
  id: string;
  action: 'deposit' | 'ticket' | 'rentModified';
  user: {
    firstName: string;
    lastName: string;
    email: string;
    uid: string;
  };
  currencyForPayment?: {
    currency: string;
    amount: string;
    name: string;
  };
  open?: boolean;
  newRent?: number;
  creationTS: number;
};


export interface LastPrice {
  currency: string;
  price: string;
  name: string
};

export interface LastPriceForFeesAndCharges extends LastPrice { //-- /lastPriceForFeesAndCharges/{userId}

};


export interface LastPriceForEsign extends LastPrice { //-- /lastPriceForEsign/{userId}

};

export interface LastPriceForMilestone extends LastPrice { //-- /lastPriceForMilestone/{userId}

}

export interface LastPriceForPlan extends LastPrice { //-- /lastPriceForPlan/{userId}
  
}


export interface Transaction { //-- /transactions/{transactionId}
  id: string;
  currency: string;
  type: 'fees and charges' | 'payment sent' | 'payment received' | 'contractExecution' | 'membership' | 'withdrawal' | 'deposit';
  action: 'contractCreation' | 'saveDocLocation';
  isInternal?: boolean;
  to?: string;
  myProperty?: {
    id: string;
    tenant: string;
    signatureRequestSelectedID: string;
  };
  agreement?: {
    id: string;
    landlord: string;
    signatureRequestSelectedID: string;
  };
  serviceProviderJobID?: string;
  processed: boolean;
  status: 'pending' | 'confirmed';
  txHash?: string;
  amount: string;
  fee? : string;
  details?: string;
  uid: string;
  creationTS: number;
};


export interface MyServiceProvidersJob { //-- /myServiceProvidersJobs/{serviceProviderJobId}
  id: string;
  name: string;
  details: string;
  price: string;
  skillsRequired: { [skillId: string]: true; };
  status: 'wating for acceptance' | 'ongoing' | 'canceled' | 'completed';
  creationTS: number;
  culminationTS: number;
  amountPaid: string;
  employer: {
    firstName: string;
    lastName: string;
    email: string;
    uid: string;
    role: 'tenant' | 'landlord';
  };
  serviceProvider: {
    firstName: string;
    lastName: string;
    email: string;
    uid: string;
  };
  employerProvidedFeedback: boolean;
  serviceProviderProvidedFeedback: boolean;
};


export interface Milestone { //-- /milestones/{milestoneId}
  amount: string;
  details: string;
  serviceProviderJobID: string;
  creationTS: number;
  id: string;
  status: 'proposed' | 'created' | 'canceled' | 'released';
  sender: {
    firstName: string;
    lastName: string;
    email: string;
    uid: string;
  };
  receiver: {
    firstName: string;
    lastName: string;
    email: string;
    uid: string;
  };
  currencyForPayment: {
    currency: string;
    amount: string;
    name: string;
  };
};


export interface Currency { //-- /currencies/{currency}
  currency: string;
  name: string;
  price: {
    USD: number;
  };
  styleColor: string;
}

export interface TotalUsersBalance { //-- /totalUsersBalances/{currency}
  name: string;
  amount: string;
  symbol: string;
}