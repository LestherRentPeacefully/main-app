import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { environment } from '../../environments/environment';
import { HttpClient }from '@angular/common/http';
import {SharedService} from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class PricingService {

  private APIURL = environment.APIURL;

  constructor(
    private http: HttpClient,
    private sharedService:SharedService) { }


  public async getCurrencyPrice(req:{currency:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/pricing/getCurrencyPrice`, req, _httpOptions).toPromise();
  }

  public async buyPricingPlan(req:{currency:string, plan:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/pricing/buyPricingPlan`, req, _httpOptions).toPromise();
  }

  public async cancelPricingPlan(): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/pricing/cancelPricingPlan`, {}, _httpOptions).toPromise();
  }

  public async selectPaymentMethod(req:{currency:string}): Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/pricing/selectPaymentMethod`, req, _httpOptions).toPromise();
  }

  // public pricings = [
  //   {
  //     plan: 'free',
  //     value: 0
  //   },{
  //     plan: 'plus',
  //     value: 9.95
  //   },{
  //     plan: 'professional',
  //     value: 14.95
  //   },{
  //     plan: 'enterprise',
  //     value: 19.95
  //   }
  // ];


}
