import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-membership-icon',
  templateUrl: './membership-icon.component.html',
  styleUrls: ['./membership-icon.component.css']
})
export class MembershipIconComponent implements OnInit {

  @Input() pricingPlan: string;

  constructor() { }

  ngOnInit() {
  }

}
