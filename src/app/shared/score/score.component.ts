import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css']
})
export class ScoreComponent implements OnInit {
  
  @Input() score = 0;

  @Output() selectedScore = new EventEmitter<number>();
 
  constructor() { }

  ngOnInit() {
  }

  selectScore(score:number){
    if(score>1 && score==this.score) this.selectedScore.emit(score-1);
    else this.selectedScore.emit(score);
  }

  public formatScore(score:number){
    if(score>=0 && score<0.25) return 0;
    if(score>=0.25 && score<0.75) return 0.5;
    if(score>=0.75 && score<1.25) return 1;
    if(score>=1.25 && score<1.75) return 1.5;
    if(score>=1.75 && score<2.25) return 2;
    if(score>=2.25 && score<2.75) return 2.5;
    if(score>=2.75 && score<3.25) return 3;
    if(score>=3.25 && score<3.75) return 3.5;
    if(score>=3.75 && score<4.25) return 4;
    if(score>=4.25 && score<4.75) return 4.5;
    if(score>=4.75) return 5;
  }

}
