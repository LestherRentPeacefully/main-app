import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {ToastService, ModalDirective} from 'ng-uikit-pro-standard';
import {ChatService} from './chat.service';
import {SharedService} from '../shared.service';
import { FormBuilder, Validators } from '@angular/forms';
import {MyServiceProvidersJobsService} from '../../my-service-providers-jobs/my-service-providers-jobs.service';
import { ListingsService } from '../../listings/listings.service';
import { MyPropertiesService } from '../../my-properties/my-properties.service';
import { Agreement } from '../interfaces';
import {AgreementsService} from '../../agreements/agreements.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  @ViewChild('chatModal') public chatModal:ModalDirective;
  @ViewChild('scrollbar') public scrollbar;

  @Input() userInfo;
  @Input() serviceProviderJob;
  @Input() applications:any[];
  @Input() tenantSelected;
  @Input() agreement: Agreement;

  chats:any[];
  messages:any[];
  chatSelected;
  sendingMessage;
  getLastMessageSub;
  loadingOldMessages;
  canLoadMoreOldMessages = true;
  private messagesOffset = 10;
  private toastOptions = this.sharedService.toastOptions;

  public messageForm = this.fb.group({
    content: ['', [Validators.required]],
  });

  constructor(
    private chatService: ChatService,
    private myServiceProvidersJobsService: MyServiceProvidersJobsService,
    public sharedService: SharedService,
    private fb: FormBuilder,
    private listingsService: ListingsService, 
    private myPropertiesService: MyPropertiesService,
    private agreementsService: AgreementsService,
    private toast: ToastService) { }

  ngOnInit() {
  }

  ngOnDestroy(){
    if(this.getLastMessageSub) this.getLastMessageSub.unsubscribe();
  }

  async openChatModal(){
    this.chatModalClosed();
    if(!this.chatModal.isShown) this.chatModal.show();
    const response = await this.chatService.getChats();
    if (response.success) {
      this.chats = response.data;
    } else {
      this.toast.error(response.error.message,'', this.toastOptions);
    }
  }


  async openMessagesModal(chat:any){
    this.chatModalClosed();
    this.chatSelected = chat;
    if(!this.chatModal.isShown) this.chatModal.show();

    if(this.chatSelected.id) {
      this.messages = await this.chatService.getMessages(this.chatSelected.id, this.messagesOffset, null);
      this.getLastMessageSub = this.chatService.getLastMessage$(this.chatSelected.id).subscribe(message=>{
        this.insertMessage(message);
        this.scrollToBottom();
      });
    }else{
      this.messages = [];
    }
    this.scrollToBottom();
  }

  chatModalClosed(){
    if(this.getLastMessageSub) this.getLastMessageSub.unsubscribe();
    this.getLastMessageSub = null;
    this.messages = null;
    this.canLoadMoreOldMessages = true;
    this.chats = null;
    this.chatSelected = null;
    this.loadingOldMessages = false;
  }


  async sendMessage(){
    const content = this.messageForm.get('content').value;
    this.messageForm.disable();
    this.sendingMessage = true;
    let response;

    if (this.chatSelected.id) {
      response = await this.chatService.sendMessage({content, chatID:this.chatSelected.id});

      if (response.success) {
        this.insertMessage({
          content,
          uid: this.userInfo.uid,
          creationTS: Date.now(),
          id: response.data
        });
        this.messageForm.get('content').setValue('');
        this.scrollToBottom();

      }



    } else if(!this.chatSelected.id && this.serviceProviderJob){
      response = await this.myServiceProvidersJobsService.startChat({content, serviceProviderJobID: this.serviceProviderJob.id});

      if (response.success) this.serviceProviderJob.chatID = response.data.chatID;
    
    
    } else if(!this.chatSelected.id && this.applications){
      response = await this.listingsService.startChat({content, listingID: this.chatSelected.listingID, applicantID: this.chatSelected.applicantID});
      
      if (response.success) {
        this.applications[this.applications.findIndex(item=>item.applicant.uid==this.chatSelected.applicantID)].applicant.chatID = response.data.chatID;
      }
    
    
    } else if(!this.chatSelected.id && this.tenantSelected) {
      response = await this.myPropertiesService.startChat({content, myPropertyID: this.chatSelected.myPropertyID, tenantID: this.tenantSelected.uid});

      if (response.success) this.tenantSelected.info.chatID = response.data.chatID;


    } else if(!this.chatSelected.id && this.agreement) {
      response = await this.agreementsService.startChat({content, agreementId: this.agreement.id});

      if (response.success) this.agreement.chatID = response.data.chatID;


    }

    if(response && response.success && !this.chatSelected.id) {
      this.chatSelected.id = response.data.chatID;

      this.insertMessage({
        content,
        uid: this.userInfo.uid,
        creationTS: Date.now(),
        id: response.data.messageID
      });
      this.messageForm.get('content').setValue('');
      this.scrollToBottom();

      this.getLastMessageSub = this.chatService.getLastMessage$(this.chatSelected.id).subscribe(message=>{
        this.insertMessage(message);
        this.scrollToBottom();
      });

    } else if(response && !response.success) {
      console.error(response.error.message);
      this.toast.error('',response.error.message, this.toastOptions);
    }

   
    this.messageForm.enable();
    this.sendingMessage = false;
  }

  

  chatScrollHandler(e){
  }


  async messagesScrollHandler(e){
    if(e=='top' && !this.loadingOldMessages && this.canLoadMoreOldMessages){
      this.loadingOldMessages = true;
      const oldMessages = await this.chatService.getMessages(this.chatSelected.id, this.messagesOffset, this.messages[0].creationTS);
      this.messages.unshift(...oldMessages);
      this.loadingOldMessages = false;
      this.canLoadMoreOldMessages = oldMessages.length==this.messagesOffset;
    }
  }

  insertMessage(message){
    if(!this.messages.find(item=>item.id==message.id)) {
      this.messages.push(message);
    }
  }

  private scrollToBottom(){
    setTimeout(() => {
      this.scrollbar.nativeElement.scrollTop = this.scrollbar.nativeElement.scrollHeight;
    }, 0);
  }

  public dateFromNow(date){
    return this.sharedService.dateFromNow(date);
  }

  public formatDate(unFormatedDate:number | Date){
    let formatedDate = this.sharedService.formatDate(unFormatedDate);
    
    formatedDate.minutes = <any>(formatedDate.minutes<10? `0${String(formatedDate.minutes)}` : formatedDate.minutes);
    formatedDate.hours = formatedDate.hours==0? 12:formatedDate.hours;
    formatedDate.hours = formatedDate.hours>12? formatedDate.hours-12:formatedDate.hours;
    return `${formatedDate.month} ${formatedDate.dayOfMonth} ${formatedDate.year}, ${formatedDate.hours}:${formatedDate.minutes} ${formatedDate.hours>=12? 'PM':'AM'}`;
  }


}
