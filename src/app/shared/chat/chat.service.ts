import { Injectable } from '@angular/core';
import {SharedService} from '../shared.service';
import { HttpClient }from '@angular/common/http';
import { environment } from '../../../environments/environment';
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private APIURL = environment.APIURL;

  constructor(
    private http: HttpClient,
    private afs: AngularFirestore,
    private sharedService:SharedService) { }


  async getChats():Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/chats/getChats`, {}, _httpOptions).toPromise();
  }

  async sendMessage(req:{content:string, chatID:string}):Promise<any>{
    let _httpOptions = await this.sharedService.getAuthHeader();
    return this.http.post(`${this.APIURL}/chats/sendMessage`, req, _httpOptions).toPromise();
  }

  async getMessages(chatID:string, offset:number, firstVisibleTS:number){
    const db = firebase.firestore();
    let messages = [];
    let messagesDocs:firebase.firestore.QueryDocumentSnapshot[];
    if (!firstVisibleTS) {
      messagesDocs = (await db.collection(`chats/${chatID}/messages`).orderBy('creationTS','desc').limit(offset).get()).docs;
    } else {
      messagesDocs = (await db.collection(`chats/${chatID}/messages`).orderBy('creationTS','desc').startAfter(firstVisibleTS).limit(offset).get()).docs;
    }

    for(let doc of messagesDocs.reverse()){
      messages.push(doc.data());
    }

    return messages;
  }

  getLastMessage$(chatID:string){
    return this.afs.collection(`chats/${chatID}/messages`, (ref)=>ref.orderBy('creationTS','desc').limit(1)
    ).valueChanges().pipe(map(val=><any>val[0]));
  }


}
