import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import {BigNumber} from 'bignumber.js';
import * as moment from 'moment';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

declare var require:any;

@Injectable({
  providedIn: 'root'
})
export class SharedService {

	public countriesObj = require('../../assets/countries.json');

  private APIURL = environment.APIURL;

	public countriesOptions = [];

  public toastOptions = { positionClass:'md-toast-bottom-right', progressBar:true, toastClass:'opacity' };
  
  private scriptStore = [
    {name: 'hellosign', src: 'https://s3.amazonaws.com/cdn.hellosign.com/public/js/hellosign-embedded.LATEST.min.js'},
    {name: 'ipfs', src: 'https://unpkg.com/ipfs-http-client/dist/index.min.js'}
  ];

  private scripts: any = {};

  public IDTypeOptions = [
    {
      value:'1',
      label:'Passport'
    },
    {
      value:'2',
      label:'Drivers Licence'
    },
    {
      value:'3',
      label:'National ID Card'
    }
  ];

  public propertyOptions = [
      { value: '1', label: 'House' },
      { value: '2', label: 'Apartment' },
      { value: '3', label: 'Basement' },
      { value: '4', label: 'Condo' },
      { value: '5', label: 'Duplex' },
      { value: '6', label: 'Mobile Home' },
      { value: '7', label: 'Room' },
      { value: '8', label: 'Townhouse' },
      // { value: '100', label: 'Other' },
  ];

  public petsAllowanceOptions = [
    { value: '1', label: 'Do not specify' },
    { value: '2', label: 'Yes' },
    { value: '3', label: 'No' },
    { value: '4', label: "Only with Landlord's written consent" },
  ];

  public bedsOptions = [
    { value: '0', label: 'Studio' },
    { value: '1', label: '1' },
    { value: '2', label: '2' },
    { value: '3', label: '3' },
    { value: '4', label: '4' },
    { value: '5', label: '5' },
    { value: '6', label: '6' },
    { value: '7', label: '7' },
    { value: '8', label: '8' },
    { value: '9', label: '9+' },
  ];

  public bathsOptions = [
    // { value: '0', label: '0.5' },
    { value: '1', label: '1' },
    // { value: '2', label: '1.5' },
    { value: '2', label: '2' },
    // { value: '4', label: '2.5' },
    { value: '3', label: '3' },
    // { value: '6', label: '3.5' },
    { value: '4', label: '4' },
    // { value: '8', label: '4.5' },
    { value: '5', label: '5' },
    // { value: '10', label: '5.5' },
    { value: '6', label: '6' },
    // { value: '12', label: '6.5' },
    { value: '7', label: '7' },
    // { value: '14', label: '7.5' },
    { value: '8', label: '8' },
    // { value: '16', label: '8.5' },
    { value: '9', label: '9+' },
  ];

  public roleOptions = [
      { value: '1', label: 'I am a Tenant', role:'tenant'},
      { value: '2', label: 'I am a Landlord', role:'landlord'},
      { value: '3', label: 'I am a Service Provider', role:'service provider'},
      { value: '4', label: 'I am a Real Estate Agent', role:'real estate agent'},
      // { value: '5', label: 'I am a Broker', role:'broker'},
  ];

  public amenities = [
    {
      name:'Washer/Dryer In Unit',
      id:'washerUnit',
    },{
      name:'Pool',
      id:'pool',
    },{
      name:'Gym',
      id:'gym',
    },{
      name:'Elevator',
      id:'elevator',
    },{
      name:'Parking Spot',
      id:'parking',
    },{
      name:'Fireplace',
      id:'fireplace',
    },{
      name:'Air Conditioning',
      id:'airConditioning',
    },{
      name:'Doorman',
      id:'doorman',
    },{
      name:'Dishwasher',
      id:'dishwasher',
    },{
      name:'Deck',
      id:'roofDeck',
    },{
      name:'Storage',
      id:'storage',
    },{
      name:'Wheelchair Accessible',
      id:'wheelchairAccessible',
    },{
      name:'Hardwood Floors',
      id:'hardwoodFloors',
    },{
      name:'Garden',
      id:'garden',
    },{
      name:'Balcony',
      id:'balcony',
    },{
      name:'Furnished',
      id:'furnished',
    },{
      name:'View',
      id:'view',
    } ,{
      name:'Student Friendly',
      id:'studentFriendly',
    },{
      name:'High Rise',
      id:'highRise',
    },{
      name:'Utilities Included',
      id:'utilitiesIncluded',
    }
  ];



  public serviceProviderSkills = [
    {
      name:'General Labor',
      id:'generalLabor'
    },
    {
      name:'Computer Support',
      id:'computerSupport'
    },
    {
      name:'Handyman',
      id:'handyman'
    },
    {
      name:'Painting',
      id:'painting'
    },
    {
      name:'Drafting',
      id:'drafting'
    },
    {
      name:'Housework',
      id:'housework'
    },
    {
      name:'Electric Repair',
      id:'electricRepair'
    },
    {
      name:'Shopping',
      id:'shopping'
    },
    {
      name:'Appliance Repair',
      id:'applianceRepair'
    },
    {
      name:'Carpentry',
      id:'carpentry'
    },
    {
      name:'Cooking And Recipes',
      id:'cookingAndRecipes'
    },
    {
      name:'Extension And Additions',
      id:'extensionAndAdditions'
    },
    {
      name:'Plumbing',
      id:'plumbing'
    },
    {
      name:'Sewing',
      id:'sewing'
    },
    {
      name:'Appliance Installation',
      id:'applianceInstallation'
    },
    {
      name:'Building',
      id:'building'
    },
    {
      name:'Drone Photography',
      id:'dronePhotography'
    },
    {
      name:'Event Staffing',
      id:'eventStaffing'
    },
    {
      name:'Home Automation',
      id:'homeAutomation'
    },
    {
      name:'Interiors',
      id:'interiors'
    },
    {
      name:'Make up',
      id:'makeUp'
    },
    {
      name:'Cooking / Backing',
      id:'cooking_backing'
    },
    {
      name:'Furniture Assembly',
      id:'furnitureAssembly'
    },
    {
      name:'House Cleaning',
      id:'houseCleaning'
    },
    {
      name:'Landscape Design',
      id:'landscapeDesign'
    },
    {
      name:'Mural Painting',
      id:'muralPainting'
    },
    {
      name:'Commercial Cleaning',
      id:'commercialCleaning'
    },
    {
      name:'Domestic Cleaning',
      id:'domesticCleaning'
    },
    {
      name:'Air Conditioning',
      id:'airConditioning'
    },
    {
      name:'Bathroom',
      id:'bathroom'
    },
    {
      name:'Building Certification',
      id:'buildingCertification'
    },
    {
      name:'Carpet Cleaning',
      id:'carpetCleaning'
    },
    {
      name:'Carpet Repair And Laying',
      id:'carpetRepairAndLaying'
    },
    {
      name:'Decking',
      id:'decking'
    },
    {
      name:'Decoration',
      id:'decoration'
    },
    {
      name:'Embroidery',
      id:'embroidery'
    },
    {
      name:'Gardening',
      id:'gardening'
    },
    {
      name:'Home Organization',
      id:'homeOrganization'
    },
    {
      name:'Installation',
      id:'installation'
    },
    {
      name:'Hot Water Installation',
      id:'hotWaterInstallation'
    },
    {
      name:'Kitchen',
      id:'kitchen'
    },
    {
      name:'Landscape and Gardening',
      id:'landscapeAndGardening'
    },
    {
      name:'Lighting',
      id:'lighting'
    },
    {
      name:'Piping',
      id:'piping'
    },
    {
      name:'Antenna Services',
      id:'antennaServices'
    },
    {
      name:'Asphalt',
      id:'asphalt'
    },
    {
      name:'Bracket Installation',
      id:'bracketInstallation'
    },
    {
      name:'Car Washing',
      id:'carWashing'
    },
    {
      name:'Gas Fitting',
      id:'gasFitting'
    },
    {
      name:'Gutter Installation',
      id:'gutterInstallation'
    },
    {
      name:'Heating Systems',
      id:'heatingSystems'
    },
    {
      name:'Pet Sitting',
      id:'petSitting'
    },
    {
      name:'Pavement',
      id:'pavement'
    },
    {
      name:'Tiling',
      id:'tiling'
    },
    {
      name:'Lawn Mowing',
      id:'lawnMowing'
    },
    {
      name:'Laundry and Ironing',
      id:'laundryAndIroning'
    },
    {
      name:'Locksmith',
      id:'locksmith'
    },
    {
      name:'Bricklaying',
      id:'bricklaying'
    },
  ];

  
  public daysOfTheWeekOptions = [
      { value: '0', label: 'Sunday' },
      { value: '1', label: 'Monday' },
      { value: '2', label: 'Tuesday' },
      { value: '3', label: 'Wednesday' },
      { value: '4', label: 'Thurday' },
      { value: '5', label: 'Friday' },
      { value: '6', label: 'Saturday' },
  ];


  public daysOfTheMonthOptions = [
      { value: '1', label: '1st' },
      { value: '2', label: '2nd' },
      { value: '3', label: '3rd' },
      { value: '4', label: '4th' },
      { value: '5', label: '5th' },
      { value: '6', label: '6th' },
      { value: '7', label: '7th' },
      { value: '8', label: '8th' },
      { value: '9', label: '9th' },
      { value: '10', label: '10th' },
      { value: '11', label: '11st' },
      { value: '12', label: '12th' },
      { value: '13', label: '13rd' },
      { value: '14', label: '14th' },
      { value: '15', label: '15th' },
      { value: '16', label: '16th' },
      { value: '17', label: '17th' },
      { value: '18', label: '18th' },
      { value: '19', label: '19th' },
      { value: '20', label: '20th' },
      { value: '21', label: '21st' },
      { value: '22', label: '22nd' },
      { value: '23', label: '23rd' },
      { value: '24', label: '24th' },
      { value: '25', label: '25th' },
      { value: '26', label: '26th' },
      { value: '27', label: '27th' },
      { value: '28', label: '28th' },
      { value: '29', label: '29th' },
      { value: '30', label: '30th' },
      { value: '31', label: '31st' }
  ];

  public monthsOptions = [
      { value: '1', label: 'January' },
      { value: '2', label: 'February' },
      { value: '3', label: 'March' },
      { value: '4', label: 'May' },
      { value: '5', label: 'April' },
      { value: '6', label: 'June' },
      { value: '7', label: 'July' },
      { value: '8', label: 'August' },
      { value: '9', label: 'September' },
      { value: '10', label: 'October' },
      { value: '11', label: 'November' },
      { value: '12', label: 'December' }
  ];




  constructor(private http: HttpClient,
              public afAuth: AngularFireAuth,
              private afs: AngularFirestore,
              private storage: AngularFireStorage) {

    for(let i in this.countriesObj){
      this.countriesOptions.push({ value: String(i), label: this.countriesObj[i].name.common });
    }

    this.scriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src
      };
    });

  }

  public formatDate(unFormatedDate:number | Date){
      let date = new Date(unFormatedDate);
      let obj = {
        dayOfTheWeek : this.daysOfTheWeekOptions.find(option=>option.value==String(date.getDay())).label,
        dayOfMonth : this.daysOfTheMonthOptions.find(option=>option.value==String(date.getDate())).label,
        month : this.monthsOptions.find(option=>option.value==String(date.getMonth()+1)).label,
        year : date.getFullYear(),
        hours : date.getHours(),
        minutes : date.getMinutes(),
        seconds : date.getSeconds()
      
      };
      return obj;  
  }

  public contactSupport(user:{name:string, subject:string,email:string,message:string}):Promise<any>{
    return this.http.post(`${this.APIURL}/support/contact`,user,httpOptions).toPromise();
  }


  private getFileExtension(type: any): string{
      switch (type) {
          case 'image/jpeg':
              return 'jpeg';
          case 'image/jpg':
              return 'jpg';
          case 'image/gif':
              return 'gif';
          case 'image/png':
              return 'png';
          case 'image/svg+xml':
              return 'svg';
          default:
              return '';
      }
  }

  public newFileName(type:string):string{
    const db = firebase.firestore();
    let id = db.collection('uniqueID').doc().id;
    return `${id}.${this.getFileExtension(type)}`;
  }

  public deletePhoto(filePath:string){ 
    let fileRef =this.storage.storage.ref(filePath);
    return fileRef.delete();
  }

  public photoPathFromURL(url:string){
    return this.storage.storage.refFromURL(url).fullPath;
  }

  async getUser(uid:string){
    const db = firebase.firestore();
    let snapshot = await db.doc(`users/${uid}`).get();
    return snapshot.data();
  }

  getToken(){
    return this.afAuth.auth.currentUser.getIdToken(true);
  }

  async getAuthHeader(){
    let token = await this.getToken();
    return { headers : httpOptions.headers.append('Authorization', token) };
  }

  public formatBalance(value, decimals){
    return new BigNumber(value).times(new BigNumber(10).pow(-decimals)).decimalPlaces(10).toString(10); 
  }

  public async getCurrencies(){
    let currencies = [];
    const db = firebase.firestore();
    let snashots = await db.collection('currencies').orderBy('name','asc').get();
    for(let snashot of snashots.docs){
      currencies.push(snashot.data());
    }
    return currencies;
  }


  public loadScripts(...names: string[]) {
    var promises: any[] = [];
    names.forEach(name => promises.push(this.loadScript(name)));
    return Promise.all(promises);
  }

  public loadScript(name: string) {
    return new Promise((resolve, reject) => {
      //resolve if already loaded
      if (this.scripts[name].loaded) {
          resolve({script: name, loaded: true, status: 'Already Loaded'});
      } else {
        //load script
        let script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.scripts[name].src;

        if ((<any>script).readyState) {  //IE
          (<any>script).onreadystatechange = () => {
            if ((<any>script).readyState === "loaded" || (<any>script).readyState === "complete") {
              (<any>script).onreadystatechange = null;
              this.scripts[name].loaded = true;
              resolve({script: name, loaded: true, status: 'Loaded'});
            }
          };

        } else {  //Others
          script.onload = () => {
            this.scripts[name].loaded = true;
            resolve({script: name, loaded: true, status: 'Loaded'});
          };
        }

        script.onerror = (err) => reject({script: name, loaded: false, status: 'Loaded'});
        document.getElementsByTagName('head')[0].appendChild(script);
      }
    });
  }


  public dateFromNow(date:number | Date){
    return moment(date).fromNow();
  }


  async getNotifications(uid:string, offset:number, lastVisibleTS:number){
    const db = firebase.firestore();
    let notifications = [];
    let notificationsDocs:firebase.firestore.QueryDocumentSnapshot[];
    if (!lastVisibleTS) {
      notificationsDocs = (await db.collection(`users/${uid}/notifications`).orderBy('creationTS','desc').limit(offset).get()).docs;
    } else {
      notificationsDocs = (await db.collection(`users/${uid}/notifications`).orderBy('creationTS','desc').startAfter(lastVisibleTS).limit(offset).get()).docs;
    }

    notificationsDocs.forEach(doc=> notifications.push(doc.data()));

    return notifications;
  }

  getLastNotification$(uid:string){
    return this.afs.collection(`users/${uid}/notifications`, (ref)=>ref.orderBy('creationTS','desc').limit(1)
    ).valueChanges().pipe(map(val=><any>val[0]));
  }

  async getLastNotification(uid:string){
    const db = firebase.firestore();
    let notification = (await db.collection(`users/${uid}/notifications`).orderBy('creationTS','desc').limit(1).get()).docs[0].data();
    return notification;
  }

  async markNotificationsAsRead(uid:string){
    const db = firebase.firestore();
    return db.doc(`users/${uid}/settings/preferences`).update({ 'counters.unreads.notifications':0 });
  }

  async provideFeedback(req:{notificationID:string, feedback:any}){
    let _httpOptions = await this.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/notifications/provideFeedback`, req, _httpOptions).toPromise();
  }

  async confirmPropertyListing(req:{notificationID:string}){
    let _httpOptions = await this.getAuthHeader();
    return this.http.post<any>(`${this.APIURL}/notifications/confirmPropertyListing`, req, _httpOptions).toPromise();
  }

  capitalize(value:string){
		return value.charAt(0).toUpperCase() + value.slice(1);
  }
  

  addMonths(oldDate:number | Date, count:number) {
    const date = new Date(oldDate);
    const day = date.getDate();
    date.setMonth(date.getMonth() + count, 1);
    const month = date.getMonth();
    date.setDate(day);
    if (date.getMonth() !== month) date.setDate(0);
    return date;
  }


  public pricings = {
    free: {
      amount: 0
    },
    basic: {
      amount: 4.95
    },
    plus: {
      amount: 9.95
    },
    professional: {
      amount: 14.95
    },
    enterprise: {
      amount: 19.95
    }
  }

  copyToClipboard(value: string) {
    const el = document.createElement('textarea');  // Create a <textarea> element
    el.value = value;                                 // Set its value to the string that you want copied
    el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
    el.style.position = 'absolute';                 
    el.style.left = '-9999px';                      // Move outside the screen to make it invisible
    document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
    const selected =            
      document.getSelection().rangeCount > 0        // Check if there is any content selected previously
        ? document.getSelection().getRangeAt(0)     // Store selection if found
        : false;                                    // Mark as false to know no selection existed before
    el.select();                                    // Select the <textarea> content
    document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
    document.body.removeChild(el);                  // Remove the <textarea> element
    if (selected) {                                 // If a selection existed before copying
      document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
      document.getSelection().addRange(selected);   // Restore the original selection
    }
  }



}
