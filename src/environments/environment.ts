let blockchain='main';
// let blockchain='test';
// AIzaSyApUUuFkU2n7TAVHeniz_GcKPOFGcSvDDU rappi
// AIzaSyAk5UwwQ6TZyJ1pER_5bJhiUkUWWreLFSY glovo
// AIzaSyB-Z4JlqJFSXUSUHLsdojpyb4zzSZWEOJg buenosaires21
export const environment = {
  production: false,
  etherscanExplorer: blockchain=='main'? 'https://etherscan.io':'https://ropsten.etherscan.io',
  etherscanURL: blockchain=='main'? 'https://api.etherscan.io':'https://api-ropsten.etherscan.io',
  social:{
    facebook:'https://www.facebook.com/Rent-Peacefully-256877458261031',
    twitter:'https://twitter.com/RPeacefully'
  },
  googleMaps:{
    apiKey:'AIzaSyB-Z4JlqJFSXUSUHLsdojpyb4zzSZWEOJg'
  },
  hellosign:{
    clientId: 'c675f77b711a96f7aaf15a2deb333d33'
  },
  recapchaKey:'6LfFSTgUAAAAAND7nK-Fntp2YbTTbCtZDX_hNcdd', //6LfRTWkUAAAAAMazu71sKW4UIZBqNoxT00oxB6LK production
  app:{
    name:'RentPeacefully',
    symbol:'ERT',
  },
  APIURL:'https://rentpeacefully-main-api.herokuapp.com',
  firebase: {
    apiKey: "AIzaSyCPaIsy18OhydzE1FDI7mEKd28qaNSbULI",
    authDomain: "blockchain-projects.firebaseapp.com",
    databaseURL: "https://blockchain-projects.firebaseio.com",
    projectId: "blockchain-projects",
    storageBucket: "blockchain-projects.appspot.com",
    messagingSenderId: "323863378443"
  },
  gaConfig: {
    trackingId: 'UA-125316243-1',
    debug: false
  }

};

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
